#pragma once
#include "stdafx.h"

namespace basecross {

	class EnemyInterface {
	public:
		EnemyInterface() {}
		virtual ~EnemyInterface() {}
	};

	//--------------------------------------------------------------------------------------
	//	class HeadObject : public GameObject;
	//	用途: 追いかける配置オブジェクト
	//--------------------------------------------------------------------------------------
	class HeadEnemy : public GameObject, public EnemyInterface {
		Vector3 m_StartPos;
		float m_BaseY;
		float m_StateChangeSize;
		//行動マシーン
		shared_ptr< BehaviorMachine<HeadEnemy> >  m_BehaviorMachine;
	public:
		//ユーティリティ関数群
		float GetStateChangeSize()const {
			return m_StateChangeSize;
		}
		//プレイヤーの位置を返す
		Vector3 GetPlayerPosition() const;
		//プレイヤーまでの距離を返す
		float GetPlayerLength() const;
		//回転を進行方向に向かせる
		void RotToFront();
		//構築と破棄
		HeadEnemy(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~HeadEnemy();
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr<BehaviorMachine<HeadEnemy>> GetBehaviorMachine() const {
			return m_BehaviorMachine;
		}
		//操作
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		virtual void OnLastUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	//	静止行動
	//--------------------------------------------------------------------------------------
	class HeadStop : public Behavior<HeadEnemy> {
		float m_StopTime;
	public:
		HeadStop() :m_StopTime(0) {}
		virtual ~HeadStop() {}
		virtual void OnCreateWithParam() {}
		virtual void Enter(const shared_ptr<HeadEnemy>& Obj) override;
		virtual void Execute(const shared_ptr<HeadEnemy>& Obj) override;
		virtual void WakeUp(const shared_ptr<HeadEnemy>& Obj) override;
		virtual void Exit(const shared_ptr<HeadEnemy>& Obj) override;
	};

	//--------------------------------------------------------------------------------------
	//	ジャンプして追跡行動
	//--------------------------------------------------------------------------------------
	class HeadJumpSeek : public Behavior<HeadEnemy> {
	public:
		HeadJumpSeek() {}
		virtual ~HeadJumpSeek() {}
		virtual void OnCreateWithParam() {}
		virtual void Enter(const shared_ptr<HeadEnemy>& Obj) override;
		virtual void Execute(const shared_ptr<HeadEnemy>& Obj) override;
		virtual void Execute2(const shared_ptr<HeadEnemy>& Obj) override;
		virtual void Exit(const shared_ptr<HeadEnemy>& Obj) override;
	};

	//--------------------------------------------------------------------------------------
	//	敵1
	//--------------------------------------------------------------------------------------
	class CellSeekEnemy1 : public GameObject, public EnemyInterface {
	protected:
		weak_ptr<StageCellMap> m_CelMap;
		Vector3 m_Scale;
		Vector3 m_StartRotation;
		Vector3 m_StartPosition;
		vector<CellIndex> m_CellPath;
		//現在の自分のセルインデックス
		int m_CellIndex;
		//めざす（次の）のセルインデックス
		int m_NextCellIndex;
		//ターゲットのセルインデックス
		int m_TargetCellIndex;
		shared_ptr<StateMachine<CellSeekEnemy1>> m_StateMachine;
	public:
		//構築と破棄
		CellSeekEnemy1(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~CellSeekEnemy1();
		//プレイヤーの検索
		bool SearchPlayer();

		//デフォルト行動
		virtual bool DefaultBehavior();
		//Seek行動
		bool SeekBehavior();
		//アクセサ
		shared_ptr< StateMachine<CellSeekEnemy1> > GetStateMachine() const {
			return m_StateMachine;
		}
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;

		virtual void OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec)override;

		virtual void OnCollisionExit(vector<shared_ptr<GameObject>>& OtherVec) override;

		virtual void OnLastUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	class EnemyDefault1 : public ObjState<CellSeekEnemy1>
	{
		EnemyDefault1() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(EnemyDefault1)
		virtual void Enter(const shared_ptr<CellSeekEnemy1>& Obj)override;
		virtual void Execute(const shared_ptr<CellSeekEnemy1>& Obj)override;
		virtual void Exit(const shared_ptr<CellSeekEnemy1>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	///	プレイヤーを追いかけるステート
	//--------------------------------------------------------------------------------------
	class EnemySeek1 : public ObjState<CellSeekEnemy1>
	{
		EnemySeek1() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(EnemySeek1)
		virtual void Enter(const shared_ptr<CellSeekEnemy1>& Obj)override;
		virtual void Execute(const shared_ptr<CellSeekEnemy1>& Obj)override;
		virtual void Exit(const shared_ptr<CellSeekEnemy1>& Obj)override;
	};

}