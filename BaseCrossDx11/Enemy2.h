#pragma once
#include "stdafx.h"

namespace basecross {

	class EnemyInterface2 {
	public:
		EnemyInterface2() {}
		virtual ~EnemyInterface2() {}
	};

	//--------------------------------------------------------------------------------------
	//	class HeadObject2 : public GameObject;
	//	用途: 追いかける配置オブジェクト
	//--------------------------------------------------------------------------------------
	class HeadEnemy2 : public GameObject, public EnemyInterface2 {
		Vector3 m_StartPos;
		float m_BaseY;
		float m_StateChangeSize;
		//行動マシーン
		shared_ptr< BehaviorMachine<HeadEnemy2> >  m_BehaviorMachine;
	public:
		//ユーティリティ関数群
		float GetStateChangeSize()const {
			return m_StateChangeSize;
		}
		//プレイヤーの位置を返す
		Vector3 GetPlayer2Position() const;
		//プレイヤーまでの距離を返す
		float GetPlayer2Length() const;
		//回転を進行方向に向かせる
		void RotToFront();
		//構築と破棄
		HeadEnemy2(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~HeadEnemy2();
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr<BehaviorMachine<HeadEnemy2>> GetBehaviorMachine() const {
			return m_BehaviorMachine;
		}
		//操作
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		virtual void OnLastUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	//	静止行動
	//--------------------------------------------------------------------------------------
	class HeadStop2 : public Behavior<HeadEnemy2> {
		float m_StopTime;
	public:
		HeadStop2() :m_StopTime(0) {}
		virtual ~HeadStop2() {}
		virtual void OnCreateWithParam() {}
		virtual void Enter(const shared_ptr<HeadEnemy2>& Obj) override;
		virtual void Execute(const shared_ptr<HeadEnemy2>& Obj) override;
		virtual void WakeUp(const shared_ptr<HeadEnemy2>& Obj) override;
		virtual void Exit(const shared_ptr<HeadEnemy2>& Obj) override;
	};

	//--------------------------------------------------------------------------------------
	//	ジャンプして追跡行動
	//--------------------------------------------------------------------------------------
	class HeadJumpSeek2 : public Behavior<HeadEnemy2> {
	public:
		HeadJumpSeek2() {}
		virtual ~HeadJumpSeek2() {}
		virtual void OnCreateWithParam() {}
		virtual void Enter(const shared_ptr<HeadEnemy2>& Obj) override;
		virtual void Execute(const shared_ptr<HeadEnemy2>& Obj) override;
		virtual void Execute2(const shared_ptr<HeadEnemy2>& Obj) override;
		virtual void Exit(const shared_ptr<HeadEnemy2>& Obj) override;
	};

	//--------------------------------------------------------------------------------------
	//	敵1
	//--------------------------------------------------------------------------------------
	class CellSeekEnemy2 : public GameObject, public EnemyInterface2 {
	protected:
		weak_ptr<StageCellMap> m_CelMap;
		Vector3 m_Scale;
		Vector3 m_StartRotation;
		Vector3 m_StartPosition;
		vector<CellIndex> m_CellPath;
		//現在の自分のセルインデックス
		int m_CellIndex;
		//めざす（次の）のセルインデックス
		int m_NextCellIndex;
		//ターゲットのセルインデックス
		int m_TargetCellIndex;
		shared_ptr<StateMachine<CellSeekEnemy2>> m_StateMachine;
	public:
		//構築と破棄
		CellSeekEnemy2(const shared_ptr<Stage>& StagePtr,
			const shared_ptr<StageCellMap>& CellMap,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~CellSeekEnemy2();
		//プレイヤーの検索
		bool SearchPlayer();

		//デフォルト行動
		virtual bool DefaultBehavior();
		//Seek行動
		bool SeekBehavior();
		//アクセサ
		shared_ptr< StateMachine<CellSeekEnemy2> > GetStateMachine() const {
			return m_StateMachine;
		}
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnUpdate() override;
		virtual void OnLastUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	class EnemyDefault2 : public ObjState<CellSeekEnemy2>
	{
		EnemyDefault2() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(EnemyDefault2)
		virtual void Enter(const shared_ptr<CellSeekEnemy2>& Obj)override;
		virtual void Execute(const shared_ptr<CellSeekEnemy2>& Obj)override;
		virtual void Exit(const shared_ptr<CellSeekEnemy2>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	///	プレイヤーを追いかけるステート
	//--------------------------------------------------------------------------------------
	class EnemySeek2 : public ObjState<CellSeekEnemy2>
	{
		EnemySeek2() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(EnemySeek2)
		virtual void Enter(const shared_ptr<CellSeekEnemy2>& Obj)override;
		virtual void Execute(const shared_ptr<CellSeekEnemy2>& Obj)override;
		virtual void Exit(const shared_ptr<CellSeekEnemy2>& Obj)override;
	};

}