/*!
@file Timer.h
@brief タイマー
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class TimerSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class TimerSprite : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		float m_TotalTime;

		int count = 0;
	public:
		//構築と破棄
		TimerSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, float StartTime);
		virtual ~TimerSprite();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;

		float getTotalTime()const { return m_TotalTime; }
		void setTotalTime(float Time) { m_TotalTime = Time; }
	};

	//--------------------------------------------------------------------------------------
	//	class TimerSprite2 : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class TimerSprite2 : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		float m_TotalTime;
	public:
		//構築と破棄
		TimerSprite2(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, float StartTime);
		virtual ~TimerSprite2();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;

		float getTotalTime()const { return m_TotalTime; }
		void setTotalTime(float Time) { m_TotalTime = Time; }
	};
	//--------------------------------------------------------------------------------------
	//	class TimerSprite3 : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class TimerSprite3 : public GameObject {
		Vector3 m_StartPos;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		float m_TotalTime;
	public:
		//構築と破棄
		TimerSprite3(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, float StartTime);
		virtual ~TimerSprite3();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;

		float getTotalTime()const { return m_TotalTime; }
		void setTotalTime(float Time) { m_TotalTime = Time; }
	};

	
}
//end basecross
