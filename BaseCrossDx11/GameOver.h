/*!
@file GameOver.h
@brief ゲームオーバー
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス
	//--------------------------------------------------------------------------------------
	class GameOver : public Stage {
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		//void CreatePlate();
		//壁模様のスプライト作成
		void CreateSprite();

		int a = 0;
		bool b = false;

		shared_ptr<MultiAudioObject> m_AudioObjectPtr;


	public:
		//構築と破棄
		GameOver() :Stage() {}
		virtual ~GameOver() {}
		void SceneChange1();
		void SceneChange2();
		void SceneChange3();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
	};

}
//end basecross
