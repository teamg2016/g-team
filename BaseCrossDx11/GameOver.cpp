/*!
@file GameOver.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス実体
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void GameOver::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"hukaiao.png";
		App::GetApp()->RegisterTexture(L"HUKAIAO_TX", strTexture);
		strTexture = DataDir + L"haiiro.png";
		App::GetApp()->RegisterTexture(L"HIIRO_TX", strTexture);
		strTexture = DataDir + L"Gameover.png";
		App::GetApp()->RegisterTexture(L"GAMEOVER_TX", strTexture);
		strTexture = DataDir + L"Over.jpg";
		App::GetApp()->RegisterTexture(L"OVER_TX", strTexture);
		strTexture = DataDir + L"Retry.png";
		App::GetApp()->RegisterTexture(L"RETRY_TX", strTexture);
		strTexture = DataDir + L"StageSelect1.png";
		App::GetApp()->RegisterTexture(L"SELECT_TX", strTexture);
		strTexture = DataDir + L"Title3.png";
		App::GetApp()->RegisterTexture(L"TITLE3_TX", strTexture);


	}

	void GameOver::CreateViewLight()
	{
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}
	//スプライト作成
	void GameOver::CreateSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<Sprite>(L"OVER_TX", false,
			Vector2(w, h), Vector2(0, 0));
		AddGameObject<Sprite>(L"GAMEOVER_TX", false,
			Vector2(w / 3, h / 7), Vector2(0, 200));
			Vector2(400.0f, 200.0f), Vector2(0.0f, 200.0f);
		auto ssg = CreateSharedObjectGroup(L"SelectPlateGroup");
		auto ss = AddGameObject<Sprite>(L"RETRY_TX", false,
			Vector2(w / 7, h / 7), Vector2(-300, -200));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"SELECT_TX", false,
			Vector2(w / 7, h / 7), Vector2(0, -200));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"TITLE3_TX", false,
			Vector2(w / 7, h / 7), Vector2(300, -200));
		ssg->IntoGroup(ss);
	}

	//シーン変更
	void GameOver::SceneChange1()
	{
		auto Ptr = App::GetApp()->GetScene<Scene>();
		int StageNo = Ptr->StageNum;
		if (StageNo == 11) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-1");
		}
		if (StageNo == 12) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-2");
		}
		if (StageNo == 13) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-3");
		}
		if (StageNo == 14) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-4");
		}
		if (StageNo == 15) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-5");
		}
		if (StageNo == 21) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-1");
		}
		if (StageNo == 22) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-2");
		}
		if (StageNo == 23) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-3");
		}
		if (StageNo == 24) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-4");
		}
		if (StageNo == 25) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-5");
		}
		if (StageNo == 31) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage3-1");
		}
		if (StageNo == 32) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage3-2");
		}
		if (StageNo == 33) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage3-3");
		}
		if (StageNo == 51) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage5-1");
		}
		if (StageNo == 52) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage5-2");
		}
		Ptr->setSF(false);
	}

	//シーン変更
	void GameOver::SceneChange2()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToMenuStage");
		ScenePtr->setSF(false);
	}
	//シーン変更
	void GameOver::SceneChange3()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToTitle");
		ScenePtr->setSF(false);
	}

	void GameOver::OnCreate() {

		auto Ptr = AddGameObject<GameObject>();
		SetSharedGameObject(L"SEManager", Ptr);

		auto Ptr1 = App::GetApp()->GetScene<Scene>();

		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateSprite();

			Ptr1->setCF1(false);
			Ptr1->setCF2(false);
			Ptr1->setCF3(false);
			Ptr1->setCF4(false);

			auto FB = AddGameObject<Black>();
			SetSharedGameObject(L"Black", FB);
			auto FBIn = AddGameObject<BlackIn>();
			SetSharedGameObject(L"BlackIn", FBIn);
		}
		catch (...) {
			throw;
		}

		//サウンドを登録.
		auto pMultiSoundEffect = Ptr->AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Decision");
		pMultiSoundEffect->AddAudioResource(L"Select");

	}

	//更新
	void GameOver::OnUpdate() {

		GetSharedGameObject<BlackIn>(L"BlackIn", false)->StartBlackIn();

		if (GetSharedGameObject<BlackIn>(L"BlackIn", false)->GetBlackInFinish())
		{

			//コントローラの取得
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected) {
				//Aボタンが押された瞬間ならステージ推移
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
				}
				if (a == 0)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange1();
					}
				}
				if (a == 1)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange2();
					}
				}
				if (a == 2)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange3();
					}
				}

				if (CntlVec[0].fThumbLX < -0.5f) {
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Select", 0, 0.2f);
					if (!b) {
						a -= 1;
						if (a < 0) {
							a = 2;
						}
						b = true;
					}
				}
				else if (CntlVec[0].fThumbLX > 0.5f) {
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Select", 0, 0.2f);
					if (!b) {
						a += 1;
						if (a > 2) {
							a = 0;
						}
						b = true;
					}
				}
				else {
					b = false;
				}
			}
			auto ssg = GetSharedObjectGroup(L"SelectPlateGroup");
			float w = static_cast<float>(App::GetApp()->GetGameWidth());
			float h = static_cast<float>(App::GetApp()->GetGameHeight());
			for (int i = 0; i < ssg->size(); i++) {
				if (i == a) {
					ssg->at(i)->GetComponent<Transform>()->SetScale(w / 5, h / 5, 0.1);
					continue;
				}
				ssg->at(i)->GetComponent<Transform>()->SetScale(w / 7, h / 7, 0.1);
			}
		}
	}

}
//end basecross
