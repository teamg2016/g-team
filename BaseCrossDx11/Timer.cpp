/*!
@file Timer.cpp
@brief タイマー
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class TimerSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TimerSprite::TimerSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, float StartTime) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_TotalTime(StartTime)
	{}
	TimerSprite::~TimerSprite() {}

	//初期化
	void TimerSprite::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(100.0f, 100.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"TIMER_TX");
		//透明処理
		SetAlphaActive(true);
		//スプライトの中のメッシュからバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//各数字ごとにUV値を含む頂点データを配列化しておく
		for (size_t i = 0; i < 10; i++) {
			float from = ((float)i) / 10.0f;
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
					SpVertexVec[0].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 0)
				),
				//右上頂点
				VertexPositionColorTexture(
					SpVertexVec[1].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 0)
				),
				//左下頂点
				VertexPositionColorTexture(
					SpVertexVec[2].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 1.0f)
				),
				//右下頂点
				VertexPositionColorTexture(
					SpVertexVec[3].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 1.0f)
				),
			};
			m_NumberVertexVec.push_back(NumVirtex);
		}
		GetStage()->SetSharedGameObject(L"TimerSprite", GetThis<TimerSprite>());
	}

	void TimerSprite::OnUpdate() {
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto tf = Ptr->getTF();
		auto SF = Ptr->getSF();
		if (m_TotalTime >= 0.0f) {
			if (!tf && SF) {
				m_TotalTime -= ElapsedTime;
				Ptr->setGameTime(m_TotalTime);
			}
		}
		else {
			m_TotalTime = 0.0f;
		}

		size_t Num = (size_t)m_TotalTime;
		Num = Num % 10;
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		if (m_TotalTime < -0.0f) {
			m_TotalTime = 0.0f;
			count++;
			if (count == 30) {
				auto ScenePtr = App::GetApp()->GetScene<Scene>();
				PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameOver");
			}
		}
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[Num]);
	}

	//--------------------------------------------------------------------------------------
	//	class TimerSprite2 : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TimerSprite2::TimerSprite2(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, float StartTime) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_TotalTime(StartTime)
	{}
	TimerSprite2::~TimerSprite2() {}

	//初期化
	void TimerSprite2::OnCreate() {

		m_TotalTime /= 10;
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(100.0f, 100.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"TIMER_TX");
		//透明処理
		SetAlphaActive(true);
		//スプライトの中のメッシュからバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//各数字ごとにUV値を含む頂点データを配列化しておく
		for (size_t i = 0; i < 10; i++) {
			float from = ((float)i) / 10.0f;
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
					SpVertexVec[0].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 0)
				),
				//右上頂点
				VertexPositionColorTexture(
					SpVertexVec[1].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 0)
				),
				//左下頂点
				VertexPositionColorTexture(
					SpVertexVec[2].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 1.0f)
				),
				//右下頂点
				VertexPositionColorTexture(
					SpVertexVec[3].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 1.0f)
				),
			};
			m_NumberVertexVec.push_back(NumVirtex);
		}
		GetStage()->SetSharedGameObject(L"TimerSprite2", GetThis<TimerSprite2>());
	}

	void TimerSprite2::OnUpdate() {
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto tf = Ptr->getTF();
		auto SF = Ptr->getSF();
		if (m_TotalTime > 0.0f) {
			if (!tf && SF) {
				m_TotalTime -= ElapsedTime / 10;
			}
		}
		else {
			m_TotalTime = 0.0f;
		}
		size_t Num = (size_t)m_TotalTime;
		Num = Num % 10;
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[Num]);
	}

	//--------------------------------------------------------------------------------------
	//	class TimerSprite3 : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TimerSprite3::TimerSprite3(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, float StartTime) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_TotalTime(StartTime)
	{}
	TimerSprite3::~TimerSprite3() {}

	//初期化
	void TimerSprite3::OnCreate() {

		m_TotalTime /= 100;
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(100.0f, 100.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"TIMER_TX");
		//透明処理
		SetAlphaActive(true);
		//スプライトの中のメッシュからバックアップの取得
		auto& SpVertexVec = PtrSprite->GetMeshResource()->GetBackupVerteces<VertexPositionColorTexture>();
		//各数字ごとにUV値を含む頂点データを配列化しておく
		for (size_t i = 0; i < 10; i++) {
			float from = ((float)i) / 10.0f;
			float to = from + (1.0f / 10.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
					SpVertexVec[0].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 0)
				),
				//右上頂点
				VertexPositionColorTexture(
					SpVertexVec[1].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 0)
				),
				//左下頂点
				VertexPositionColorTexture(
					SpVertexVec[2].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(from, 1.0f)
				),
				//右下頂点
				VertexPositionColorTexture(
					SpVertexVec[3].position,
					Color4(1.0f, 1.0f, 1.0f, 1.0f),
					Vector2(to, 1.0f)
				),
			};
			m_NumberVertexVec.push_back(NumVirtex);
		}
	}

	void TimerSprite3::OnUpdate() {
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto tf = Ptr->getTF();
		auto SF = Ptr->getSF();
		if (m_TotalTime > 0.0f) {
			if (!tf && SF) {
				m_TotalTime -= ElapsedTime / 100;
			}
		}
		else {
			m_TotalTime = 0.0f;
		}
		size_t Num = (size_t)m_TotalTime;
		Num = Num % 10;
		auto PtrSprite = GetComponent<PCTSpriteDraw>();
		auto MeshRes = PtrSprite->GetMeshResource();
		//動的にUV値が変わる頂点を設定する
		MeshRes->UpdateVirtexBuffer(m_NumberVertexVec[Num]);

	}

	
}
//end basecross
