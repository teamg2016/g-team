/*!
@file Scene.h
@brief シーン
*/
#pragma once

#include "stdafx.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	class Scene : public SceneBase {
		shared_ptr<MultiAudioObject> m_AudioObjectPtr;
		wstring m_NowStartMusic;
		//タイマーのカウンタ
		float m_GameTimer;

		bool m_sf = false;

		bool m_ssf = false;

		bool m_cf1 = false;

		bool m_cf2 = false;

		bool m_cf3 = false;

		bool m_cf4 = false;

		bool m_tf = false;

		bool m_gf = false;

		bool m_af = false;

		bool m_gf2 = false;

		bool m_af2 = false;

		int Score = 0;

		int Bcount = 0;


	public:
		float m_GameTotalTime = 0.0f;

		float m_GameTotalTime2 = 0.0f;
		//ステージの番号を格納
		int StageNum = 0;
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		*/
		//--------------------------------------------------------------------------------------
		Scene() :SceneBase() {}
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Scene() {}
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief イベント取得
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnEvent(const shared_ptr<Event>& event) override;


		//ゲームの時間を取得
		float getGameTime()const { return m_GameTimer; }
		void setGameTime(float time) { m_GameTimer = time; }

		//スタート状態かを取得
		float getSF()const { return m_sf; }
		void setSF(bool s) { m_sf = s; }

		float getSSF()const { return m_ssf; }
		void setSSF(bool ss) { m_ssf = ss; }

		float getCF1()const { return m_cf1; }
		void setCF1(bool c1) { m_cf1 = c1; }

		float getCF2()const { return m_cf2; }
		void setCF2(bool c2) { m_cf2 = c2; }

		float getCF3()const { return m_cf3; }
		void setCF3(bool c3) { m_cf3 = c3; }

		float getCF4()const { return m_cf4; }
		void setCF4(bool c4) { m_cf4 = c4; }

		//ゴールに入っている状態かを取得
		float getTF()const { return m_tf; }
		void setTF(bool b) { m_tf = b; }

		float getGF()const { return m_gf; }
		void setGF(bool g) { m_gf = g; }

		float getAF()const { return m_af; }
		void setAF(bool a) { m_af = a; }

		float getGF2()const { return m_gf2; }
		void setGF2(bool g2) { m_gf2 = g2; }

		float getAF2()const { return m_af2; }
		void setAF2(bool a2) { m_af2 = a2; }

		void CreateResource();

		void SetGameTotalTime(float f) {
			m_GameTotalTime = f;
		}
		float GetGameTotalTime()const {
			return m_GameTotalTime;
		}

		void SetGameTotalTime2(float f) {
			m_GameTotalTime2 = f;
		}
		float GetGameTotalTime2()const {
			return m_GameTotalTime2;
		}

		bool KEIKOKU = false;

		void ScoreUp() { Score++; }

		void ScoreReset() { Score = 0; }

		int GetScore() { return Score; }

	};

}

//end basecross
