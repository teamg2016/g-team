/*!
@file Project.h
@brief コンテンツ用のヘッダをまとめる
*/

#pragma once


#include "resource.h"

#include "ProjectShader.h"
#include "Scene.h"
#include "Character.h"
#include "Player.h"
#include "Player2.h"
#include "Enemy.h"
#include "Enemy2.h"
#include "Title.h"
#include "Timer.h"
#include "Result.h"
#include "Menu.h"
#include "Tutorial.h"
#include "GameStage1-0.h"
#include "GameStage1-1.h"
#include "GameStage1-2.h"
#include "GameStage1-3.h"
#include "GameStage1-4.h"
#include "GameStage1-5.h"
#include "GameStage1-6.h"
#include "GameStage2-1.h"
#include "GameStage2-2.h"
#include "GameStage2-3.h"
#include "GameStage2-4.h"
#include "GameStage2-5.h"
#include "GameStage3-1.h"
#include "GameStage3-2.h"
#include "GameStage3-3.h"
#include "GameStage3-5.h"
#include "GameStage5-1.h"
#include "GameStage5-2.h"
#include "GameOver.h"
#include "StageSelect1.h"
#include "StageSelect2.h"
#include "StageSelect3.h"
#include "StageSelect5.h"
#include "SelectPlate.h"

enum DataID
{
	NONE = 0,			//何も置かない
	PLAYER = 1,			//プレイヤー１
	PLAYER2 = 2,		//プレイヤー２
	GOAL = 3,			//ゴール
	BLOCKMIN = 4,		//飛び越えられる壁
	BLOCK = 5,			//ステージ１用ブロック
	DOUBLEBLOCK = 6,	//ステージ１用外壁
	RENGABLOCK = 7,		//ステージ２用ブロック
	DOUBLERENGA = 8,	//ステージ２用外壁
	PLAYERRENGA = 9,	//ステージ２用プレイヤー１とブロック
	PLAYER2RENGA = 10,	//ステージ２用プレイヤー２とブロック
	GOALRENGA = 11,		//ステージ２用ゴールとブロック
	ICEBLOCK = 12,		//ステージ３用ブロック
	DOUBLEICE = 13,		//ステージ３用外壁
	ENEMY = 14,         //敵
	ENEMY2 = 15,         //敵２
	Sphere = 16,        //反発する球
	RAINBOWBLOCK = 19,	//ステージ５用ブロック
	DOUBLERAINBOW = 20,	//ステージ５用外壁
	SCOREIEAM = 21,     //スコアアイテム
};

