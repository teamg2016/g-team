/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス実体
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void Title::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"hukaiao.png";
		App::GetApp()->RegisterTexture(L"HUKAIAO_TX", strTexture);
		strTexture = DataDir + L"BackTitle.png";
		App::GetApp()->RegisterTexture(L"BACKTITLE_TX", strTexture);
		strTexture = DataDir + L"kuro.png";
		App::GetApp()->RegisterTexture(L"KURO_TX", strTexture);
		strTexture = DataDir + L"haiiro.png";
		App::GetApp()->RegisterTexture(L"HIIRO_TX", strTexture);
		strTexture = DataDir + L"usukin.png";
		App::GetApp()->RegisterTexture(L"USUKIN_TX", strTexture);
		strTexture = DataDir + L"title.png";
		App::GetApp()->RegisterTexture(L"TITLE_TX", strTexture);
		strTexture = DataDir + L"Press A.png";
		App::GetApp()->RegisterTexture(L"PRESSA_TX", strTexture);
		strTexture = DataDir + L"Teikyo.png";
		App::GetApp()->RegisterTexture(L"TEIKYO_TX", strTexture);

		wstring DecisionWav = App::GetApp()->m_wstrRelativeDataPath + L"Decision.wav";
		App::GetApp()->RegisterWav(L"Decision", DecisionWav);

	}

	//ビューとライトの作成
	void Title::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}

	//プレートの作成
	void Title::CreatePlate() {

		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		auto BACK = CreateSharedObjectGroup(L"BACK");
		auto b = AddGameObject<Sprite>(L"BACKTITLE_TX", false,
			Vector2(w, h), Vector2(0, 800));
		b->AddComponent<Action>();
		b->SetDrawActive(false);
		BACK->IntoGroup(b);
	}

	//壁模様のスプライト作成
	void Title::CreateSprite() {
		auto TITLE = CreateSharedObjectGroup(L"TITLE");
		auto ti = AddGameObject<Sprite>(L"TITLE_TX", false,
			Vector2(1000.0f, 350.0f), Vector2(0.0f, 150.0f));
		ti->SetDrawActive(false);
		TITLE->IntoGroup(ti);
		auto ss = AddGameObject<Sprite>(L"PRESSA_TX", false,
			Vector2(600.0f, 100.0f), Vector2(0.0f, -150.0f));
		ss->SetAlphaActive(true);
		auto tx = CreateSharedObjectGroup(L"TX");
		tx->IntoGroup(ss);
	}


	//シーン変更
	void Title::SceneChange()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToMenuStage");
	}

	void Title::OnCreate() {

		auto Ptr = AddGameObject<GameObject>();
		SetSharedGameObject(L"SEManager", Ptr);

		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//プレートの作成
			CreatePlate();
			//壁模様のスプライト作成
			CreateSprite();

			//SceneChange();
			auto FB = AddGameObject<Black>();
			SetSharedGameObject(L"Black", FB);

		}
		catch (...) {
			throw;
		}

		//サウンドを登録.
		auto pMultiSoundEffect = Ptr->AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Decision");

		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		auto TEIKYO = CreateSharedObjectGroup(L"TEIKYO");
		auto t = AddGameObject<Sprite>(L"TEIKYO_TX", false,
			Vector2(w, h), Vector2(0, 0));
		t->AddComponent<Action>();
		t->SetDrawActive(true);
		TEIKYO->IntoGroup(t);
	}

	//更新
	void Title::OnUpdate() {
		Tcount++;

		if (Tcount == 120) {
			GetSharedObjectGroup(L"TEIKYO")->at(0)->GetComponent<Action>()->AddMoveBy(1.0f, Vector3(0, -800, 0));
			GetSharedObjectGroup(L"BACK")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"BACK")->at(0)->GetComponent<Action>()->AddMoveTo(1.0f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"TEIKYO")->at(0)->GetComponent<Action>()->Run();
			GetSharedObjectGroup(L"BACK")->at(0)->GetComponent<Action>()->Run();
		}

		if (Tcount <= 150) {
			GetSharedObjectGroup(L"TX")->at(0)->SetDrawActive(false);
		}

		if (Tcount >= 180) {

			auto Ptr = App::GetApp()->GetScene<Scene>();

			shared_ptr<Sprite> ss = NULL;
			ss = dynamic_pointer_cast<Sprite>(GetSharedObjectGroup(L"TX")->at(0));
			GetSharedObjectGroup(L"TITLE")->at(0)->SetDrawActive(true);

			//コントローラの取得
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected) {

				//Aボタンが押された瞬間ならステージ推移

				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
				}
				//暗転を毎回判定
				//暗転終わり
				if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
				{
					//シーン切り替え
					SceneChange();
				}

			}
			if (ss != NULL) {
				count++;
				if (count % 30 == 0) {
					if (ss->GetDrawActive() == true) {
						ss->SetDrawActive(false);
					}
					else {
						ss->SetDrawActive(true);
					}
				}
			}
		}
	}
}
//end basecross
