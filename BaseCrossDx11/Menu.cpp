/*!
@file Menu.cpp
@brief メニューステージ
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス実体
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void MenuStage::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"LevelSelect.png";
		App::GetApp()->RegisterTexture(L"LEVELSELECT_TX", strTexture);
		strTexture = DataDir + L"Easy.png";
		App::GetApp()->RegisterTexture(L"EASY_TX", strTexture);
		strTexture = DataDir + L"Normal.png";
		App::GetApp()->RegisterTexture(L"NORMAL_TX", strTexture);
		strTexture = DataDir + L"Hard.png";
		App::GetApp()->RegisterTexture(L"HARD_TX", strTexture);
		strTexture = DataDir + L"Extra.png";
		App::GetApp()->RegisterTexture(L"EXTRA_TX", strTexture);
		strTexture = DataDir + L"BackSelect.png";
		App::GetApp()->RegisterTexture(L"BACKSELECT_TX", strTexture);
		strTexture = DataDir + L"kuro.png";
		App::GetApp()->RegisterTexture(L"KURO_TX", strTexture);
		strTexture = DataDir + L"EASYS.png";
		App::GetApp()->RegisterTexture(L"EASYS_TX", strTexture); 
		strTexture = DataDir + L"NORMALS.png";
		App::GetApp()->RegisterTexture(L"NORMALS_TX", strTexture);
		strTexture = DataDir + L"HARDS.png";
		App::GetApp()->RegisterTexture(L"HARDS_TX", strTexture);
		strTexture = DataDir + L"EXTRAS.png";
		App::GetApp()->RegisterTexture(L"EXTRAS_TX", strTexture);
		strTexture = DataDir + L"EXTRAA.png";
		App::GetApp()->RegisterTexture(L"EXTRAA_TX", strTexture);
		strTexture = DataDir + L"Tutorial.png";
		App::GetApp()->RegisterTexture(L"TUTORIAL_TX", strTexture);
		strTexture = DataDir + L"TutorialS.png";
		App::GetApp()->RegisterTexture(L"TUTORIALS_TX", strTexture);

		wstring DecisionWav = App::GetApp()->m_wstrRelativeDataPath + L"Decision.wav";
		App::GetApp()->RegisterWav(L"Decision", DecisionWav);
		wstring SelectWav = App::GetApp()->m_wstrRelativeDataPath + L"Select.wav";
		App::GetApp()->RegisterWav(L"Select", SelectWav);
		wstring MissWav = App::GetApp()->m_wstrRelativeDataPath + L"Miss.wav";
		App::GetApp()->RegisterWav(L"Miss", MissWav);

	}
	void MenuStage::CreateViewLight()
	{
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}
	//スプライト作成
	void MenuStage::CreateSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<Sprite>(L"BACKSELECT_TX", false,
			Vector2(w, h), Vector2(0, 0));
		AddGameObject<Sprite>(L"LEVELSELECT_TX", false,
			Vector2(w / 3, h / 7), Vector2(0, 250));
		auto ssg = CreateSharedObjectGroup(L"SelectPlateGroup");
		auto ss = AddGameObject<Sprite>(L"EASY_TX", false,
			Vector2(w / 7, h / 7), Vector2(-250, -30));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"NORMAL_TX", false,
			Vector2(w / 7, h / 7), Vector2(0, -30));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"HARD_TX", false,
			Vector2(w / 7, h / 7), Vector2(250, -30));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"EXTRA_TX", false,
			Vector2(w / 7, h / 7), Vector2(500, -30));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"TUTORIAL_TX", false,
			Vector2(w / 7, h / 7), Vector2(-500, -30));
		ssg->IntoGroup(ss);

		auto ssg1 = CreateSharedObjectGroup(L"SelectPlateGroup1");
		auto ss1 = AddGameObject<Sprite>(L"EASYS_TX", false,
			Vector2(w / 0, h / 0), Vector2(0, -250));
		ssg1->IntoGroup(ss1);
		ss1 = AddGameObject<Sprite>(L"NORMALS_TX", false,
			Vector2(w / 0, h / 0), Vector2(0, -250));
		ssg1->IntoGroup(ss1);
		ss1 = AddGameObject<Sprite>(L"HARDS_TX", false,
			Vector2(w / 0, h / 0), Vector2(0, -250));
		ssg1->IntoGroup(ss1);
		/*ss1 = AddGameObject<Sprite>(L"EXTRAA_TX", false,
			Vector2(w / 0, h / 0), Vector2(0, -250));
		ssg1->IntoGroup(ss1);*/
		ss1 = AddGameObject<Sprite>(L"EXTRAS_TX", false,
			Vector2(w / 0, h / 0), Vector2(0, -250));
		ssg1->IntoGroup(ss1);
		ss1 = AddGameObject<Sprite>(L"TUTORIALS_TX", false,
			Vector2(w / 0, h / 0), Vector2(0, -250));
		ssg1->IntoGroup(ss1);
	}

	//シーン変更
	void MenuStage::SceneChange1()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToStageSelect");
	}

	//シーン変更
	void MenuStage::SceneChange2()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToStageSelect2");
	}

	//シーン変更
	void MenuStage::SceneChange3()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToStageSelect3");
	}

	//シーン変更
	void MenuStage::SceneChange4()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToStageSelect5");
	}

	//シーン変更
	void MenuStage::SceneChange5()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToTutorialStage");
	}

	//シーン変更
	void MenuStage::SceneChange6()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToTitle");
	}

	void MenuStage::OnCreate() {

		auto Ptr = AddGameObject<GameObject>();
		SetSharedGameObject(L"SEManager", Ptr);

		auto Ptr1 = App::GetApp()->GetScene<Scene>();

		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateSprite();

			Ptr1->setCF1(false);
			Ptr1->setCF2(false);
			Ptr1->setCF3(false);
			Ptr1->setCF4(false);

			auto FB = AddGameObject<Black>();
			SetSharedGameObject(L"Black", FB);
			auto FBIn = AddGameObject<BlackIn>();
			SetSharedGameObject(L"BlackIn", FBIn);
		}
		catch (...) {
			throw;
		}

		//サウンドを登録.
		auto pMultiSoundEffect = Ptr->AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Decision");
		pMultiSoundEffect->AddAudioResource(L"Select");
		pMultiSoundEffect->AddAudioResource(L"Miss");

	}

	//更新
	void MenuStage::OnUpdate() {

		GetSharedGameObject<BlackIn>(L"BlackIn", false)->StartBlackIn();

		if (GetSharedGameObject<BlackIn>(L"BlackIn", false)->GetBlackInFinish())
		{

			//コントローラの取得
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected) {
				//Aボタンが押された瞬間ならステージ推移
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 0) {
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 1) {
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 2) {
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
				}
				else if(CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 3)
				{
					if (EX == false) {
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Miss", 0, 0.5f);
					}
					else
					{
						GetSharedGameObject<Black>(L"Black", false)->StartBlack();
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
					}
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 4)
				{
					if (TU == false) {
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Miss", 0, 0.5f);
					}
					else
					{
						GetSharedGameObject<Black>(L"Black", false)->StartBlack();
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
					}
				}
				if (a == 0)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange1();
					}
				}
				if (a == 1)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange2();
					}
				}

				if (a == 2)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange3();
					}
				}

				if (a == 3 && EX == true)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange4();
					}
				}

				if (a == 4 && TU == true)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange5();
					}
				}

				if (CntlVec[0].fThumbLX < -0.5f) {
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Select", 0, 0.2f);
					if (!b) {
						a -= 1;
						if (a < 0) {
							a = 4;
						}
						b = true;
					}
				}
				else if (CntlVec[0].fThumbLX > 0.5f) {
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Select", 0, 0.2f);
					if (!b) {
						a += 1;
						if (a > 4) {
							a = 0;
						}
						b = true;
					}
				}
				else {
					b = false;
				}
			}
			auto ssg = GetSharedObjectGroup(L"SelectPlateGroup");
			float w = static_cast<float>(App::GetApp()->GetGameWidth());
			float h = static_cast<float>(App::GetApp()->GetGameHeight());
			for (int i = 0; i < ssg->size(); i++) {
				if (i == a) {
					ssg->at(i)->GetComponent<Transform>()->SetScale(w / 5, h / 5, 0.1);
					continue;
				}
				ssg->at(i)->GetComponent<Transform>()->SetScale(w / 7, h / 7, 0.1);
			}

			auto ssg1 = GetSharedObjectGroup(L"SelectPlateGroup1");
			for (int i = 0; i < ssg1->size(); i++) {
				if (i == a) {
					ssg1->at(i)->GetComponent<Transform>()->SetScale(w / 1.5f, h / 7, 0.1);
					continue;
				}
				ssg1->at(i)->GetComponent<Transform>()->SetScale(w / 0, h / 0, 0.1);
			}
		}
	}
}
//end basecross
