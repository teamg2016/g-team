/*!
@file Menu.h
@brief メニューステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス
	//--------------------------------------------------------------------------------------
	class TutorialStage : public Stage {
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//壁模様のスプライト作成
		void CreateSprite();

		int a = 0;

		shared_ptr<MultiAudioObject> m_AudioObjectPtr;


	public:
		//構築と破棄
		TutorialStage() :Stage() {}
		virtual ~TutorialStage() {}
		void SceneChange1();

		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
	};

}
//end basecross