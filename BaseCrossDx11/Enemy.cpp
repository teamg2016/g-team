#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class HeadEnemy : public GameObject;
	//	用途: 追いかける配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	HeadEnemy::HeadEnemy(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_BaseY(m_StartPos.y),
		m_StateChangeSize(1.0f)
	{
	}
	HeadEnemy::~HeadEnemy() {}

	//初期化
	void HeadEnemy::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//Rigidbodyをつける
		auto PtrRigid = AddComponent<Rigidbody>();
		
		//横部分のみ反発
		//反発係数は0.5（半分）
		PtrRigid->SetReflection(0.5f);
		//Seek操舵
		auto PtrSeek = AddComponent<SeekSteering>();
		
		
		//Seekは無効にしておく
		PtrSeek->SetUpdateActive(false);
		//Arrive操舵
		auto PtrArrive = AddComponent<ArriveSteering>();
		//Arriveは無効にしておく
		PtrArrive->SetUpdateActive(false);

		//SPの衝突判定をつける
		auto PtrColl = AddComponent<CollisionSphere>();
		//横部分のみ反発
		PtrColl->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//		PtrColl->SetDrawActive(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(L"TRACE2_TX");
		//透明処理をする
		SetAlphaActive(true);
		m_BehaviorMachine = ObjectFactory::Create<BehaviorMachine<HeadEnemy>>(GetThis<HeadEnemy>());
		m_BehaviorMachine->AddBehavior<HeadStop>(L"Stop");
		m_BehaviorMachine->AddBehavior<HeadJumpSeek>(L"JumpSeek");
		m_BehaviorMachine->Reset(L"Stop");
	}

	//ユーティリティ関数群
	Vector3 HeadEnemy::GetPlayerPosition() const {
		//もしプレイヤーが初期化化されてない場合には、Vector3(0,m_BaseY,0)を返す
		Vector3 PlayerPos(0, m_BaseY, 0);
		auto PtrPlayer = GetStage()->GetSharedGameObject<Player>(L"Player", false);
		if (PtrPlayer) {
			PlayerPos = PtrPlayer->GetComponent<Transform>()->GetPosition();
		}
		return PlayerPos;
	}

	float HeadEnemy::GetPlayerLength() const {
		auto MyPos = GetComponent<Transform>()->GetPosition();
		auto LenVec = GetPlayerPosition() - MyPos;
		return LenVec.Length();
	}

	//操作
	void HeadEnemy::OnUpdate() {
		m_BehaviorMachine->Update();
		
	}

	void HeadEnemy::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
	}

	//回転を進行方向に向かせる
	void HeadEnemy::RotToFront() {
		auto PtrRigidbody = GetComponent<Rigidbody>();
		//回転の更新
		//Velocityの値で、回転を変更する
		//これで進行方向を向くようになる
		auto PtrTransform = GetComponent<Transform>();
		Vector3 Velocity = PtrRigidbody->GetVelocity();
		if (Velocity.Length() > 0.0f) {
			Vector3 Temp = Velocity;
			Temp.Normalize();
			float ToAngle = atan2(Temp.x, Temp.z);
			Quaternion Qt;
			Qt.RotationRollPitchYaw(0, ToAngle, 0);
			Qt.Normalize();
			//現在の回転を取得
			Quaternion NowQt = PtrTransform->GetQuaternion();
			//現在と目標を補間（10分の1）
			NowQt.Slerp(NowQt, Qt, 0.1f);
			PtrTransform->SetQuaternion(NowQt);
		}
	}


	void HeadEnemy::OnLastUpdate() {
		m_BehaviorMachine->Update2();
	}

	//--------------------------------------------------------------------------------------
	//	静止行動
	//--------------------------------------------------------------------------------------
	void HeadStop::Enter(const shared_ptr<HeadEnemy>& Obj) {
		auto PtrRigidbody = Obj->GetComponent<Rigidbody>();
		PtrRigidbody->SetVelocity(0, 0, 0);
	}
	void HeadStop::Execute(const shared_ptr<HeadEnemy>& Obj) {
		if (Obj->GetPlayerLength() < 10.0f) {
			Obj->GetBehaviorMachine()->Push(L"JumpSeek");
		}
	}

	void HeadStop::WakeUp(const shared_ptr<HeadEnemy>& Obj) {
		auto PtrRigidbody = Obj->GetComponent<Rigidbody>();
		PtrRigidbody->SetVelocity(0, 0, 0);
	}

	void HeadStop::Exit(const shared_ptr<HeadEnemy>& Obj) {
	}

	//--------------------------------------------------------------------------------------
	//	ジャンプして追跡行動
	//--------------------------------------------------------------------------------------
	void HeadJumpSeek::Enter(const shared_ptr<HeadEnemy>& Obj) {
		auto PtrArrive = Obj->GetComponent<ArriveSteering>();
		PtrArrive->SetUpdateActive(true);
		PtrArrive->SetDecl(0.0);
		PtrArrive->SetTargetPosition(Obj->GetPlayerPosition());
	}
	void HeadJumpSeek::Execute(const shared_ptr<HeadEnemy>& Obj) {
		auto PtrArrive = Obj->GetComponent<ArriveSteering>();
		PtrArrive->SetTargetPosition(Obj->GetPlayerPosition());
		//重力
		auto PtrGravity = Obj->GetComponent<Gravity>();
		if (PtrGravity->GetGravityVelocity().Length() <= 0) {
			//プレイヤーとの位置関係
			if (Obj->GetPlayerLength() >= 15.0f) {
				Obj->GetBehaviorMachine()->Pop();
			}
		}
	}

	void HeadJumpSeek::Execute2(const shared_ptr<HeadEnemy>& Obj) {
		Obj->RotToFront();
	}

	void HeadJumpSeek::Exit(const shared_ptr<HeadEnemy>& Obj) {
		auto PtrArrive = Obj->GetComponent<ArriveSteering>();
		PtrArrive->SetUpdateActive(false);
		auto PtrTrans = Obj->GetComponent<Transform>();
		auto Pos = PtrTrans->GetPosition();
		Pos.y = 0.125f;
		PtrTrans->SetPosition(Pos);
	}

	//--------------------------------------------------------------------------------------
	//	敵
	//--------------------------------------------------------------------------------------
	//構築と破棄
	CellSeekEnemy1::CellSeekEnemy1(const shared_ptr<Stage>& StagePtr,
		const shared_ptr<StageCellMap>& CellMap,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_CelMap(CellMap),
		m_Scale(Vector3(0.6f,0.6f,0.6f)),
		m_StartRotation(Rotation),
		m_StartPosition(Position),
		m_CellIndex(-1),
		m_NextCellIndex(-1),
		m_TargetCellIndex(-1)
	{
	}
	CellSeekEnemy1::~CellSeekEnemy1() {}

	bool CellSeekEnemy1::SearchPlayer() {
		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			auto PathPtr = GetComponent<PathSearch>();
			auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
			auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
			m_CellPath.clear();
			//パス検索をかける
			if (PathPtr->SearchCell(PlayerPos, m_CellPath)) {
				//検索が成功した
				m_CellIndex = 0;
				m_TargetCellIndex = m_CellPath.size() - 1;
				if (m_CellIndex == m_TargetCellIndex) {
					//すでに同じセルにいる
					m_NextCellIndex = m_CellIndex;
				}
				else {
					//離れている
					m_NextCellIndex = m_CellIndex + 1;

				}
				return true;
			}
			else {
				//失敗した
				m_CellIndex = -1;
				m_NextCellIndex = -1;
				m_TargetCellIndex = -1;
			}
		}
		return false;
	}

	bool CellSeekEnemy1::DefaultBehavior() {
		auto PtrRigid = GetComponent<Rigidbody>();
		auto Velo = PtrRigid->GetVelocity();
		Velo *= 0.95f;
		PtrRigid->SetVelocity(Velo);
		auto MapPtr = m_CelMap.lock();
		if (MapPtr) {
			auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
			auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
			CellIndex PlayerCell;
			if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
				return false;
			}
		}
		return true;
	}


	bool CellSeekEnemy1::SeekBehavior() {
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto MyPos = GetComponent<Transform>()->GetPosition();
		auto TotalPos = PlayerPos - MyPos;
		auto MapPtr = m_CelMap.lock();

		if (MapPtr) {
			//プレイヤーと敵の距離
			if (TotalPos.Length() <= 15.0f) {
				if (SearchPlayer()) {
					auto PtrSeek = GetComponent<SeekSteering>();
					if (m_NextCellIndex == 0) {
						auto PtrRigid = GetComponent<Rigidbody>();
						auto Velo = PtrRigid->GetVelocity();
						Velo *= 0.95f;
						PtrRigid->SetVelocity(Velo);
						PlayerPos.y = m_StartPosition.y;
						PtrSeek->SetTargetPosition(PlayerPos);
					}
					else {
						if (Vector3EX::Length(MyPos - PlayerPos) <= 3.0f) {
							auto PtrRigid = GetComponent<Rigidbody>();
							auto Velo = PtrRigid->GetVelocity();
							Velo *= 0.95f;
							PtrRigid->SetVelocity(Velo);
						}
						AABB ret;
						MapPtr->FindAABB(m_CellPath[m_NextCellIndex], ret);
						auto Pos = ret.GetCenter();
						Pos.y = m_StartPosition.y;
						PtrSeek->SetTargetPosition(Pos);
					}
					return true;
				}
				else {
					CellIndex PlayerCell;
					if (MapPtr->FindCell(PlayerPos, PlayerCell)) {
						auto PtrSeek = GetComponent<SeekSteering>();
						AABB ret;
						MapPtr->FindAABB(PlayerCell, ret);
						auto Pos = ret.GetCenter();
						Pos.y = m_StartPosition.y;
						PtrSeek->SetTargetPosition(Pos);
						return true;
					}
				}
			}

		}
		return false;
	}


	//初期化
	void CellSeekEnemy1::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_StartPosition);
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_StartRotation);
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//Rigidbodyをつける
		auto PtrRigid = AddComponent<Rigidbody>();
		//反発係数は0.5（半分）
		//		PtrRigid->SetReflection(0.5f);
		auto PtrSeek = AddComponent<SeekSteering>();
		PtrSeek->SetUpdateActive(false);
		//パス検索
		auto MapPtr = m_CelMap.lock();
		if (!MapPtr) {
			throw BaseException(
				L"セルマップが不定です",
				L"if (!MapPtr) ",
				L" Enemy::OnCreate()"
			);
		}
		auto PathPtr = AddComponent<PathSearch>(MapPtr);

		//SPの衝突判定をつける
		auto PtrColl = AddComponent<CollisionSphere>();
		PtrColl->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(L"TRACE2_TX");
		//透明処理をする
		SetAlphaActive(true);

		m_StateMachine = make_shared<StateMachine<CellSeekEnemy1>>(GetThis<CellSeekEnemy1>());
		m_StateMachine->ChangeState(EnemyDefault1::Instance());
	}

	//更新
	void CellSeekEnemy1::OnUpdate() {
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		if (ScenePtr->getSF()) {
			//スタートしてたら
			//ステートマシンのUpdateを行う
			//この中でステートの切り替えが行われる
			m_StateMachine->Update();
		}
	}

	void CellSeekEnemy1::OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec) {

	}

	void CellSeekEnemy1::OnCollisionExit(vector<shared_ptr<GameObject>>& OtherVec) {

	}

	//回転を進行方向に向かせる
	void CellSeekEnemy1::OnLastUpdate() {
		auto PtrRigidbody = GetComponent<Rigidbody>();
		//回転の更新
		//Velocityの値で、回転を変更する
		//これで進行方向を向くようになる
		auto PtrTransform = GetComponent<Transform>();
		Vector3 Velocity = PtrRigidbody->GetVelocity();
		if (Velocity.Length() > 0.0f) {
			Vector3 Temp = Velocity;
			Temp.Normalize();
			float ToAngle = atan2(Temp.x, Temp.z);
			Quaternion Qt;
			Qt.RotationRollPitchYaw(0, ToAngle, 0);
			Qt.Normalize();
			//現在の回転を取得
			Quaternion NowQt = PtrTransform->GetQuaternion();
			//現在と目標を補間（10分の1）
			NowQt.Slerp(NowQt, Qt, 0.1f);
			PtrTransform->SetQuaternion(NowQt);
		}
	}

	//--------------------------------------------------------------------------------------
	///	デフォルトステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	IMPLEMENT_SINGLETON_INSTANCE(EnemyDefault1)

		void EnemyDefault1::Enter(const shared_ptr<CellSeekEnemy1>& Obj) {
	}

	void EnemyDefault1::Execute(const shared_ptr<CellSeekEnemy1>& Obj) {
		if (!Obj->DefaultBehavior()) {
			Obj->GetStateMachine()->ChangeState(EnemySeek1::Instance());
		}
	}
	void EnemyDefault1::Exit(const shared_ptr<CellSeekEnemy1>& Obj) {
	}

	//--------------------------------------------------------------------------------------
	///	プレイヤーを追いかけるステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	IMPLEMENT_SINGLETON_INSTANCE(EnemySeek1)

		void EnemySeek1::Enter(const shared_ptr<CellSeekEnemy1>& Obj) {
		auto PtrSeek = Obj->GetComponent<SeekSteering>();
		PtrSeek->SetUpdateActive(true);
	}

	void EnemySeek1::Execute(const shared_ptr<CellSeekEnemy1>& Obj) {
		if (!Obj->SeekBehavior()) {
				Obj->GetStateMachine()->ChangeState(EnemyDefault1::Instance());
		}
	}

	void EnemySeek1::Exit(const shared_ptr<CellSeekEnemy1>& Obj) {
		auto PtrSeek = Obj->GetComponent<SeekSteering>();
		PtrSeek->SetUpdateActive(false);
	}

}