
/*!
@file Scene.cpp
@brief シーン実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------

	void Scene::OnCreate() {
		try {
			CreateResource();
			wstring strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Title.wav";
			App::GetApp()->RegisterWav(L"Title", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Menu.wav";
			App::GetApp()->RegisterWav(L"Menu", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Tutorial.wav";
			App::GetApp()->RegisterWav(L"Tutorial", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Main.wav";
			App::GetApp()->RegisterWav(L"Main", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Win.wav";
			App::GetApp()->RegisterWav(L"Win", strMusic);
			strMusic = App::GetApp()->m_wstrRelativeDataPath + L"Lose.wav";
			App::GetApp()->RegisterWav(L"Lose", strMusic);

			//オーディオの初期化
			m_AudioObjectPtr = ObjectFactory::Create<MultiAudioObject>();
			m_AudioObjectPtr->AddAudioResource(L"Title");
			m_AudioObjectPtr->AddAudioResource(L"Menu");
			m_AudioObjectPtr->AddAudioResource(L"Tutorial");
			m_AudioObjectPtr->AddAudioResource(L"Main");
			m_AudioObjectPtr->AddAudioResource(L"Win");
			m_AudioObjectPtr->AddAudioResource(L"Lose");

			m_NowStartMusic = L"";

			//最初のアクティブステージの設定
			m_AudioObjectPtr->Start(L"Title", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Title";

			ResetActiveStage<Title>();
		}
		catch (...) {
			throw;
		}
	}

	void Scene::OnEvent(const shared_ptr<Event>& event) {

		setAF(false);
		setAF2(false);
		setGF(false);
		setGF2(false);

		if (event->m_MsgStr == L"ToTitle") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Title", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Title";
			//最初のアクティブステージの設定
			ResetActiveStage<Title>();
		}
		else if (event->m_MsgStr == L"ToMenuStage") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Menu", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Menu";
			//最初のアクティブステージの設定
			ResetActiveStage<MenuStage>();
		}
		else if (event->m_MsgStr == L"ToStageSelect") {
			//最初のアクティブステージの設定
			ResetActiveStage<StageSelect>();
		}
		else if (event->m_MsgStr == L"ToStageSelect2") {
			//最初のアクティブステージの設定
			ResetActiveStage<StageSelect2>();
		}
		else if (event->m_MsgStr == L"ToStageSelect3") {
			//最初のアクティブステージの設定
			ResetActiveStage<StageSelect3>();
		}
		else if (event->m_MsgStr == L"ToStageSelect5") {
			//最初のアクティブステージの設定
			ResetActiveStage<StageSelect5>();
		}

		else if (event->m_MsgStr == L"ToResult") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Win", XAUDIO2_NO_LOOP_REGION, 0.2f);
			m_NowStartMusic = L"Win";
			//最初のアクティブステージの設定
			ResetActiveStage<Result>();
		}
		else if (event->m_MsgStr == L"ToGameOver") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Lose", XAUDIO2_NO_LOOP_REGION, 0.5f);
			m_NowStartMusic = L"Lose";
			//最初のアクティブステージの設定
			ResetActiveStage<GameOver>();
		}
		else if (event->m_MsgStr == L"ToTutorialStage") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Tutorial", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Tutorial";
			//最初のアクティブステージの設定
			ResetActiveStage<TutorialStage>();
		}
		else if (event->m_MsgStr == L"ToGameStage1-0") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage1−0>();
		}

		else if (event->m_MsgStr == L"ToGameStage1-1") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage1−1>();
		}

		else if (event->m_MsgStr == L"ToGameStage1-2") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage1−2>();
		}
		else if (event->m_MsgStr == L"ToGameStage1-3") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage1−3>();
		}
		else if (event->m_MsgStr == L"ToGameStage1-4") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage1−4>();
		}
		else if (event->m_MsgStr == L"ToGameStage1-5") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage1−5>();
		}

		else if (event->m_MsgStr == L"ToGameStage2-1") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage2−1>();
		}
		else if (event->m_MsgStr == L"ToGameStage2-2") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage2−2>();
		}
		else if (event->m_MsgStr == L"ToGameStage2-3") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage2−3>();
		}
		else if (event->m_MsgStr == L"ToGameStage2-4") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage2−4>();
		}
		else if (event->m_MsgStr == L"ToGameStage2-5") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage2−5>();
		}

		else if (event->m_MsgStr == L"ToGameStage3-1") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage3−1>();
		}
		else if (event->m_MsgStr == L"ToGameStage3-2") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage3−2>();
		}
		else if (event->m_MsgStr == L"ToGameStage3-3") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage3−3>();
		}
		else if (event->m_MsgStr == L"ToGameStage3-5") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage3−5>();
		}

		else if (event->m_MsgStr == L"ToGameStage5-1") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage5−1>();
		}
		else if (event->m_MsgStr == L"ToGameStage5-2") {
			if (m_NowStartMusic != L"") {
				m_AudioObjectPtr->Stop(m_NowStartMusic);
			}
			m_AudioObjectPtr->Start(L"Main", XAUDIO2_LOOP_INFINITE, 0.2f);
			m_NowStartMusic = L"Main";
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage5−2>();
		}

	}

	void Scene::CreateResource() {
	}

}
//end basecross
