/*!
@file Result.h
@brief タイトル
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	数字のスクエア
	//--------------------------------------------------------------------------------------
	class NumberSquare : public GameObject {
		
		//このオブジェクトのみで使用するスクエアメッシュ
		shared_ptr<MeshResource> m_SquareMeshResource;
		//背番号
		size_t m_Number;
	public:
		//構築と破棄
		NumberSquare(const shared_ptr<Stage>& StagePtr);
		virtual ~NumberSquare();
		//初期化
		virtual void OnCreate() override;
	};


	//--------------------------------------------------------------------------------------
	//	メニューステージクラス
	//--------------------------------------------------------------------------------------
	class Result : public Stage {
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		//void CreatePlate();
		//壁模様のスプライト作成
		void CreateSprite();

		void CreateScoreSprite();

		void CreateScore();

		int a = 0;
		bool b = false;

		shared_ptr<MultiAudioObject> m_AudioObjectPtr;


	public:
		//構築と破棄
		Result() :Stage() {}
		virtual ~Result() {}
		void SceneChange1();
		void SceneChange2();
		void SceneChange3();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;

		//Result判定を行う
		void ResultDecision();
	};

}
//end basecross
