/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage1−6 : public Stage {
		//カメラの注視点からカメラ位置のベクトル
		Vector3 m_AtToEyeVec;
		//カメラズームの最大距離
		float m_ArmMax;
		//カメラズームの最小距離
		float m_ArmMin;
		//ズームスピード(ターン単位)
		float m_ZoomSpeed;

		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//固定のボックスの作成
		void CreateFixedBox();

		//上下移動しているボックスの作成
		void CreateMoveBox2();
		//上下移動しているボックスの作成
		void CreateMoveBox3();

		void CreateMoveBoxL();
		void CreateMoveBoxR();
		void CreateMoveBoxDown();
		void CreateMoveBoxDown2();
		void CreateMoveBoxDown3();
		void CreateMoveBoxDown5();


		void CreateSpark();

		shared_ptr<MultiAudioObject> m_AudioObjectPtr;

		void CreateTimerSprite();
		void CreateTimerSprite2();
		void CreateTimerSprite3();

		Vector2 m_PlayerSpawnCell;

		size_t m_sColSize;
		size_t m_sRowSize;

		vector<vector<size_t>>m_MapDataVec;
		vector<vector<size_t>>m_MapDataVec2;

		void CreateRoad();

		float m_Timer = 0;
		float ElapsedTime = App::GetApp()->GetElapsedTime();


	public:
		//構築と破棄
		GameStage1−6() :Stage(),
			m_AtToEyeVec(0.0f, 15.0f, -18.0f),
			m_ArmMax(25.0f),
			m_ArmMin(3.0f),
			m_ZoomSpeed(0.1f)
		{}
		virtual ~GameStage1−6() {}
		void SceneChange();
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		virtual void OnLastUpdate()override;
		//virtual void OnUpdate()override;

	};


}
//end basecross
