/*!
@file Result.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス実体
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void Result::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"Result.png";
		App::GetApp()->RegisterTexture(L"RESULT_TX", strTexture);
		strTexture = DataDir + L"Clear.jpg";
		App::GetApp()->RegisterTexture(L"CLEAR_TX", strTexture);
		strTexture = DataDir + L"NextStage.png";
		App::GetApp()->RegisterTexture(L"NEXT_TX", strTexture);
		strTexture = DataDir + L"NextStageSelect.png";
		App::GetApp()->RegisterTexture(L"NEXTSTAGESELECT_TX", strTexture);
		strTexture = DataDir + L"Title2.png";
		App::GetApp()->RegisterTexture(L"TITLE2_TX", strTexture);
		strTexture = DataDir + L"Clock.png";
		App::GetApp()->RegisterTexture(L"TIME_TX", strTexture);
		strTexture = DataDir + L"Item.png";
		App::GetApp()->RegisterTexture(L"ITEM_TX", strTexture);
		strTexture = DataDir + L"Rank.png";
		App::GetApp()->RegisterTexture(L"RANK_TX", strTexture);
		strTexture = DataDir + L"Timer4.png";
		App::GetApp()->RegisterTexture(L"TIMER_TX", strTexture);

		//test
		strTexture = DataDir + L"S.png";
		App::GetApp()->RegisterTexture(L"S_TX", strTexture);
		//
		strTexture = DataDir + L"A.png";
		App::GetApp()->RegisterTexture(L"A_TX", strTexture);
		//
		strTexture = DataDir + L"B.png";
		App::GetApp()->RegisterTexture(L"B_TX", strTexture);
		//
		strTexture = DataDir + L"C.png";
		App::GetApp()->RegisterTexture(L"C_TX", strTexture);
		//
		strTexture = DataDir + L"D.png";
		App::GetApp()->RegisterTexture(L"D_TX", strTexture);

	}
	//--------------------------------------------------------------------------------------
	//	数字のスクエア
	//--------------------------------------------------------------------------------------
	//構築と破棄
	NumberSquare::NumberSquare(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}
	NumberSquare::~NumberSquare() {}

	//初期化
	void NumberSquare::OnCreate() {

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(40.0f, 350.0f, 1.0f);
		PtrTransform->SetScale(100.0f, 100.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

	
		float ScenePtr = App::GetApp()->GetScene<Scene>()->GetGameTotalTime();
		
			//変更できるスクエアリソースを作成

			//頂点配列
			vector<VertexPositionNormalTexture> vertices;
			//インデックスを作成するための配列
			vector<uint16_t> indices;
			//Squareの作成(ヘルパー関数を利用)
			MeshUtill::CreateSquare(1.0f, vertices, indices);
			//UV値の変更
			float from = ScenePtr;
			float to = from + (1.0f / 10.0f);
			//左上頂点
			vertices[0].textureCoordinate = Vector2(from, 0);
			//右上頂点
			vertices[1].textureCoordinate = Vector2(to, 0);
			//左下頂点
			vertices[2].textureCoordinate = Vector2(from, 1.0f);
			//右下頂点
			vertices[3].textureCoordinate = Vector2(to, 1.0f);
			//頂点の型を変えた新しい頂点を作成
			vector<VertexPositionColorTexture> new_vertices;
			for (auto& v : vertices) {
				VertexPositionColorTexture nv;
				nv.position = v.position;
				nv.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
				nv.textureCoordinate = v.textureCoordinate;
				new_vertices.push_back(nv);
			}
			//新しい頂点を使ってメッシュリソースの作成
			m_SquareMeshResource = MeshResource::CreateMeshResource<VertexPositionColorTexture>(new_vertices, indices, true);

			auto DrawComp = AddComponent<PCTStaticDraw>();
			DrawComp->SetMeshResource(m_SquareMeshResource);
			DrawComp->SetTextureResource(L"TIMER_TX");
			SetAlphaActive(true);
			//SetDrawLayer(1);
		

	}
	

	void Result::CreateViewLight()
	{
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}
	//スプライト作成
	void Result::CreateSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<Sprite>(L"CLEAR_TX", false,
			Vector2(w, h), Vector2(0, 0));

		ResultDecision();

		AddGameObject<Sprite>(L"RESULT_TX", false,
			Vector2(w / 3, h / 7), Vector2(0, 280));
		AddGameObject<Sprite>(L"TIME_TX", false,
			Vector2(w / 8, h / 5), Vector2(-370, 100));
		AddGameObject<Sprite>(L"ITEM_TX", false,
			Vector2(w / 8, h / 5), Vector2(-370, -100));
		AddGameObject<Sprite>(L"RANK_TX", false,
			Vector2(w / 4, h / 5), Vector2(300, 120));
		auto ssg = CreateSharedObjectGroup(L"SelectPlateGroup");
		auto ss = AddGameObject<Sprite>(L"NEXT_TX", false,
			Vector2(w / 7, h / 7), Vector2(-300, -280));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"NEXTSTAGESELECT_TX", false,
			Vector2(w / 6, h / 7), Vector2(0, -280));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"TITLE2_TX", false,
			Vector2(w / 7, h / 7), Vector2(300, -280));
		ssg->IntoGroup(ss);
	}

	void Result::CreateScoreSprite() {
		AddGameObject<ScoreSprite>(3,
			L"TIMER_TX",
			true,
			Vector2(350.0f, 200.0f),
			Vector2(-100.0f, 100.0f));
	}

	void Result::CreateScore() {
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto s = AddGameObject<NumberSprite>(0, Vector2(-70, -100), Vector2(180, 180), 10);
		s->SetNum(Ptr->GetScore());
	}

	//シーン変更
	void Result::SceneChange1()
	{
		auto Ptr = App::GetApp()->GetScene<Scene>();
		int StageNo = Ptr->StageNum;
		if (StageNo == 10) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-1");
		}
		if (StageNo == 11) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-2");
		}
		if (StageNo == 12) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-3");
		}
		if (StageNo == 13) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-4");
		}
		if (StageNo == 14) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-5");
		}
		if (StageNo == 15) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-1");
		}
		if (StageNo == 21) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-2");
		}
		if (StageNo == 22) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-3");
		}
		if (StageNo == 23) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-4");
		}
		if (StageNo == 24) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-5");
		}
		if (StageNo == 25) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage3-1");
		}
		if (StageNo == 31) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage3-2");
		}
		if (StageNo == 32) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage5-1");
		}
		if (StageNo == 51) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage5-2");
		}
		if (StageNo == 52) {
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-1");
		}
	}

	//シーン変更
	void Result::SceneChange2()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToMenuStage");
	}

	//シーン変更
	void Result::SceneChange3()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToTitle");
	}

	void Result::OnCreate() {

		auto Ptr = AddGameObject<GameObject>();
		SetSharedGameObject(L"SEManager", Ptr);

		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateSprite();

			CreateScoreSprite();

			CreateScore();

			auto FB = AddGameObject<Black>();
			SetSharedGameObject(L"Black", FB);
			auto FBIn = AddGameObject<BlackIn>();
			SetSharedGameObject(L"BlackIn", FBIn);
		}
		catch (...) {
			throw;
		}

		//サウンドを登録.
		auto pMultiSoundEffect = Ptr->AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Decision");
		pMultiSoundEffect->AddAudioResource(L"Select");

	}

	//更新
	void Result::OnUpdate() {

		GetSharedGameObject<BlackIn>(L"BlackIn", false)->StartBlackIn();

		if (GetSharedGameObject<BlackIn>(L"BlackIn", false)->GetBlackInFinish())
		{

			//コントローラの取得
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected) {
				//Aボタンが押された瞬間ならステージ推移
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
				}
				if (a == 0)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange1();
					}
				}
				if (a == 1)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange2();
					}
				}
				if (a == 2)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange3();
					}
				}

				if (CntlVec[0].fThumbLX < -0.5f) {
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Select", 0, 0.2f);
					if (!b) {
						a -= 1;
						if (a < 0) {
							a = 2;
						}
						b = true;
					}
				}
				else if (CntlVec[0].fThumbLX > 0.5f) {
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Select", 0, 0.2f);
					if (!b) {
						a += 1;
						if (a > 2) {
							a = 0;
						}
						b = true;
					}
				}
				else {
					b = false;
				}
			}
			auto ssg = GetSharedObjectGroup(L"SelectPlateGroup");
			float w = static_cast<float>(App::GetApp()->GetGameWidth());
			float h = static_cast<float>(App::GetApp()->GetGameHeight());
			for (int i = 0; i < ssg->size(); i++) {
				if (i == a) {
					ssg->at(i)->GetComponent<Transform>()->SetScale(w / 5, h / 5, 0.1);
					continue;
				}
				ssg->at(i)->GetComponent<Transform>()->SetScale(w / 7, h / 7, 0.1);
			}
		}
		auto ScorPtr = GetSharedGameObject<ScoreSprite>(L"ScoreSprite");
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		ScorPtr->SetScore(ScenePtr->m_GameTotalTime);
	}

	void Result::ResultDecision()
	{
		auto Ptr = App::GetApp()->GetScene<Scene>();
		int StageNo = Ptr->StageNum;
		float CrearTime = Ptr->GetGameTotalTime();
		int Score = Ptr->GetScore();

		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		if (StageNo == 10)
		{
			if (CrearTime >= 40 && Score >= 4)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 3)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 2)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 1)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 11)
		{
			if (CrearTime >= 50 && Score >= 5)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 3)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 2)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 1)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 12)
		{
			if (CrearTime >= 50 && Score >= 5)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 3)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 2)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 1)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 13)
		{
			if (CrearTime >= 50 && Score >= 5)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 3)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 2)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 1)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 14)
		{
			if (CrearTime >= 50 && Score >= 5)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 3)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 2)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 1)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 15)
		{
			if (CrearTime >= 50 && Score >= 5)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 3)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 2)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 1)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 21)
		{
			if (CrearTime >= 50 && Score >= 10)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 6)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 4)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 2)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 22)
		{
			if (CrearTime >= 50 && Score >= 10)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 6)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 4)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 2)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 23)
		{
			if (CrearTime >= 50 && Score >= 10)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 6)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 4)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 2)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 24)
		{
			if (CrearTime >= 50 && Score >= 10)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 6)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 4)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 2)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 25)
		{
			if (CrearTime >= 50 && Score >= 10)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 30 && Score >= 6)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 20 && Score >= 4)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 10 && Score >= 2)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 31)
		{
			if (CrearTime >= 60 && Score >= 15)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 40 && Score >= 9)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 30 && Score >= 6)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 20 && Score >= 3)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 32)
		{
			if (CrearTime >= 60 && Score >= 15)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 40 && Score >= 9)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 30 && Score >= 6)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 20 && Score >= 3)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 51)
		{
			if (CrearTime >= 70 && Score >= 40)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 50 && Score >= 30)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 40 && Score >= 20)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 30 && Score >= 10)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		if (StageNo == 52)
		{
			if (CrearTime >= 70 && Score >= 40)
			{
				AddGameObject<Sprite>(L"S_TX", false,
					Vector2(w / 5, h / 2), Vector2(300, -80));
			}
			else
				if (CrearTime >= 50 && Score >= 30)
				{
					AddGameObject<Sprite>(L"A_TX", false,
						Vector2(w / 5, h / 2), Vector2(300, -80));
				}
				else
					if (CrearTime >= 40 && Score >= 20)
					{
						AddGameObject<Sprite>(L"B_TX", false,
							Vector2(w / 5, h / 2), Vector2(300, -80));
					}
					else
						if (CrearTime >= 30 && Score >= 10)
						{
							AddGameObject<Sprite>(L"C_TX", false,
								Vector2(w / 5, h / 2), Vector2(300, -80));
						}
						else
							if (CrearTime >= 0 && Score >= 0)
							{
								AddGameObject<Sprite>(L"D_TX", false,
									Vector2(w / 5, h / 2), Vector2(300, -80));
							}
		}
		
	}

}
//end basecross
