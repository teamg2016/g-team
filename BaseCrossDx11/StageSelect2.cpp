/*!
@file Menu.cpp
@brief メニューステージ
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス実体
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void StageSelect2::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"Stageselect.png";
		App::GetApp()->RegisterTexture(L"STAGESELECT_TX", strTexture);
		strTexture = DataDir + L"1.png";
		App::GetApp()->RegisterTexture(L"1_TX", strTexture);
		strTexture = DataDir + L"2.png";
		App::GetApp()->RegisterTexture(L"2_TX", strTexture);
		strTexture = DataDir + L"3.png";
		App::GetApp()->RegisterTexture(L"3_TX", strTexture);
		strTexture = DataDir + L"4.png";
		App::GetApp()->RegisterTexture(L"4_TX", strTexture);
		strTexture = DataDir + L"5.png";
		App::GetApp()->RegisterTexture(L"5_TX", strTexture);
		strTexture = DataDir + L"return.png";
		App::GetApp()->RegisterTexture(L"RETURN_TX", strTexture);

		strTexture = DataDir + L"sogen.jpg";
		App::GetApp()->RegisterTexture(L"SOGEN_TX", strTexture);

		wstring DecisionWav = App::GetApp()->m_wstrRelativeDataPath + L"Decision.wav";
		App::GetApp()->RegisterWav(L"Decision", DecisionWav);
		wstring SelectWav = App::GetApp()->m_wstrRelativeDataPath + L"Select.wav";
		App::GetApp()->RegisterWav(L"Select", SelectWav);
		wstring MissWav = App::GetApp()->m_wstrRelativeDataPath + L"Miss.wav";
		App::GetApp()->RegisterWav(L"Miss", MissWav);

	}

	void StageSelect2::CreateViewLight()
	{
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}
	//スプライト作成
	void StageSelect2::CreateSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<Sprite>(L"SOGEN_TX", false,
			Vector2(w, h), Vector2(0, 0));
		AddGameObject<Sprite>(L"STAGESELECT_TX", false,
			Vector2(w / 3, h / 7), Vector2(0, 250));
		auto ssg = CreateSharedObjectGroup(L"SelectPlateGroup");
		auto ss = AddGameObject<Sprite>(L"1_TX", false,
			Vector2(w / 7, h / 6), Vector2(-300, 70));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"2_TX", false,
			Vector2(w / 7, h / 6), Vector2(0, 70));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"3_TX", false,
			Vector2(w / 7, h / 6), Vector2(300, 70));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"4_TX", false,
			Vector2(w / 7, h / 6), Vector2(-300, -130));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"5_TX", false,
			Vector2(w / 7, h / 6), Vector2(0, -130));
		ssg->IntoGroup(ss);
		ss = AddGameObject<Sprite>(L"RETURN_TX", false,
			Vector2(w / 6, h / 7), Vector2(300, -130));
		ssg->IntoGroup(ss);
	}

	//シーン変更
	void StageSelect2::SceneChange1()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-1");
	}

	//シーン変更
	void StageSelect2::SceneChange2()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-2");
	}

	//シーン変更
	void StageSelect2::SceneChange3()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-3");
	}

	//シーン変更
	void StageSelect2::SceneChange4()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-4");
	}

	//シーン変更
	void StageSelect2::SceneChange5()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage2-5");
	}

	//シーン変更
	void StageSelect2::SceneChange6()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToMenuStage");
	}

	void StageSelect2::OnCreate() {
		auto Ptr = AddGameObject<GameObject>();
		SetSharedGameObject(L"SEManager", Ptr);
		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateSprite();

			auto FB = AddGameObject<Black>();
			SetSharedGameObject(L"Black", FB);
			auto FBIn = AddGameObject<BlackIn>();
			SetSharedGameObject(L"BlackIn", FBIn);
		}
		catch (...) {
			throw;
		}
		//サウンドを登録.
		auto pMultiSoundEffect = Ptr->AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Decision");
		pMultiSoundEffect->AddAudioResource(L"Select");
		pMultiSoundEffect->AddAudioResource(L"Miss");
	}

	//更新
	void StageSelect2::OnUpdate() {

		GetSharedGameObject<BlackIn>(L"BlackIn", false)->StartBlackIn();

		if (GetSharedGameObject<BlackIn>(L"BlackIn", false)->GetBlackInFinish())
		{

			//コントローラの取得
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected) {
				//Aボタンが押された瞬間ならステージ推移
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 0) {
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 1) {
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 2)
				{
					if (N3 == false) {
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Miss", 0, 0.5f);
					}
					else
					{
						GetSharedGameObject<Black>(L"Black", false)->StartBlack();
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
					}
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 3)
				{
					if (N4 == false) {
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Miss", 0, 0.5f);
					}
					else
					{
						GetSharedGameObject<Black>(L"Black", false)->StartBlack();
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
					}
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 4)
				{
					if (N5 == false) {
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Miss", 0, 0.5f);
					}
					else
					{
						GetSharedGameObject<Black>(L"Black", false)->StartBlack();
						auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
						auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
						pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
					}
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 5) {
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
				}

				if (a == 0)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange1();
					}
				}
				if (a == 1)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange2();
					}
				}

				if (a == 2 && N3 == true)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange3();
					}
				}

				if (a == 3 && N4 == true)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange4();
					}
				}

				if (a == 4 && N5 == true)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange5();
					}
				}

				if (a == 5)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange6();
					}
				}
			}
			if (CntlVec[0].fThumbLX < -0.5f) {
				auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
				auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Select", 0, 0.2f);
				if (!b) {
					a -= 1;
					if (a < 0) {
						a = 5;
					}
					b = true;
				}
			}
			else if (CntlVec[0].fThumbLX > 0.5f) {
				auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
				auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Select", 0, 0.2f);
				if (!b) {
					a += 1;
					if (a > 5) {
						a = 0;
					}
					b = true;
				}
			}
			else {
				b = false;
			}
		}
		auto ssg = GetSharedObjectGroup(L"SelectPlateGroup");
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());
		for (int i = 0; i < ssg->size(); i++) {
			if (i == a) {
				ssg->at(i)->GetComponent<Transform>()->SetScale(w / 5, h / 4, 0.1);
				continue;
			}
			ssg->at(i)->GetComponent<Transform>()->SetScale(w / 7, h / 6, 0.1);
		}
	}

}
//end basecross
