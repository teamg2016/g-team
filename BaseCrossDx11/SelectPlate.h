#pragma once
#include "stdafx.h"

namespace basecross {


	class SelectPlate : public GameObject {
		Vector3 m_pos;
		Vector3 m_scl;
		wstring pouse;
		wstring m_scene;
		int m_Num;
		int m_autosideNum;
		bool m_Select;
	public:
		SelectPlate(const shared_ptr<Stage>& stage, const wstring & plate, const wstring & scene, int nam,const Vector3& pos,const Vector3& scl);
		
		//������
		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		void SetOutSide(int num)
		{
			m_autosideNum = num;
		}

		void SetSelect(bool sel)
		{
			m_Select = sel;
		}

	};
}