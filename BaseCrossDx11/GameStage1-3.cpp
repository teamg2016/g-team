/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------


	//リソースの作成
	void GameStage1−3::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"siro.png";
		App::GetApp()->RegisterTexture(L"SIRO_TX", strTexture);
		strTexture = DataDir + L"kuro.png";
		App::GetApp()->RegisterTexture(L"KURO_TX", strTexture);
		strTexture = DataDir + L"haiiro.png";
		App::GetApp()->RegisterTexture(L"HAIIRO_TX", strTexture);
		strTexture = DataDir + L"nizi.jpg";
		App::GetApp()->RegisterTexture(L"NIZI_TX", strTexture);
		strTexture = DataDir + L"kimidori.png";
		App::GetApp()->RegisterTexture(L"KIMIDORI_TX", strTexture);
		strTexture = DataDir + L"sougen.png";
		App::GetApp()->RegisterTexture(L"SOUGEN_TX", strTexture);
		strTexture = DataDir + L"renga.png";
		App::GetApp()->RegisterTexture(L"RENGA_TX", strTexture);
		strTexture = DataDir + L"tutirenga.png";
		App::GetApp()->RegisterTexture(L"TUTI_TX", strTexture);
		strTexture = DataDir + L"spark.png";
		App::GetApp()->RegisterTexture(L"SPARK_TX", strTexture);
		strTexture = DataDir + L"START.png";
		App::GetApp()->RegisterTexture(L"START_TX", strTexture);
		strTexture = DataDir + L"Timepinch.png";
		App::GetApp()->RegisterTexture(L"NOKORI_TX", strTexture);
		strTexture = DataDir + L"Goal.png";
		App::GetApp()->RegisterTexture(L"GOAL_TX", strTexture);
		strTexture = DataDir + L"Setumei.png";
		App::GetApp()->RegisterTexture(L"SETUMEI_TX", strTexture);
		strTexture = DataDir + L"trace2.png";
		App::GetApp()->RegisterTexture(L"TRACE2_TX", strTexture);
		strTexture = DataDir + L"33.png";
		App::GetApp()->RegisterTexture(L"33_TX", strTexture);
		strTexture = DataDir + L"22.png";
		App::GetApp()->RegisterTexture(L"22_TX", strTexture);
		strTexture = DataDir + L"11.png";
		App::GetApp()->RegisterTexture(L"11_TX", strTexture);
		strTexture = DataDir + L"kiiro.png";
		App::GetApp()->RegisterTexture(L"KIIRO_TX", strTexture);
		strTexture = DataDir + L"Clock.png";
		App::GetApp()->RegisterTexture(L"TIME1_TX", strTexture);
		strTexture = DataDir + L"Item.png";
		App::GetApp()->RegisterTexture(L"ITEM_TX", strTexture);
		strTexture = DataDir + L"Score1.png";
		App::GetApp()->RegisterTexture(L"SCORE1_TX", strTexture);
		strTexture = DataDir + L"rengawaku.png";
		App::GetApp()->RegisterTexture(L"WAKU_TX", strTexture);
		strTexture = DataDir + L"Pause.png";
		App::GetApp()->RegisterTexture(L"PAUSE_TX", strTexture);
		strTexture = DataDir + L"RetryP.png";
		App::GetApp()->RegisterTexture(L"RETRYP_TX", strTexture);
		strTexture = DataDir + L"Stageselect.png";
		App::GetApp()->RegisterTexture(L"STAGESELECT_TX", strTexture);
		strTexture = DataDir + L"Title2.png";
		App::GetApp()->RegisterTexture(L"TITLE2_TX", strTexture);
		strTexture = DataDir + L"A0.png";
		App::GetApp()->RegisterTexture(L"A0_TX", strTexture);
		strTexture = DataDir + L"A1.png";
		App::GetApp()->RegisterTexture(L"A1_TX", strTexture);
		strTexture = DataDir + L"A2.png";
		App::GetApp()->RegisterTexture(L"A2_TX", strTexture);
		strTexture = DataDir + L"A3.png";
		App::GetApp()->RegisterTexture(L"A3_TX", strTexture);
		strTexture = DataDir + L"A4.png";
		App::GetApp()->RegisterTexture(L"A4_TX", strTexture);
		strTexture = DataDir + L"A5.png";
		App::GetApp()->RegisterTexture(L"A5_TX", strTexture);
		strTexture = DataDir + L"A6.png";
		App::GetApp()->RegisterTexture(L"A6_TX", strTexture);
		strTexture = DataDir + L"A7.png";
		App::GetApp()->RegisterTexture(L"A7_TX", strTexture);
		strTexture = DataDir + L"A8.png";
		App::GetApp()->RegisterTexture(L"A8_TX", strTexture);
		strTexture = DataDir + L"A9.png";
		App::GetApp()->RegisterTexture(L"A9_TX", strTexture);
		strTexture = DataDir + L"Retry.png";
		App::GetApp()->RegisterTexture(L"RETRY_TX", strTexture);
		strTexture = DataDir + L"StageSelect1.png";
		App::GetApp()->RegisterTexture(L"SELECT_TX", strTexture);
		strTexture = DataDir + L"Title3.png";
		App::GetApp()->RegisterTexture(L"TITLE3_TX", strTexture);
		strTexture = DataDir + L"SwitchA.png";
		App::GetApp()->RegisterTexture(L"SA_TX", strTexture);
		strTexture = DataDir + L"SwitchB.png";
		App::GetApp()->RegisterTexture(L"SB_TX", strTexture);
		strTexture = DataDir + L"SwitchC.png";
		App::GetApp()->RegisterTexture(L"SC_TX", strTexture);

		wstring JumpWav = App::GetApp()->m_wstrRelativeDataPath + L"Jump.wav";
		App::GetApp()->RegisterWav(L"Jump", JumpWav);
		wstring BodyWav = App::GetApp()->m_wstrRelativeDataPath + L"Bodyblow.wav";
		App::GetApp()->RegisterWav(L"Body", BodyWav);
		wstring WarningWav = App::GetApp()->m_wstrRelativeDataPath + L"warning.wav";
		App::GetApp()->RegisterWav(L"Warning", WarningWav);
		wstring ClearWav = App::GetApp()->m_wstrRelativeDataPath + L"Clear.wav";
		App::GetApp()->RegisterWav(L"Clear", ClearWav);
		wstring StartWav = App::GetApp()->m_wstrRelativeDataPath + L"Start.wav";
		App::GetApp()->RegisterWav(L"Start", StartWav);
		wstring CountWav = App::GetApp()->m_wstrRelativeDataPath + L"Count.wav";
		App::GetApp()->RegisterWav(L"Count", CountWav);
		wstring SwitchWav = App::GetApp()->m_wstrRelativeDataPath + L"Switch.wav";
		App::GetApp()->RegisterWav(L"Switch", SwitchWav);
		wstring GateWav = App::GetApp()->m_wstrRelativeDataPath + L"Gate.wav";
		App::GetApp()->RegisterWav(L"Gate", GateWav);
		wstring ItemWav = App::GetApp()->m_wstrRelativeDataPath + L"Item.wav";
		App::GetApp()->RegisterWav(L"Item", ItemWav);
		wstring SelectWav = App::GetApp()->m_wstrRelativeDataPath + L"Select.wav";
		App::GetApp()->RegisterWav(L"Select", SelectWav);
		wstring DecisionWav = App::GetApp()->m_wstrRelativeDataPath + L"Decision.wav";
		App::GetApp()->RegisterWav(L"Decision", DecisionWav);
		wstring PauseWav = App::GetApp()->m_wstrRelativeDataPath + L"Pause.wav";
		App::GetApp()->RegisterWav(L"Pause", PauseWav);

		strTexture = DataDir + L"Timer4.png";
		App::GetApp()->RegisterTexture(L"TIMER_TX", strTexture);

	}



	//ビューとライトの作成
	void GameStage1−3::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vector3(0.0f, 10.0f, -20.0f));
		PtrCamera->SetAt(m_AtToEyeVec);
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}


	//プレートの作成
	void GameStage1−3::CreatePlate() {
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(200.0f, 200.0f, 1.0f),
			Qt,
			Vector3(0.0f, 0.0f, 0.0f)
		);
		PtrTrans->SetScale(100.0f, 150.0f, 1.0f);
		PtrTrans->SetQuaternion(Qt);
		PtrTrans->SetPosition(0.0f, 0.0f, 20.0f);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"SOUGEN_TX");

		Ptr->AddComponent<CollisionRect>();
	}

	//CSVマッピングによるステージ作成
	void GameStage1−3::CreateRoad()
	{

		auto BoxGroup = CreateSharedObjectGroup(L"BoxGroup");

		//CSVファイルからゲームステージの選択
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath + L"GameStage\\";

		wstring FileName = MediaPath + L"Stage_E3.csv";

		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(FileName);
		//ファイルがあるかどうか
		if (!GameStageCsv.ReadCsv()) {
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません",
				FileName,
				L"選択された場所にdアクセスできません"

			);
		}

		const int iDataSizeRow = 0; //データが0行目から始まるように定数
		vector<wstring>StageMapVec; //ワーク用のベクター配列
		GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);

		//行、列の取得
		m_sRowSize = static_cast<size_t>(_wtoi(StageMapVec[0].c_str())); //Row=行
		m_sColSize = static_cast<size_t>(_wtoi(StageMapVec[1].c_str())); //Col=列

																		 //マップデータの配列作成
		m_MapDataVec = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分
		m_MapDataVec2 = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分


		//配列の初期化
		for (size_t i = 0; i < m_sRowSize; i++) {
			for (size_t j = 0; j < m_sColSize; j++) {
				m_MapDataVec[i][j] = 0;	//0で初期化
				m_MapDataVec2[i][j] = 0;	//0で初期化
			}
		}

		//1行目からステージが定義されている
		const int iDataStartRow = 1;

		auto CellGroup = CreateSharedObjectGroup(L"SeekGroup");
		auto CellGroup2 = CreateSharedObjectGroup(L"SeekGroup2");

		if (m_sRowSize > 0) {
			for (size_t i = 0; i < m_sRowSize; i++) {
				GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec); //START + iだから最初は２が入る
				for (size_t j = 0; j < m_sColSize; j++) {
					const int iNum = _wtoi(StageMapVec[j].c_str()); //列から取得したwstringをintに

					float ihalf = (float)i / 2.0f;
					float jhalf = (float)j / 2.0f;
					//マップデータ配列に格納
					m_MapDataVec[i][j] = iNum;
					//マップデータ配列に格納
					m_MapDataVec2[i][j] = iNum;
					//配置されるオブジェクトの基準のスケール
					float ObjectScale = 1.0f;


					//基準スケール
					Vector3 Scale(ObjectScale, ObjectScale, ObjectScale);
					//キャラクタースケール
					Vector3 Scale_Chara(ObjectScale*0.95f, ObjectScale*0.95f, ObjectScale*0.95f);

					//基準ローテーション
					Vector3 Rotation(0.0f, 0.0f, 0.0f);
					//床ローテーション
					Vector3 Rotationxx(45.0f, 0.0f, 0.0f);
					//列の半分の数
					float ColHalf = (float)m_sColSize / 2.0f;
					//行の半分の数
					float RowHalf = (float)m_sRowSize / 2.0f;

					//ステージの真ん中をX、Z軸０にしたいのでポジションのXを行の半分の値分引いて、Zを列の半分の値でプラスしてあげる
					//基準ポジション
					Vector3 Pos(static_cast<float>(j) - ColHalf, 0.0f, -static_cast<float>(i) + RowHalf);


					//Block用のポジション
					Vector3 BlockPos(static_cast<float>(j) - ColHalf, 1.0f, -static_cast<float>(i) + RowHalf);

					//Character用のポジション
					Vector3 CharaPos(static_cast<float>(j) - ColHalf, 20.0f, -static_cast<float>(i) + RowHalf);

					//Enemy用のポジション
					Vector3 EnemyPos(static_cast<float>(j) - ColHalf, 1.0f, -static_cast<float>(i) + RowHalf);

					//Score用のポジション
					Vector3 ScorePos(static_cast<float>(j) - ColHalf, 0.2f, -static_cast<float>(i) + RowHalf);

					switch (iNum)
					{
					case DataID::NONE://何も置かない
						break;
					case DataID::DOUBLEBLOCK://外壁
					{
						auto BoxPtr = AddGameObject<CSVFixedBox>(Scale, Rotation, BlockPos);
						BoxGroup->IntoGroup(BoxPtr);
					}
					break;
					case DataID::BLOCKMIN://飛び越えられるブロック
						AddGameObject<CSVFixedBoxMin>(Scale, Rotation, Pos);
						break;

					case DataID::PLAYER://プレイヤーを生成
						SetSharedGameObject(L"Player", AddGameObject<Player>(CharaPos, 30.0f));
						break;

					case DataID::PLAYER2://プレイヤー2を生成
						SetSharedGameObject(L"Player2", AddGameObject<Player2>(CharaPos, 30.0f));
						break;

					case DataID::GOAL://ゴールを生成
						AddGameObject<MoveTORUS>(Scale, Rotation, Pos);
						break;

					case DataID::SCOREIEAM://スコアアイテムを生成
						AddGameObject<SphereObject>(ScorePos);
						break;

					case DataID::ENEMY://敵1を生成
					{
						auto MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap");
						vector< vector<Vector3> > Vec1 = {
							{
								Vector3(1.0f, 1.0f, 1.0f),
								Vector3(0.0f, 0.0f, 0.0f),
								EnemyPos
							},
						};
						//オブジェクトの作成
						for (auto v : Vec1) {
							CellGroup->IntoGroup(AddGameObject<CellSeekEnemy1>(MapPtr, v[0], v[1], v[2]));
						}
					}
					break;

					case DataID::ENEMY2://敵2を生成
					{
						auto MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap");
						vector< vector<Vector3> > Vec1 = {
							{
								Vector3(1.0f, 1.0f, 1.0f),
								Vector3(0.0f, 0.0f, 0.0f),
								EnemyPos
							},
						};
						//オブジェクトの作成
						for (auto v : Vec1) {
							CellGroup2->IntoGroup(AddGameObject<CellSeekEnemy2>(MapPtr, v[0], v[1], v[2]));
						}
					}
					break;

					default:
						assert(!"不正な値が入ってます,CSV関係のところな気がします");
					}


				}
			}
		}
		//--------------------------------------------------------
		//最適化,
		//********************************************************
		////ステージデータのコピーを作成,
		auto & WorkVec = m_MapDataVec;
		auto & WorkVec2 = m_MapDataVec2;

	}

	//固定のボックスの作成
	void GameStage1−3::CreateFixedBox() {
		//配列の初期化
		vector< vector<Vector3> > Vec = {
		};
		//オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<FixedBox>(v[0], v[1], v[2]);
		}
	}



	//上下移動しているボックスの作成
	void GameStage1−3::CreateMoveBox2() {
		CreateSharedObjectGroup(L"MoveBox2");
	}

	//上下移動しているボックスの作成
	void GameStage1−3::CreateMoveBox3() {
		CreateSharedObjectGroup(L"MoveBox3");
	}

	//セルマップの作成
	void GameStage1−3::CreateStageCellMap() {
		float  PieceSize = 2.0f;
		auto Ptr = AddGameObject<StageCellMap>(Vector3(-13.0f, 0, -39.0f), PieceSize, 13, 40);
		SetSharedGameObject(L"StageCellMap", Ptr);
	}


	//固定のボックスのコストをセルマップに反映
	void GameStage1−3::SetCellMapCost() {
		auto BoxGroup = GetSharedObjectGroup(L"BoxGroup");
		auto MapPtr = GetSharedGameObject<StageCellMap>(L"StageCellMap");
		if (MapPtr) {
			//セルマップからセルの配列を取得
			auto& CellVec = MapPtr->GetCellVec();
			//ボックスグループからボックスの配列を取得
			auto& BoxVec = BoxGroup->GetGroupVector();
			vector<AABB> ObjectsAABBVec;
			for (auto& v : BoxVec) {
				auto BoxPtr = dynamic_pointer_cast<GameObject>(v.lock());
				if (BoxPtr) {
					auto ColPtr = BoxPtr->GetComponent<CollisionObb>(false);
					if (ColPtr) {
						//ボックスの衝突判定かラッピングするAABBを取得して保存
						ObjectsAABBVec.push_back(ColPtr->GetWrappingAABB());
					}
				}
			}
			//セル配列からセルをスキャン
			for (auto& v : CellVec) {
				for (auto& v2 : v) {
					for (auto& vObj : ObjectsAABBVec) {
						if (HitTest::AABB_AABB_NOT_EQUAL(v2.m_PieceRange, vObj)) {
							//ボックスのABBとNOT_EQUALで衝突判定
							v2.m_Cost = -1;
							break;
						}
					}
				}
			}
		}
	}

	//描画マネージャ
	void GameStage1−3::CreateCSVFixedBoxManager() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"tutirenga.png";

		auto Ptr = AddGameObject<CSVFixedBoxManager>(strTexture);

		SetSharedGameObject(L"CSVFixedBoxManager", Ptr);


	}

	//スパークの作成
	void GameStage1−3::CreateSpark() {
		auto MultiSparkPtr = AddGameObject<MultiSpark>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	//スパークの作成
	void GameStage1−3::CreateSpark2() {
		auto MultiSparkPtr2 = AddGameObject<MultiSpark2>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark2", MultiSparkPtr2);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	//スパークの作成
	void GameStage1−3::CreateSpark3() {
		auto MultiSparkPtr3 = AddGameObject<MultiSpark3>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark3", MultiSparkPtr3);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	//タイマーの作成
	void GameStage1−3::CreateTimerSprite() {
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto Timer = Ptr->getGameTime();
		//配列の初期化
		vector<Vector3> Vec = {
			{ -65.0f, 350.0f, -1.0f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<TimerSprite>(v, Timer);
		}
	}

	void GameStage1−3::CreateTimerSprite2() {
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto Timer = Ptr->getGameTime();
		//配列の初期化
		vector<Vector3> Vec = {
			{ -115.0f, 350.0f, -1.0f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<TimerSprite2>(v, Timer);
		}
	}
	void GameStage1−3::CreateTimerSprite3() {
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto Timer = Ptr->getGameTime();
		//配列の初期化
		vector<Vector3> Vec = {
			{ -165.0f, 350.0f, -1.0f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<TimerSprite3>(v, Timer);
		}
	}

	//スコアを表示
	void GameStage1−3::CreateScore() {
		auto Ptr = App::GetApp()->GetScene<Scene>();
		Ptr->ScoreReset();
		auto scoreptr = AddGameObject<NumberSprite>(0, Vector2(180, 350), Vector2(100, 100), 10);
		SetSharedGameObject(L"SCORE", scoreptr);
	}

	//シーン変更
	void GameStage1−3::SceneChange()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToResult");
	}

	void GameStage1−3::OnCreate() {

		CreateSharedObjectGroup(L"PlateGroup");
		m_ContlFlag = true;
		m_Count = 1;
		Selectflag = false;

		try {
			auto Ptr = App::GetApp()->GetScene<Scene>();

			//ステージ番号の格納
			Ptr->StageNum = 13;

			//時間の設定
			Ptr->setGameTime(100.0f);

			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//プレートの作成
			CreatePlate();

			//シェアドグループを作成
			CreateSharedObjectGroup(L"MoveTORUS");
			//セルマップの作成
			CreateStageCellMap();

			//CSVでオブジェクト作成
			CreateRoad();
			//固定のボックスの作成
			CreateFixedBox();
			//上下移動しているボックスの作成
			CreateMoveBox2();
			//上下移動しているボックスの作成
			CreateMoveBox3();

			//ボックスのコストをセルマップに反映
			SetCellMapCost();
			//描画マネージャ
			CreateCSVFixedBoxManager();

			CreateSpark();
			CreateSpark2();
			CreateSpark3();

			CreateTimerSprite();
			CreateTimerSprite2();
			CreateTimerSprite3();

			//スコア
			CreateScore();

			CreateSelect1();

			auto FB = AddGameObject<Black>();
			SetSharedGameObject(L"Black", FB);
			auto FBIn = AddGameObject<BlackIn>();
			SetSharedGameObject(L"BlackIn", FBIn);

			//サウンドを登録.
			auto Ptrgo = AddGameObject<GameObject>();
			auto pMultiSoundEffect = Ptrgo->AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->AddAudioResource(L"Warning");
			pMultiSoundEffect->AddAudioResource(L"Clear");
			pMultiSoundEffect->AddAudioResource(L"Start");
			pMultiSoundEffect->AddAudioResource(L"Count");
			pMultiSoundEffect->AddAudioResource(L"Switch");
			pMultiSoundEffect->AddAudioResource(L"Gate");
			pMultiSoundEffect->AddAudioResource(L"Select");
			pMultiSoundEffect->AddAudioResource(L"Decision");
			pMultiSoundEffect->AddAudioResource(L"Pause");
			SetSharedGameObject(L"SoundManager", Ptrgo);

		}
		catch (...) {
			throw;
		}
		//UI
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		auto Count3 = CreateSharedObjectGroup(L"Count3");
		auto s = AddGameObject<Sprite>(L"33_TX", false,
			Vector2(w / 3, h / 2), Vector2(0, 500));
		s->AddComponent<Action>();
		s->SetDrawActive(false);
		Count3->IntoGroup(s);

		auto Count2 = CreateSharedObjectGroup(L"Count2");
		auto ss = AddGameObject<Sprite>(L"22_TX", false,
			Vector2(w / 3, h / 2), Vector2(0, 500));
		ss->AddComponent<Action>();
		ss->SetDrawActive(false);
		Count2->IntoGroup(ss);

		auto Count1 = CreateSharedObjectGroup(L"Count1");
		auto sss = AddGameObject<Sprite>(L"11_TX", false,
			Vector2(w / 3, h / 2), Vector2(0, 500));
		sss->AddComponent<Action>();
		sss->SetDrawActive(false);
		Count1->IntoGroup(sss);

		auto Start = CreateSharedObjectGroup(L"Start");
		auto ssss = AddGameObject<Sprite>(L"START_TX", false,
			Vector2(w / 2, h / 3), Vector2(0, 500));
		ssss->AddComponent<Action>();
		ssss->SetDrawActive(false);
		Start->IntoGroup(ssss);

		auto A0 = CreateSharedObjectGroup(L"A0");
		auto a0 = AddGameObject<Sprite>(L"A0_TX", false,
			Vector2(w / 2, h / 3), Vector2(0, 500));
		a0->AddComponent<Action>();
		a0->SetDrawActive(false);
		A0->IntoGroup(a0);

		auto A1 = CreateSharedObjectGroup(L"A1");
		auto a1 = AddGameObject<Sprite>(L"A1_TX", false,
			Vector2(w / 4, h / 3), Vector2(0, 500));
		a1->AddComponent<Action>();
		a1->SetDrawActive(false);
		A1->IntoGroup(a1);

		auto A2 = CreateSharedObjectGroup(L"A2");
		auto a2 = AddGameObject<Sprite>(L"A2_TX", false,
			Vector2(w / 4, h / 3), Vector2(0, 500));
		a2->AddComponent<Action>();
		a2->SetDrawActive(false);
		A2->IntoGroup(a2);

		auto A3 = CreateSharedObjectGroup(L"A3");
		auto a3 = AddGameObject<Sprite>(L"A3_TX", false,
			Vector2(w / 4, h / 3), Vector2(0, 500));
		a3->AddComponent<Action>();
		a3->SetDrawActive(false);
		A3->IntoGroup(a3);

		auto A4 = CreateSharedObjectGroup(L"A4");
		auto a4 = AddGameObject<Sprite>(L"A4_TX", false,
			Vector2(w / 4, h / 3), Vector2(0, 500));
		a4->AddComponent<Action>();
		a4->SetDrawActive(false);
		A4->IntoGroup(a4);

		auto A5 = CreateSharedObjectGroup(L"A5");
		auto a5 = AddGameObject<Sprite>(L"A5_TX", false,
			Vector2(w / 4, h / 3), Vector2(0, 500));
		a5->AddComponent<Action>();
		a5->SetDrawActive(false);
		A5->IntoGroup(a5);

		auto A6 = CreateSharedObjectGroup(L"A6");
		auto a6 = AddGameObject<Sprite>(L"A6_TX", false,
			Vector2(w / 4, h / 3), Vector2(0, 500));
		a6->AddComponent<Action>();
		a6->SetDrawActive(false);
		A6->IntoGroup(a6);

		auto A7 = CreateSharedObjectGroup(L"A7");
		auto a7 = AddGameObject<Sprite>(L"A7_TX", false,
			Vector2(w / 4, h / 3), Vector2(0, 500));
		a7->AddComponent<Action>();
		a7->SetDrawActive(false);
		A7->IntoGroup(a7);

		auto A8 = CreateSharedObjectGroup(L"A8");
		auto a8 = AddGameObject<Sprite>(L"A8_TX", false,
			Vector2(w / 4, h / 3), Vector2(0, 500));
		a8->AddComponent<Action>();
		a8->SetDrawActive(false);
		A8->IntoGroup(a8);

		auto A9 = CreateSharedObjectGroup(L"A9");
		auto a9 = AddGameObject<Sprite>(L"A9_TX", false,
			Vector2(w / 4, h / 3), Vector2(0, 500));
		a9->AddComponent<Action>();
		a9->SetDrawActive(false);
		A9->IntoGroup(a9);

		auto Goal = CreateSharedObjectGroup(L"Goal");
		auto g = AddGameObject<Sprite>(L"GOAL_TX", false,
			Vector2(w, h / 2), Vector2(0, 0));
		g->AddComponent<Action>();
		g->SetDrawActive(false);
		Goal->IntoGroup(g);

		auto Time = CreateSharedObjectGroup(L"Time");
		auto Ts = AddGameObject<Sprite>(L"TIME1_TX", false,
			Vector2(w / 20, h / 15), Vector2(-240, 350));
		Start->IntoGroup(Ts);

		auto Score = CreateSharedObjectGroup(L"Score");
		auto Ss = AddGameObject<Sprite>(L"ITEM_TX", false,
			Vector2(w / 20, h / 15), Vector2(50, 350));
		Start->IntoGroup(Ss);

		auto Waku1 = CreateSharedObjectGroup(L"Waku1");
		auto Ws1 = AddGameObject<Sprite>(L"WAKU_TX", false,
			Vector2(w / 7, h / 8), Vector2(-110, 350));
		Start->IntoGroup(Ws1);

		auto Waku2 = CreateSharedObjectGroup(L"Waku2");
		auto Ws2 = AddGameObject<Sprite>(L"WAKU_TX", false,
			Vector2(w / 9, h / 8), Vector2(160, 350));
		Start->IntoGroup(Ws2);

		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto SSF = Ptr->getSSF();
		Ptr->setSSF(false);
	}

	void GameStage1−3::CreateSelect1() {

		auto aaaa = AddGameObject<Sprite>(L"PAUSE_TX", true, Vector2(600, 680), Vector2(0, -50));
		SetSharedGameObject(L"PauseBack", aaaa);
		aaaa->SetDrawActive(false);
		AddGameObject<SelectPlate>(L"RETRYP_TX", L"ToGameStage1-3", 1, Vector3(0, 10, 0), Vector3(200, 100, 0));
		AddGameObject<SelectPlate>(L"STAGESELECT_TX", L"ToMenuStage", 2, Vector3(0, -120, 0), Vector3(200, 100, 0));
		AddGameObject<SelectPlate>(L"TITLE2_TX", L"ToTitle", 3, Vector3(0, -250, 0), Vector3(200, 100, 0));
	}

	void GameStage1−3::PlateSelect()
	{
		auto Cntl = App::GetApp()->GetInputDevice().GetControlerVec();
		auto PausePtr = GetSharedGameObject<Sprite>(L"PauseBack");
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto PtrTime = Ptr->getGameTime();
		if (PF1 == true) {
			if (Cntl[0].wPressedButtons & XINPUT_GAMEPAD_START && PtrTime >= 11.0f)
			{
				auto Ptrgo = GetSharedGameObject<GameObject>(L"SoundManager");
				auto pMultiSoundEffect = Ptrgo->GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Pause", 0, 0.5f);

				auto P1Speed = GetSharedGameObject<Player>(L"Player");
				auto P2Speed = GetSharedGameObject<Player2>(L"Player2");
				P1Speed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);
				P2Speed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);
				auto Ptr = App::GetApp()->GetScene<Scene>();
				auto PtrSeek1 = GetSharedObjectGroup(L"SeekGroup");
				auto PtrSeek2 = GetSharedObjectGroup(L"SeekGroup2");

				if (PausePtr->GetDrawActive())
				{
					PausePtr->SetDrawActive(false);
				}
				else
				{
					PausePtr->SetDrawActive(true);

				}
				if (Selectflag)
				{
					Selectflag = false;
					Ptr->setSF(true);
					for (auto s : PtrSeek1->GetGroupVector()) {
						dynamic_pointer_cast<CellSeekEnemy1>(s.lock())->SetUpdateActive(true);
					}
					for (auto s2 : PtrSeek2->GetGroupVector()) {
						dynamic_pointer_cast<CellSeekEnemy2>(s2.lock())->SetUpdateActive(true);
					}
					P1Speed->AddComponent<Rigidbody>()->SetMaxSpeed(30.0f);
					P2Speed->AddComponent<Rigidbody>()->SetMaxSpeed(30.0f);
				}
				else
				{
					Selectflag = true;
					Ptr->setSF(false);
					for (auto s : PtrSeek1->GetGroupVector()) {
						dynamic_pointer_cast<CellSeekEnemy1>(s.lock())->SetUpdateActive(false);
					}
					for (auto s2 : PtrSeek2->GetGroupVector()) {
						dynamic_pointer_cast<CellSeekEnemy2>(s2.lock())->SetUpdateActive(false);
					}
					P1Speed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);
					P2Speed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);
				}
			}
			if (Cntl[0].wPressedButtons & XINPUT_GAMEPAD_A&&m_Count == 1 && Selectflag)
			{
				auto Ptrgo = GetSharedGameObject<GameObject>(L"SoundManager");
				auto pMultiSoundEffect = Ptrgo->GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
				auto Ptr = App::GetApp()->GetScene<Scene>();
				Selectflag = false;
				count = 10.0f;
				Ptr->setCF1(false);
				Ptr->setCF2(false);
				Ptr->setCF3(false);
				Ptr->setCF4(false);
			}
			if (m_ContlFlag&&Selectflag)
			{
				if (Cntl[0].fThumbLY > 0.5f)
				{
					auto Ptrgo = GetSharedGameObject<GameObject>(L"SoundManager");
					auto pMultiSoundEffect = Ptrgo->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Select", 0, 0.5f);
					m_Count--;
					m_ContlFlag = false;
					if (m_Count < 1)
					{
						m_Count = 3;
					}
				}
				if (Cntl[0].fThumbLY < -0.5f)
				{
					auto Ptrgo = GetSharedGameObject<GameObject>(L"SoundManager");
					auto pMultiSoundEffect = Ptrgo->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Select", 0, 0.5f);
					m_Count++;
					m_ContlFlag = false;
					if (m_Count > 3)
					{
						m_Count = 1;
					}
				}
			}
			if (!m_ContlFlag)
			{
				if (Cntl[0].fThumbLY == 0.0f)
				{

					m_ContlFlag = true;
				}
			}
		}

		auto Group = GetSharedObjectGroup(L"PlateGroup");

		for (auto s : Group->GetGroupVector())
		{
			auto Plate = dynamic_pointer_cast<SelectPlate>(s.lock());

			if (Selectflag) {

				Plate->SetDrawActive(true);
				Plate->SetSelect(true);
				Plate->SetOutSide(m_Count);
			}
			else
			{
				PausePtr->SetDrawActive(false);
				Plate->SetDrawActive(false);
				Plate->SetSelect(false);

			}

		}

	}

	void GameStage1−3::OnUpdate() {

		PlateSelect();
		GetSharedGameObject<BlackIn>(L"BlackIn", false)->StartBlackIn();

		if (GetSharedGameObject<BlackIn>(L"BlackIn", false)->GetBlackInFinish())
		{

			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			//前回のターンからの時間
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			if (CntlVec[0].bConnected) {
				//アームの変更
				//現在のアームの長さを得る
				float ArmLen = m_AtToEyeVec.Length();
				//Dパッド下
				if (CntlVec[0].wButtons & XINPUT_GAMEPAD_DPAD_DOWN) {
					//カメラ位置を引く
					ArmLen += m_ZoomSpeed;
					if (m_ArmMax <= ArmLen) {
						ArmLen = m_ArmMax;
					}
				}
				//Dパッド上
				if (CntlVec[0].wButtons & XINPUT_GAMEPAD_DPAD_UP) {
					//カメラ位置を寄る
					ArmLen -= m_ZoomSpeed;
					if (m_ArmMin >= ArmLen) {
						ArmLen = m_ArmMin;
					}
				}
				//ベクトルを正規化
				m_AtToEyeVec.Normalize();
				//ベクトルにアームの長さを設定
				m_AtToEyeVec *= ArmLen;
			}

			//
			auto PtrCamera = dynamic_pointer_cast<Camera>(GetView()->GetTargetCamera());
			auto PtrPlayer1 = GetSharedGameObject<Player>(L"Player");
			auto PtrPlayer2 = GetSharedGameObject<Player2>(L"Player2");
			if (PtrCamera && PtrPlayer1 && PtrPlayer2) {
				float CenterAtZ =
					PtrPlayer1->GetComponent<Transform>()->GetPosition().z
					+ PtrPlayer2->GetComponent<Transform>()->GetPosition().z;
				//1と2の中間地点をとる
				CenterAtZ *= 0.5f;
				//Cameraに注目するオブジェクト（プレイヤー）の設定
				PtrCamera->SetAt(-0.5f, 0, CenterAtZ);
				PtrCamera->SetEye(PtrCamera->GetAt() + m_AtToEyeVec);
			}

		}

		//カウントダウン
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		auto P1Speed = GetSharedGameObject<Player>(L"Player");
		auto P2Speed = GetSharedGameObject<Player2>(L"Player2");
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto SF = Ptr->getSF();
		auto SSF = Ptr->getSSF();
		auto CF1 = Ptr->getCF1();
		auto CF2 = Ptr->getCF2();
		auto CF3 = Ptr->getCF3();
		auto CF4 = Ptr->getCF4();
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto Ptrgo = GetSharedGameObject<GameObject>(L"SoundManager");
		auto pMultiSoundEffect = Ptrgo->GetComponent<MultiSoundEffect>();

		if (count < 4 && !CF1) {
			pMultiSoundEffect->Start(L"Count", 0, 0.5f);
			Ptr->setCF1(true);
		}
		if (count < 3 && !CF2) {
			pMultiSoundEffect->Start(L"Count", 0, 0.5f);
			Ptr->setCF2(true);
		}
		if (count < 2 && !CF3) {
			pMultiSoundEffect->Start(L"Count", 0, 0.5f);
			Ptr->setCF3(true);
		}
		if (count < 1 && !CF4) {
			pMultiSoundEffect->Start(L"Start", 0, 0.5f);
			Ptr->setCF4(true);
		}

		if (count < 5 && !SSF) {
			P1Speed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);
			P2Speed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);

			if (count < 4 && !SSF) {
				GetSharedObjectGroup(L"Count3")->at(0)->SetDrawActive(true);
				GetSharedObjectGroup(L"Count3")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
				GetSharedObjectGroup(L"Count3")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
				GetSharedObjectGroup(L"Count3")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
				if (count>3.9f && !GetSharedObjectGroup(L"Count3")->at(0)->GetComponent<Action>()->IsArrived()) {
					GetSharedObjectGroup(L"Count3")->at(0)->GetComponent<Action>()->Run();
				}

				if (count < 3 && !SSF) {
					GetSharedObjectGroup(L"Count3")->at(0)->SetDrawActive(false);
					GetSharedObjectGroup(L"Count2")->at(0)->SetDrawActive(true);
					GetSharedObjectGroup(L"Count2")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
					GetSharedObjectGroup(L"Count2")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
					GetSharedObjectGroup(L"Count2")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
					if (count>2.9f && !GetSharedObjectGroup(L"Count2")->at(0)->GetComponent<Action>()->IsArrived()) {
						GetSharedObjectGroup(L"Count2")->at(0)->GetComponent<Action>()->Run();
					}

					if (count < 2 && !SSF) {
						GetSharedObjectGroup(L"Count2")->at(0)->SetDrawActive(false);
						GetSharedObjectGroup(L"Count1")->at(0)->SetDrawActive(true);
						GetSharedObjectGroup(L"Count1")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
						GetSharedObjectGroup(L"Count1")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
						GetSharedObjectGroup(L"Count1")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
						if (count>1.9f && !GetSharedObjectGroup(L"Count1")->at(0)->GetComponent<Action>()->IsArrived()) {
							GetSharedObjectGroup(L"Count1")->at(0)->GetComponent<Action>()->Run();
						}

						if (count < 1 && !SSF) {
							GetSharedObjectGroup(L"Count1")->at(0)->SetDrawActive(false);
							GetSharedObjectGroup(L"Start")->at(0)->SetDrawActive(true);
							GetSharedObjectGroup(L"Start")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
							GetSharedObjectGroup(L"Start")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
							GetSharedObjectGroup(L"Start")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
							if (count>0.9f && !GetSharedObjectGroup(L"Start")->at(0)->GetComponent<Action>()->IsArrived()) {
								GetSharedObjectGroup(L"Start")->at(0)->GetComponent<Action>()->Run();
							}


							if (count < 0.0f && !SSF) {
								Ptr->setSF(true);
								Ptr->setSSF(true);
								PF = true;
								PF1 = true;
								GetSharedObjectGroup(L"Start")->at(0)->SetDrawActive(false);
								P1Speed->AddComponent<Rigidbody>()->SetMaxSpeed(30.0f);
								P2Speed->AddComponent<Rigidbody>()->SetMaxSpeed(30.0f);
							}
						}
					}
				}
			}
		}
		count -= ElapsedTime;

		//残り10秒になったら警告
		auto NokoriPtr = App::GetApp()->GetScene<Scene>()->getGameTime();
		if (NokoriPtr <= 11.0f && NokoriPtr > 10.0f) {
			if (!App::GetApp()->GetScene<Scene>()->KEIKOKU) {
				//サウンド
				auto Ptrgo = GetSharedGameObject<GameObject>(L"SoundManager");
				auto pMultiSoundEffect = Ptrgo->GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Warning", 9, 1.0f);

				NF2 = true;
				auto Nokori = CreateSharedObjectGroup(L"Nokori");
				float w = static_cast<float>(App::GetApp()->GetGameWidth());
				float h = static_cast<float>(App::GetApp()->GetGameHeight());
				auto n = AddGameObject<Sprite>(L"NOKORI_TX", false,
					Vector2(w / 2, h / 3), Vector2(0, 500));
				n->AddComponent<Action>();
				Nokori->IntoGroup(n);
				App::GetApp()->GetScene<Scene>()->KEIKOKU = true;

				GetSharedObjectGroup(L"Nokori")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
				GetSharedObjectGroup(L"Nokori")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
				GetSharedObjectGroup(L"Nokori")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
				if (NokoriPtr>10.9f && !GetSharedObjectGroup(L"Nokori")->at(0)->GetComponent<Action>()->IsArrived()) {
					GetSharedObjectGroup(L"Nokori")->at(0)->GetComponent<Action>()->Run();
				}
			}
		}
		if (NokoriPtr <= 10.0f) {
			if (App::GetApp()->GetScene<Scene>()->KEIKOKU) {
				GetSharedObjectGroup(L"Nokori")->at(0)->SetDrawActive(false);
				App::GetApp()->GetScene<Scene>()->KEIKOKU = false;
			}
		}
		if (NokoriPtr <= 10.0f && NF == false) {
			GetSharedObjectGroup(L"A9")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A9")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A9")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"A9")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
			if (NokoriPtr>9.9f && !GetSharedObjectGroup(L"A9")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A9")->at(0)->GetComponent<Action>()->Run();
			}
		}
		if (NokoriPtr <= 9.0f && NF == false) {
			GetSharedObjectGroup(L"A9")->at(0)->SetDrawActive(false);
			GetSharedObjectGroup(L"A8")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A8")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A8")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"A8")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
			if (NokoriPtr>8.9f && !GetSharedObjectGroup(L"A8")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A8")->at(0)->GetComponent<Action>()->Run();
			}
		}
		if (NokoriPtr <= 8.0f && NF == false) {
			GetSharedObjectGroup(L"A8")->at(0)->SetDrawActive(false);
			GetSharedObjectGroup(L"A7")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A7")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A7")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"A7")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
			if (NokoriPtr>7.9f && !GetSharedObjectGroup(L"A7")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A7")->at(0)->GetComponent<Action>()->Run();
			}
		}
		if (NokoriPtr <= 7.0f && NF == false) {
			GetSharedObjectGroup(L"A7")->at(0)->SetDrawActive(false);
			GetSharedObjectGroup(L"A6")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A6")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A6")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"A6")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
			if (NokoriPtr>6.9f && !GetSharedObjectGroup(L"A6")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A6")->at(0)->GetComponent<Action>()->Run();
			}
		}
		if (NokoriPtr <= 6.0f && NF == false) {
			GetSharedObjectGroup(L"A6")->at(0)->SetDrawActive(false);
			GetSharedObjectGroup(L"A5")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A5")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A5")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"A5")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
			if (NokoriPtr>5.9f && !GetSharedObjectGroup(L"A5")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A5")->at(0)->GetComponent<Action>()->Run();
			}
		}
		if (NokoriPtr <= 5.0f && NF == false) {
			GetSharedObjectGroup(L"A5")->at(0)->SetDrawActive(false);
			GetSharedObjectGroup(L"A4")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A4")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A4")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"A4")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
			if (NokoriPtr>4.9f && !GetSharedObjectGroup(L"A4")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A4")->at(0)->GetComponent<Action>()->Run();
			}
		}
		if (NokoriPtr <= 4.0f && NF == false) {
			GetSharedObjectGroup(L"A4")->at(0)->SetDrawActive(false);
			GetSharedObjectGroup(L"A3")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A3")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A3")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"A3")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
			if (NokoriPtr>3.9f && !GetSharedObjectGroup(L"A3")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A3")->at(0)->GetComponent<Action>()->Run();
			}
		}
		if (NokoriPtr <= 3.0f && NF == false) {
			GetSharedObjectGroup(L"A3")->at(0)->SetDrawActive(false);
			GetSharedObjectGroup(L"A2")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A2")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A2")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"A2")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
			if (NokoriPtr>2.9f && !GetSharedObjectGroup(L"A2")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A2")->at(0)->GetComponent<Action>()->Run();
			}
		}
		if (NokoriPtr <= 2.0f && NF == false) {
			GetSharedObjectGroup(L"A2")->at(0)->SetDrawActive(false);
			GetSharedObjectGroup(L"A1")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A1")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A1")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			GetSharedObjectGroup(L"A1")->at(0)->GetComponent<Action>()->AddMoveTo(0.3f, Vector3(0, -600, 0));
			if (NokoriPtr>1.9f && !GetSharedObjectGroup(L"A1")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A1")->at(0)->GetComponent<Action>()->Run();
			}
		}
		if (NokoriPtr <= 1.0f && NF == false) {
			GetSharedObjectGroup(L"A1")->at(0)->SetDrawActive(false);
			GetSharedObjectGroup(L"A0")->at(0)->SetDrawActive(true);
			GetSharedObjectGroup(L"A0")->at(0)->GetComponent<Action>()->AddMoveTo(0.2f, Vector3(0, 0.1f, 0));
			GetSharedObjectGroup(L"A0")->at(0)->GetComponent<Action>()->AddMoveTo(0.4f, Vector3(0, 0, 0));
			if (NokoriPtr>0.9f && !GetSharedObjectGroup(L"A0")->at(0)->GetComponent<Action>()->IsArrived()) {
				GetSharedObjectGroup(L"A0")->at(0)->GetComponent<Action>()->Run();
			}
		}
		auto GF = Ptr->getTF();
		if (GF)
		{
			Count++;
			//1秒＝60フレーム
			if (Count == 120) {
				GetSharedGameObject<Black>(L"Black", false)->StartBlack();
				count = 10.0f;
				Ptr->setTF(false);
				Ptr->setSF(false);
				Ptr->setCF1(false);
				Ptr->setCF2(false);
				Ptr->setCF3(false);
				Ptr->setCF4(false);
				GLF = false;
			}
		}
	}

	void GameStage1−3::OnLastUpdate() {

		float px1 = GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetPosition().x;
		float pz1 = GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetPosition().z;

		float Left1 = -11.7f;
		float Right1 = -1.3f;
		float MaxD1 = 39.2f;
		float MinD1 = -38.0f;

		if (px1 > Left1 && px1 < Right1 && pz1 > MinD1 && pz1 < MaxD1) {
			;
		}
		else
		{
			auto Beforpos = GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetBeforePosition();
			GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->SetPosition(Beforpos);
		}

		float px2 = GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->GetPosition().x;
		float pz2 = GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->GetPosition().z;

		float Left2 = 0.3f;
		float Right2 = 10.7f;
		float MaxD2 = 39.2f;
		float MinD2 = -38.0f;

		if (px2 > Left2 && px2 < Right2 && pz2 > MinD2 && pz2 < MaxD2) {
			;
		}
		else
		{
			auto Beforpos = GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->GetBeforePosition();
			GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->SetPosition(Beforpos);
		}

		auto P1Pos = GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetPosition();
		auto P2Pos = GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->GetPosition();
		auto Group = GetSharedObjectGroup(L"MoveTORUS");
		auto P1GoalSpeed = GetSharedGameObject<Player>(L"Player");
		auto P2GoalSpeed = GetSharedGameObject<Player2>(L"Player2");
		bool p1goal = false;
		bool p2goal = false;
		for (auto& v : Group->GetGroupVector()) {
			auto ptrTorus = dynamic_pointer_cast<MoveTORUS>(v.lock());
			auto Sh = v.lock();
			if (ptrTorus) {
				auto ShPos = Sh->GetComponent<Transform>()->GetPosition();
				if (Vector3EX::Length(P1Pos - ShPos) <= 2.0f) {

					//スパークの放出
					auto PtrSpark = GetSharedGameObject<MultiSpark>(L"MultiSpark", false);
					if (PtrSpark) {
						PtrSpark->InsertSpark(ShPos);
					}
					p1goal = true;
				}
				else if (Vector3EX::Length(P2Pos - ShPos) <= 2.0f) {
					//スパークの放出
					auto PtrSpark = GetSharedGameObject<MultiSpark>(L"MultiSpark", false);
					if (PtrSpark) {
						PtrSpark->InsertSpark(ShPos);
					}
					p2goal = true;
				}
			}
		}
		//2つともゴールしたら
		if (p1goal && p2goal) {

			NF = true;

			if (NF2 == true) {
				GetSharedObjectGroup(L"Nokori")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A9")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A8")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A7")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A6")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A5")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A4")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A3")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A2")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A1")->at(0)->SetDrawActive(false);
				GetSharedObjectGroup(L"A0")->at(0)->SetDrawActive(false);
			}

			auto Ptr = App::GetApp()->GetScene<Scene>();
			Ptr->setTF(true);
			Ptr->setGF(true);
			Ptr->setGF2(true);

			//タイマースプライトから時間を取得
			auto TimerPtr = GetSharedGameObject<TimerSprite>(L"TimerSprite");
			auto ScenePtr = App::GetApp()->GetScene<Scene>();
			ScenePtr->SetGameTotalTime(TimerPtr->getTotalTime());

			P1GoalSpeed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);
			P2GoalSpeed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);

			float w = static_cast<float>(App::GetApp()->GetGameWidth());
			float h = static_cast<float>(App::GetApp()->GetGameHeight());

			if (GLF == false && count <= 0) {
				GetSharedObjectGroup(L"Goal")->at(0)->SetDrawActive(true);
				GetSharedObjectGroup(L"Goal")->at(0)->GetComponent<Action>()->AddScaleTo(0.2f, Vector3(w / 3, h / 4, 0));
				GetSharedObjectGroup(L"Goal")->at(0)->GetComponent<Action>()->AddScaleTo(0.4f, Vector3(w / 2, h / 3, 0));
				GetSharedObjectGroup(L"Goal")->at(0)->GetComponent<Action>()->Run();
				GLF = true;
			}

			//演出が終わったら
			if (Win == true) {
				//サウンド
				auto Ptrgo = GetSharedGameObject<GameObject>(L"SoundManager");
				auto pMultiSoundEffect = Ptrgo->GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Clear", 0, 0.1f);
				pMultiSoundEffect->Stop(L"Warning");
				Win = false;
			}
		}
			//暗転を毎回判定
			//暗転終わり
			if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
			{
				//シーン切り替え
				SceneChange();
			}
	}
}
//end basecross
