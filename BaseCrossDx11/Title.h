/*!
@file Title.h
@brief タイトル
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス
	//--------------------------------------------------------------------------------------
	class Title : public Stage {
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//壁模様のスプライト作成
		void CreateSprite();

		shared_ptr<MultiAudioObject> m_AudioObjectPtr;

		int count = 0;

		int Tcount = 0;

	public:
		//構築と破棄
		Title() :Stage() {}
		virtual ~Title() {}
		void SceneChange();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
	};

}
//end basecross
