/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross {



	//--------------------------------------------------------------------------------------
	//	class Player2 : public GameObject;
	//	用途: プレイヤー2
	//--------------------------------------------------------------------------------------
	class Player2 : public GameObject {
		shared_ptr< StateMachine<Player2> >  m_StateMachine;	//ステートマシーン
															//移動の向きを得る
		Vector3 GetAngle();
		//最高速度
		float m_MaxSpeed;
		//減速率
		float m_Decel;
		//質量
		float m_Mass;

		bool m_Goal;

		Vector3 m_Pos;
		int m_CollCount = 0;

	public:
		//構築と破棄
		Player2(const shared_ptr<Stage>& StagePtr,const Vector3& Pos, float MaxSpeed);
		virtual ~Player2() {}
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr< StateMachine<Player2> > GetStateMachine() const {
			return m_StateMachine;
		}
		//モーションを実装する関数群
		//移動して向きを移動方向にする
		void MoveRotationMotion();
		//Aボタンでジャンプするどうかを得る
		bool IsJumpMotion();

		//プレイヤー１の状態によってAボタンでジャンプするどうかを得る
		//bool IsP1JumpMotion();

		//Aボタンでジャンプする瞬間の処理
		void JumpMotion();
		//Aボタンでジャンプしている間の処理
		//ジャンプ終了したらtrueを返す
		bool JumpMoveMotion();

		bool Goal2 = false;
		bool IsGoal() {
			return m_Goal;
		}

		int flgTime = 0;

		//更新
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;
		virtual void OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec) override;

		bool GoalCollision(vector<shared_ptr<GameObject>>& OtherVec);

		//ターンの最終更新時
		virtual void OnLastUpdate() override;

		float TotalAngle = 0;

	};

	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	class DefaultState2 : public ObjState<Player2>
	{
		DefaultState2() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<DefaultState2> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player2>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player2>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player2>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class JumpState : public ObjState<Player>;
	//	用途: ジャンプ状態
	//--------------------------------------------------------------------------------------
	class JumpState2 : public ObjState<Player2>
	{
		JumpState2() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<JumpState2> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player2>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player2>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player2>& Obj)override;
	};

}
//end basecross


