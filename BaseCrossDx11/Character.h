/*!
@file Character.h
@brief キャラクターなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{


	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	class FixedBox : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		FixedBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~FixedBox();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	class FixedBox2 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		FixedBox2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~FixedBox2();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class FixedTORUS : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	class FixedTORUS : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		FixedTORUS(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~FixedTORUS();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class CSVFixedSphere : public GameObject;
	//	用途: CSV用の固定のボックス
	//--------------------------------------------------------------------------------------
	class CSVFixedSphere : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		CSVFixedSphere(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~CSVFixedSphere();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class CSVFixedBox : public GameObject;
	//	用途: CSV用の固定のボックス
	//--------------------------------------------------------------------------------------
	class CSVFixedBox : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		CSVFixedBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~CSVFixedBox();
		//初期化
		virtual void OnCreate() override;
		//操作
		virtual void OnDraw()override;
	};


	//--------------------------------------------------------------------------------------
	//	class CSVFixedBox2 : public GameObject;
	//	用途: CSV用の固定のボックス
	//--------------------------------------------------------------------------------------
	class CSVFixedBox2 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		CSVFixedBox2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~CSVFixedBox2();
		//初期化
		virtual void OnCreate() override;
		virtual void OnDraw() override;
		//操作
	};

	DECLARE_DX11_VERTEX_SHADER(VSPNTInstance, VertexPositionNormalTextureMatrix)

	//--------------------------------------------------------------------------------------
	//	class CSVFixedBoxManager : public GameObject;
	//	用途: CSV用の固定のボックスの描画クラス
	//--------------------------------------------------------------------------------------
	class CSVFixedBoxManager : public GameObject {
		//メッシュ
		shared_ptr<MeshResource> m_CubeMesh;
		wstring m_TextureFileName;		///<テクスチャファイル名
		shared_ptr<TextureResource> m_TextureResource;	///<テクスチャリソース
		void CreateBuffers();	///<バッファの作成
		const size_t m_MaxInstance;				///<インスタンス最大値
		vector<Matrix4X4> m_CubeMatrixVec;		///<立方体のインスタンス描画配列
		ComPtr<ID3D11Buffer> m_MatrixBuffer;	///<行列用の頂点バッファ


	public:
		//構築と破棄
		CSVFixedBoxManager(const shared_ptr<Stage>& StagePtr,
			const wstring& TextureFileName);
		virtual ~CSVFixedBoxManager() {}
		//初期化
		virtual void OnCreate() override;
		void InsertCSVFixedBox(const Matrix4X4& m);
		virtual void OnDraw()override;
	};

	//--------------------------------------------------------------------------------------
	//	class CSVFixedBox : public GameObject;
	//	用途: CSV用の固定のボックス
	//--------------------------------------------------------------------------------------
	class CSVFixedBox3 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		CSVFixedBox3(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~CSVFixedBox3();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class CSVFixedBox : public GameObject;
	//	用途: CSV用の固定のボックス
	//--------------------------------------------------------------------------------------
	class CSVFixedBox5 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		CSVFixedBox5(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~CSVFixedBox5();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class CSVFixedBox : public GameObject;
	//	用途: CSV用の固定のボックス
	//--------------------------------------------------------------------------------------
	class CSVFixedBoxMin : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		CSVFixedBoxMin(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~CSVFixedBoxMin();
		//初期化
		virtual void OnCreate() override;
		//操作
	};


	//--------------------------------------------------------------------------------------
	//	class MoveBoxGoalL : public GameObject;
	//	用途: ゴール補助をするボックス
	//--------------------------------------------------------------------------------------
	class MoveBoxGoalL : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxGoalL(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxGoalL();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveBoxGoalR : public GameObject;
	//	用途: ゴール補助をするボックス
	//--------------------------------------------------------------------------------------
	class MoveBoxGoalR : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxGoalR(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxGoalR();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class MoveBoxL: public GameObject;
	//	用途: 妨害用ボックス（左）
	//--------------------------------------------------------------------------------------
	class MoveBoxL : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxL(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxL();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveBoxR: public GameObject;
	//	用途: 妨害用ボックス（右）
	//--------------------------------------------------------------------------------------
	class MoveBoxR : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxR(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxR();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown: public GameObject;
	//	用途: 下に落ちるボックス
	//--------------------------------------------------------------------------------------
	class MoveBoxDown : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxDown(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxDown();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown2: public GameObject;
	//	用途: 下に落ちるボックス
	//--------------------------------------------------------------------------------------
	class MoveBoxDown2 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxDown2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxDown2();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown3: public GameObject;
	//	用途: 下に落ちるボックス
	//--------------------------------------------------------------------------------------
	class MoveBoxDown3 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxDown3(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxDown3();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown4: public GameObject;
	//	用途: 下に落ちるボックス
	//--------------------------------------------------------------------------------------
	class MoveBoxDown4 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxDown4(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxDown4();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown5: public GameObject;
	//	用途: 徐々に下がる壁
	//--------------------------------------------------------------------------------------
	class MoveBoxDown5 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxDown5(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxDown5();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveBox2 : public GameObject;
	//	用途: 上下移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveBox2 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBox2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBox2();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveBox3 : public GameObject;
	//	用途: 上下移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveBox3 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBox3(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBox3();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveBox4 : public GameObject;
	//	用途: 上下移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveBox4 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBox4(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBox4();
		//初期化
		virtual void OnCreate() override;
		//操作
	};


	//--------------------------------------------------------------------------------------
	//	class MoveBox5 : public GameObject;
	//	用途: 上下移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveBox5 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBox5(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBox5();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveBoxRO : public GameObject;
	//	用途: 上下移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveBoxRO : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxRO(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxRO();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class MoveBoxRO2 : public GameObject;
	//	用途: 上下移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveBoxRO2 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxRO2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxRO2();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class MoveBoxRec : public GameObject;
	//	用途: 上下移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveBoxRec : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveBoxRec(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveBoxRec();
		//初期化
		virtual void OnCreate() override;
		//操作
	};


	//--------------------------------------------------------------------------------------
	//	class MoveSphere : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveSphere : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveSphere (const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveSphere();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class MoveSphere : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveSphereL : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveSphereL(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveSphereL();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class MoveSphere : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveSphere2 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveSphere2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveSphere2();
		//初期化
		virtual void OnCreate() override;
		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class MoveSphere : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveSphereL2 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveSphereL2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveSphereL2();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveTORUS : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	class MoveTORUS : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MoveTORUS(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MoveTORUS();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class MoveBox5 : public GameObject;
	//	用途: 上下移動するボックス
	//--------------------------------------------------------------------------------------
	class MovePlate : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		MovePlate(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
		);
		virtual ~MovePlate();
		//初期化
		virtual void OnCreate() override;
		//操作
	};


	//--------------------------------------------------------------------------------------
	//	class SphereObject : public GameObject;
	//	用途: コイン
	//--------------------------------------------------------------------------------------
	class SphereObject : public GameObject {
		Vector3 m_StartPos;
	public:
		//構築と破棄
		SphereObject(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~SphereObject();
		//初期化
		virtual void OnCreate() override;
		//操作
		void Col();
	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiSpark : public MultiParticle {
	public:
		//構築と破棄
		MultiSpark(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark();
		//初期化
		virtual void OnCreate() override;
		void InsertSpark(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark2 : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiSpark2 : public MultiParticle {
	public:
		//構築と破棄
		MultiSpark2(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark2();
		//初期化
		virtual void OnCreate() override;
		void InsertSpark(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark3 : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiSpark3 : public MultiParticle {
	public:
		//構築と破棄
		MultiSpark3(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark3();
		//初期化
		virtual void OnCreate() override;
		void InsertSpark(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark4 : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiSpark4 : public MultiParticle {
	public:
		//構築と破棄
		MultiSpark4(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark4();
		//初期化
		virtual void OnCreate() override;
		void InsertSpark(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	///	壁模様のスプライト
	//--------------------------------------------------------------------------------------
	class Sprite : public GameObject {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector2 m_StartPos;
		wstring m_TextureKey;

	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		@param[in]	StagePtr	ステージ
		@param[in]	TextureKey	テクスチャキー
		@param[in]	Trace	透明処理するかどうか
		@param[in]	StartScale	初期スケール
		@param[in]	StartPos	初期位置
		*/
		//--------------------------------------------------------------------------------------
		Sprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vector2& StartScale, const Vector2& StartPos);
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Sprite();
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief 更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate()override {}
	};

	//--------------------------------------------------------------------------------------
	///	スコア表示のスプライト
	//--------------------------------------------------------------------------------------
	class ScoreSprite : public GameObject {
		bool m_Trace;
		Vector2 m_StartScale;
		Vector2 m_StartPos;
		wstring m_TextureKey;
		float m_Score;
		//桁数
		UINT m_NumberOfDigits;
		//バックアップ頂点データ
		vector<VertexPositionTexture> m_BackupVertices;

	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		@param[in]	StagePtr	ステージ
		@param[in]	NumberOfDigits	桁数
		@param[in]	TextureKey	テクスチャキー
		@param[in]	Trace	透明処理するかどうか
		@param[in]	StartScale	初期スケール
		@param[in]	StartPos	初期位置
		*/
		//--------------------------------------------------------------------------------------
		ScoreSprite(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
			const wstring& TextureKey, bool Trace,
			const Vector2& StartScale, const Vector2& StartPos);
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~ScoreSprite() {}
		//--------------------------------------------------------------------------------------
		/*!
		@brief スコアのセット
		@param[in]	f	値
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		void SetScore(float f) {
			m_Score = f;
		}
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief 更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate()override;
	};

	//--------------------------------------------------------------------------------------
	//	class Black : public GameObject;
	//	用途: 暗転用黒背景
	//--------------------------------------------------------------------------------------
	class Black : public GameObject
	{
	private:
		float m_alpha = 0;
		bool m_StartFlg = false;
		bool m_EndFlg = false;
	public:
		Black(const shared_ptr<Stage>& StagePtr);
		void OnCreate() override;
		void OnUpdate() override;

		//暗転起動
		void StartBlack();

		//暗転したかどうか取得
		bool GetBlackFinish();
	};

	//--------------------------------------------------------------------------------------
	//	class White : public GameObject;
	//	用途: 暗転用黒背景
	//--------------------------------------------------------------------------------------
	class White : public GameObject
	{
	private:
		float m_alpha = 0;
		bool m_StartFlg = false;
		bool m_EndFlg = false;
	public:
		White(const shared_ptr<Stage>& StagePtr);
		void OnCreate() override;
		void OnUpdate() override;

		//暗転起動
		void StartWhite();

		//暗転したかどうか取得
		bool GetWhiteFinish();
	};

	//--------------------------------------------------------------------------------------
	//	class Black : public GameObject;
	//	用途: 暗転用黒背景
	//--------------------------------------------------------------------------------------
	class BlackIn : public GameObject
	{
	private:
		float m_alpha = 0;
		bool m_StartFlg = false;
		bool m_EndFlg = false;
	public:
		BlackIn(const shared_ptr<Stage>& StagePtr);
		void OnCreate() override;
		void OnUpdate() override;

		//暗転起動
		void StartBlackIn();

		//暗転したかどうか取得
		bool GetBlackInFinish();
	};

	//--------------------------------------------------------------------------------------
	//	class White : public GameObject;
	//	用途: 暗転用黒背景
	//--------------------------------------------------------------------------------------
	class WhiteIn : public GameObject
	{
	private:
		float m_alpha = 0;
		bool m_StartFlg = false;
		bool m_EndFlg = false;
	public:
		WhiteIn(const shared_ptr<Stage>& StagePtr);
		void OnCreate() override;
		void OnUpdate() override;

		//暗転起動
		void StartWhiteIn();

		//暗転したかどうか取得
		bool GetWhiteInFinish();
	};

	//--------------------------------------------------------------------------------------
	//	class Switch_Floor : public GameObject;
	//	用途: すいっちゆか
	//--------------------------------------------------------------------------------------
	class Switch_Floor : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;	
	public:
		int m_SwitchNo;
		//構築と破棄
		Switch_Floor(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position,
			const int& switchno
		);
		virtual ~Switch_Floor();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: スイッチに対応したどあ
	//--------------------------------------------------------------------------------------
	class DoorModel : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		int m_DoorNo;
		//構築と破棄
		DoorModel(const shared_ptr<Stage>& StagePtr,
			const Vector3& m_Scale,
			const Vector3& Position,
			const int& doorno
		);
		virtual ~DoorModel();
		//初期化
		virtual void OnCreate() override;
		//操作

		void Open();
	};

	//--------------------------------------------------------------------------------------
	//	class Switch_Floor : public GameObject;
	//	用途: すいっちゆか
	//--------------------------------------------------------------------------------------
	class Switch_Floor2 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		int m_SwitchNo;
		//構築と破棄
		Switch_Floor2(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position,
			const int& switchno
		);
		virtual ~Switch_Floor2();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: スイッチに対応したどあ
	//--------------------------------------------------------------------------------------
	class DoorModel2 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		int m_DoorNo;
		//構築と破棄
		DoorModel2(const shared_ptr<Stage>& StagePtr,
			const Vector3& m_Scale,
			const Vector3& Position,
			const int& doorno
		);
		virtual ~DoorModel2();
		//初期化
		virtual void OnCreate() override;
		//操作

		void Open();
	};
	//--------------------------------------------------------------------------------------
	//	class Switch_Floor : public GameObject;
	//	用途: すいっちゆか
	//--------------------------------------------------------------------------------------
	class Switch_Floor3 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		int m_SwitchNo;
		//構築と破棄
		Switch_Floor3(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position,
			const int& switchno
		);
		virtual ~Switch_Floor3();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};
	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: スイッチに対応したどあ
	//--------------------------------------------------------------------------------------
	class DoorModel3 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		int m_DoorNo;
		//構築と破棄
		DoorModel3(const shared_ptr<Stage>& StagePtr,
			const Vector3& m_Scale,
			const Vector3& Position,
			const int& doorno
		);
		virtual ~DoorModel3();
		//初期化
		virtual void OnCreate() override;
		//操作

		void Open();
	};
	//--------------------------------------------------------------------------------------
	//	class Switch_Floor : public GameObject;
	//	用途: すいっちゆか
	//--------------------------------------------------------------------------------------
	class Switch_Floor4 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		int m_SwitchNo;
		//構築と破棄
		Switch_Floor4(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position,
			const int& switchno
		);
		virtual ~Switch_Floor4();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(vector<shared_ptr<GameObject>>& OtherVec) override;

		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: スイッチに対応したどあ
	//--------------------------------------------------------------------------------------
	class DoorModel4 : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		int m_DoorNo;
		//構築と破棄
		DoorModel4(const shared_ptr<Stage>& StagePtr,
			const Vector3& m_Scale,
			const Vector3& Position,
			const int& doorno
		);
		virtual ~DoorModel4();
		//初期化
		virtual void OnCreate() override;
		//操作

		void Open();
	};

	//--------------------------------------------------------------------------------------
	//	class UnevenGroundData : public GameObject;
	//	用途: でこぼこ床のデータ
	//--------------------------------------------------------------------------------------
	class UnevenGroundData : public GameObject {
		vector<TRIANGLE> m_Triangles;
	public:
		//構築と破棄
		UnevenGroundData(const shared_ptr<Stage>& StagePtr);
		virtual ~UnevenGroundData();
		//初期化
		virtual void OnCreate() override;
		//三角形の配列を返す
		const vector<TRIANGLE>& GetTriangles() const {
			return m_Triangles;
		}
	};



	//--------------------------------------------------------------------------------------
	//	class UnevenGround : public GameObject;
	//	用途: でこぼこ床
	//--------------------------------------------------------------------------------------
	class UnevenGround : public GameObject {
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		UnevenGround(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);
		virtual ~UnevenGround();
		//初期化
		virtual void OnCreate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	class NumberSprite : public GameObject;
	//	用途: 数字のスプライト
	//--------------------------------------------------------------------------------------
	class NumberSprite : public GameObject
	{
	private:
		//なんかしらんメッシュのリスト
		vector<shared_ptr<MeshResource>> m_Mesh;
		int m_num = 0;
		//桁数
		int m_digit = 0;
		//生成されてる数字
		int m_Constdigit = 0;
		//表示レイヤー
		int m_layer = 0;
		//大きさ
		Vector2 m_scale;
		//位置
		Vector2 m_pos;
		//数字分のvector配列
		vector<shared_ptr<GameObject>> m_Numbers;
	public:
		NumberSprite(const shared_ptr<Stage>& StagePtr, int num, Vector2 pos, Vector2 scale, int layer);

		void OnCreate()override;

		//位置調整
		void SetPositionVec2(Vector2 pos);
		//大きさ調整
		void SetScaleVec2(Vector2 scale);
		void SetNum(int num);
	};


}
//end basecross
