/*!
@file Tutorial.cpp
@brief メニューステージ
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス実体
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void TutorialStage::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"Sousa.png";
		App::GetApp()->RegisterTexture(L"SOUSA_TX", strTexture);
		strTexture = DataDir + L"kuro.png";
		App::GetApp()->RegisterTexture(L"KURO_TX", strTexture);

		wstring DecisionWav = App::GetApp()->m_wstrRelativeDataPath + L"Decision.wav";
		App::GetApp()->RegisterWav(L"Decision", DecisionWav);

	}
	void TutorialStage::CreateViewLight()
	{
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 5.0f, -5.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}
	//スプライト作成
	void TutorialStage::CreateSprite() {
		float w = static_cast<float>(App::GetApp()->GetGameWidth());
		float h = static_cast<float>(App::GetApp()->GetGameHeight());

		AddGameObject<Sprite>(L"SOUSA_TX", false,
			Vector2(w, h), Vector2(0, 0));
	}

	//シーン変更
	void TutorialStage::SceneChange1()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-0");
	}

	void TutorialStage::OnCreate() {

		auto Ptr = AddGameObject<GameObject>();
		SetSharedGameObject(L"SEManager", Ptr);

		auto Ptr1 = App::GetApp()->GetScene<Scene>();

		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//壁模様のスプライト作成
			CreateSprite();

			Ptr1->setCF1(false);
			Ptr1->setCF2(false);
			Ptr1->setCF3(false);
			Ptr1->setCF4(false);

			auto FB = AddGameObject<Black>();
			SetSharedGameObject(L"Black", FB);
			auto FBIn = AddGameObject<BlackIn>();
			SetSharedGameObject(L"BlackIn", FBIn);
		}
		catch (...) {
			throw;
		}

		//サウンドを登録.
		auto pMultiSoundEffect = Ptr->AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Decision");

	}

	//更新
	void TutorialStage::OnUpdate() {

		GetSharedGameObject<BlackIn>(L"BlackIn", false)->StartBlackIn();

		if (GetSharedGameObject<BlackIn>(L"BlackIn", false)->GetBlackInFinish())
		{

			//コントローラの取得
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (CntlVec[0].bConnected) {
				//Aボタンが押された瞬間ならステージ推移
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && a == 0) {
					GetSharedGameObject<Black>(L"Black", false)->StartBlack();
					auto Ptr = GetSharedGameObject<GameObject>(L"SEManager");
					auto pMultiSoundEffect = Ptr->GetComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"Decision", 0, 0.5f);
				}
				if (a == 0)
				{
					//暗転を毎回判定
					//暗転終わり
					if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
					{
						//シーン切り替え
						SceneChange1();
					}
				}
			}
		}
	}
}
//end basecross
