/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------


	//リソースの作成
	void GameStage1−6::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"siro.png";
		App::GetApp()->RegisterTexture(L"SIRO_TX", strTexture);
		strTexture = DataDir + L"kuro.png";
		App::GetApp()->RegisterTexture(L"KURO_TX", strTexture);
		strTexture = DataDir + L"haiiro.png";
		App::GetApp()->RegisterTexture(L"HAIIRO_TX", strTexture);
		strTexture = DataDir + L"nizi.jpg";
		App::GetApp()->RegisterTexture(L"NIZI_TX", strTexture);
		strTexture = DataDir + L"kimidori.png";
		App::GetApp()->RegisterTexture(L"KIMIDORI_TX", strTexture);
		strTexture = DataDir + L"renga.png";
		App::GetApp()->RegisterTexture(L"RENGA_TX", strTexture);
		strTexture = DataDir + L"tuti.png";
		App::GetApp()->RegisterTexture(L"TUTI_TX", strTexture);
		strTexture = DataDir + L"sougen.png";
		App::GetApp()->RegisterTexture(L"SOUGEN_TX", strTexture);
		strTexture = DataDir + L"spark.png";
		App::GetApp()->RegisterTexture(L"SPARK_TX", strTexture);
		strTexture = DataDir + L"trace2.png";
		App::GetApp()->RegisterTexture(L"TRACE2_TX", strTexture);

		wstring JumpWav = App::GetApp()->m_wstrRelativeDataPath + L"Jump.wav";
		App::GetApp()->RegisterWav(L"Jump", JumpWav);

		strTexture = DataDir + L"Timer4.png";
		App::GetApp()->RegisterTexture(L"TIMER_TX", strTexture);

	}



	//ビューとライトの作成
	void GameStage1−6::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		//auto PtrCamera = ObjectFactory::Create<LookAtCamera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vector3(0.0f, 10.0f, -20.0f));
		PtrCamera->SetAt(m_AtToEyeVec);
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}


	//プレートの作成
	void GameStage1−6::CreatePlate() {
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(200.0f, 200.0f, 1.0f),
			Qt,
			Vector3(0.0f, 0.0f, 0.0f)
		);
		PtrTrans->SetScale(100.0f, 150.0f, 1.0f);
		PtrTrans->SetQuaternion(Qt);
		PtrTrans->SetPosition(0.0f, 0.0f, 20.0f);

		//描画コンポーネントの追加
		auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//自分に影が映りこむようにする
		DrawComp->SetOwnShadowActive(true);

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"SOUGEN_TX");

		Ptr->AddComponent<CollisionRect>();
	}

	//CSVマッピングによるステージ作成
	void GameStage1−6::CreateRoad()
	{
		//CSVファイルからゲームステージの選択
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath + L"GameStage\\";

		wstring FileName = MediaPath + L"Stage_E5.csv";

		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(FileName);
		//ファイルがあるかどうか
		if (!GameStageCsv.ReadCsv()) {
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません",
				FileName,
				L"選択された場所にdアクセスできません"

			);
		}

		const int iDataSizeRow = 0; //データが0行目から始まるように定数
		vector<wstring>StageMapVec; //ワーク用のベクター配列
		GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);

		//行、列の取得
		m_sRowSize = static_cast<size_t>(_wtoi(StageMapVec[0].c_str())); //Row=行
		m_sColSize = static_cast<size_t>(_wtoi(StageMapVec[1].c_str())); //Col=列

																		 //マップデータの配列作成
		m_MapDataVec = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分
		m_MapDataVec2 = vector<vector<size_t>>(m_sRowSize, vector<size_t>(m_sColSize)); //列の数と行の数分


																						//配列の初期化
		for (size_t i = 0; i < m_sRowSize; i++) {
			for (size_t j = 0; j < m_sColSize; j++) {
				m_MapDataVec[i][j] = 0;	//0で初期化
				m_MapDataVec2[i][j] = 0;	//0で初期化
			}
		}

		//1行目からステージが定義されている
		const int iDataStartRow = 1;

		//(float)m_sRowSize / 2;



		if (m_sRowSize > 0) {
			for (size_t i = 0; i < m_sRowSize; i++) {
				GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec); //START + iだから最初は２が入る
				for (size_t j = 0; j < m_sColSize; j++) {
					const int iNum = _wtoi(StageMapVec[j].c_str()); //列から取得したwstringをintに

					float ihalf = (float)i / 2.0f;
					float jhalf = (float)j / 2.0f;
					//マップデータ配列に格納
					m_MapDataVec[i][j] = iNum;
					//マップデータ配列に格納
					m_MapDataVec2[i][j] = iNum;
					//配置されるオブジェクトの基準のスケール
					float ObjectScale = 1.0f;


					//基準スケール
					Vector3 Scale(ObjectScale, ObjectScale, ObjectScale);
					//キャラクタースケール
					Vector3 Scale_Chara(ObjectScale*0.95f, ObjectScale*0.95f, ObjectScale*0.95f);

					//基準ローテーション
					Vector3 Rotation(0.0f, 0.0f, 0.0f);
					//床ローテーション
					Vector3 Rotationxx(45.0f, 0.0f, 0.0f);
					//列の半分の数
					float ColHalf = (float)m_sColSize / 2.0f;
					//行の半分の数
					float RowHalf = (float)m_sRowSize / 2.0f;

					//ステージの真ん中をX、Z軸０にしたいのでポジションのXを行の半分の値分引いて、Zを列の半分の値でプラスしてあげる
					//基準ポジション
					Vector3 Pos(static_cast<float>(j) - ColHalf, 0.0f, -static_cast<float>(i) + RowHalf);


					//Block用のポジション
					Vector3 BlockPos(static_cast<float>(j) - ColHalf, 1.0f, -static_cast<float>(i) + RowHalf);

					//Character用のポジション
					Vector3 CharaPos(static_cast<float>(j) - ColHalf, 1.0f, -static_cast<float>(i) + RowHalf);

					//LimitWall用のポジション
					//Vector3 LimitWallPos(static_cast<float>(j) - ColHalf, 2.0f, -static_cast<float>(i) + RowHalf);

					switch (iNum)
					{
					case DataID::NONE://
						break;
					case DataID::DOUBLEBLOCK://
						AddGameObject<CSVFixedBox>(Scale, Rotation, BlockPos);
						AddGameObject<CSVFixedBox>(Scale, Rotation, Pos);
						break;
					case DataID::BLOCKMIN://
						AddGameObject<CSVFixedBoxMin>(Scale, Rotation, Pos);
						break;

					case DataID::PLAYER://プレイヤーを生成

						SetSharedGameObject(L"Player", AddGameObject<Player>(CharaPos, 30.0f));
						break;

					case DataID::PLAYER2://プレイヤー2を生成
						SetSharedGameObject(L"Player2", AddGameObject<Player2>(CharaPos, 30.0f));
						break;

					case DataID::GOAL://ゴールを生成
						AddGameObject<MoveTORUS>(Scale, Rotation, Pos);
						break;
					case DataID::ENEMY:
						AddGameObject<HeadEnemy>(CharaPos);
						break;

					case DataID::ENEMY2:
						AddGameObject<HeadEnemy2>(CharaPos);
						break;

					default:
						assert(!"不正な値が入ってます,CSV関係のところな気がします");
					}


				}
			}
		}
		//--------------------------------------------------------
		//最適化,
		//********************************************************
		////ステージデータのコピーを作成,
		auto & WorkVec = m_MapDataVec;
		auto & WorkVec2 = m_MapDataVec2;

	}

	//固定のボックスの作成
	void GameStage1−6::CreateFixedBox() {
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			{
				Vector3(11.0f, 3.7f, 0.5f),
				Vector3(0.0f, 0.0f, 0.0f),  //障害物（壁）
				Vector3(5.5f, 0.25f, 18.0f)
			}



		};
		//オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<FixedBox>(v[0], v[1], v[2]);
		}
	}



	//上下移動しているボックスの作成
	void GameStage1−6::CreateMoveBox2() {
		CreateSharedObjectGroup(L"MoveBox2");
		AddGameObject<MoveBox2>(
			Vector3(11.0f, 3.7f, 0.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //上下移動する壁
			Vector3(-6.5f, -4.8f, 18.0f)
			);
		AddGameObject<MoveBox2>(
			Vector3(5.0f, 3.7f, 0.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //上下移動する壁
			Vector3(-3.5f, -4.8f, -15.0f)
			);
		AddGameObject<MoveBox2>(
			Vector3(5.0f, 3.7f, 0.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //上下移動する壁
			Vector3(2.5f, -4.8f, -15.0f)
			);

	}

	//上下移動しているボックスの作成
	void GameStage1−6::CreateMoveBox3() {
		CreateSharedObjectGroup(L"MoveBox3");
		AddGameObject<MoveBox3>(
			Vector3(2.5f, 0.1f, 2.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //リフト
			Vector3(5.5f, -0.0f, 16.0f)
			);
	}

	//左右移動しているボックスの作成
	void GameStage1−6::CreateMoveBoxL() {
		CreateSharedObjectGroup(L"MoveBoxL");
		AddGameObject<MoveBoxL>(
			Vector3(1.5f, 2.0f, 1.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //
			Vector3(-5.2f, -0.0f, -9.0f)
			);
	}

	//左右移動しているボックスの作成
	void GameStage1−6::CreateMoveBoxR() {
		CreateSharedObjectGroup(L"MoveBoxR");
		AddGameObject<MoveBoxR>(
			Vector3(1.5f, 2.0f, 1.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //
			Vector3(4.2f, -0.0f, -9.0f)
			);
	}

	//上下移動しているボックスの作成
	void GameStage1−6::CreateMoveBoxDown() {
		CreateSharedObjectGroup(L"MoveBoxDown");
		AddGameObject<MoveBoxDown>(
			Vector3(5.0f, 3.7f, 1.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //
			Vector3(2.5f, -0.5f, 0.0f)
			);
		AddGameObject<MoveBoxDown>(
			Vector3(5.0f, 3.7f, 1.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //
			Vector3(-3.5f, -0.5f, 0.0f)
			);

	}
	//上下移動しているボックスの作成
	void GameStage1−6::CreateMoveBoxDown2() {
		CreateSharedObjectGroup(L"MoveBoxDown2");
		AddGameObject<MoveBoxDown2>(
			Vector3(5.0f, 3.7f, 1.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //
			Vector3(2.5f, -0.5f, 3.0f)
			);
		AddGameObject<MoveBoxDown2>(
			Vector3(5.0f, 3.7f, 1.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //
			Vector3(-3.5f, -0.5f, 3.0f)
			);

	}
	//上下移動しているボックスの作成
	void GameStage1−6::CreateMoveBoxDown3() {
		CreateSharedObjectGroup(L"MoveBoxDown3");
		AddGameObject<MoveBoxDown3>(
			Vector3(5.0f, 3.7f, 1.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //
			Vector3(2.5f, -0.5f, 6.0f)
			);
		AddGameObject<MoveBoxDown3>(
			Vector3(5.0f, 3.7f, 1.5f),
			Vector3(0.0f, 0.0f, 0.0f),  //
			Vector3(-3.5f, -0.5f, 6.0f)
			);

	}

	//上下移動しているボックスの作成
	void GameStage1−6::CreateMoveBoxDown5() {
		CreateSharedObjectGroup(L"MoveBoxDown5");
		//AddGameObject<MoveBoxDown5>(
		//	Vector3(1.0f, 4.0f, 5.0f),
		//	Vector3(0.0f, 0.0f, 0.0f),  //
		//	Vector3(3.5f, -0.5f, -12.0f)
		//	);
		//AddGameObject<MoveBoxDown5>(
		//	Vector3(1.0f, 4.0f, 5.0f),
		//	Vector3(0.0f, 0.0f, 0.0f),  //
		//	Vector3(7.5f, -0.5f, -12.0f)
		//	);

	}

	//スパークの作成
	void GameStage1−6::CreateSpark() {
		auto MultiSparkPtr = AddGameObject<MultiSpark>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	void GameStage1−6::CreateTimerSprite() {
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto Timer = Ptr->getGameTime();
		//配列の初期化
		vector<Vector3> Vec = {
			{ 40.0f, 350.0f, 0.0f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<TimerSprite>(v, Timer);
		}
	}

	void GameStage1−6::CreateTimerSprite2() {
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto Timer = Ptr->getGameTime();
		//配列の初期化
		vector<Vector3> Vec = {
			{ -10.0f, 350.0f, 0.1f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<TimerSprite2>(v, Timer);
		}
	}
	void GameStage1−6::CreateTimerSprite3() {
		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto Timer = Ptr->getGameTime();
		//配列の初期化
		vector<Vector3> Vec = {
			{ -60.0f, 350.0f, 0.1f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<TimerSprite3>(v, Timer);
		}
	}

	//シーン変更
	void GameStage1−6::SceneChange()
	{
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToResult");
	}

	void GameStage1−6::OnCreate() {
		try {
			auto Ptr = App::GetApp()->GetScene<Scene>();

			//時間の設定
			Ptr->setGameTime(60.0f);

			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//プレートの作成
			CreatePlate();

			//シェアドグループを作成
			CreateSharedObjectGroup(L"MoveTORUS");
			//CreateSharedObjectGroup(L"MoveBox2");

			//CSVでオブジェクト作成
			CreateRoad();
			//固定のボックスの作成
			CreateFixedBox();

			//上下移動しているボックスの作成
			CreateMoveBox2();
			CreateMoveBox3();

			CreateMoveBoxL();
			CreateMoveBoxR();
			CreateMoveBoxDown();
			CreateMoveBoxDown2();
			CreateMoveBoxDown3();
			CreateMoveBoxDown5();

			CreateSpark();

			CreateTimerSprite();
			CreateTimerSprite2();
			CreateTimerSprite3();

			auto FB = AddGameObject<Black>();
			SetSharedGameObject(L"Black", FB);
			auto FBIn = AddGameObject<BlackIn>();
			SetSharedGameObject(L"BlackIn", FBIn);

		}
		catch (...) {
			throw;
		}
	}

	void GameStage1−6::OnUpdate() {

		GetSharedGameObject<BlackIn>(L"BlackIn", false)->StartBlackIn();

		if (GetSharedGameObject<BlackIn>(L"BlackIn", false)->GetBlackInFinish())
		{

			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			//前回のターンからの時間
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			if (CntlVec[0].bConnected) {
				//アームの変更
				//現在のアームの長さを得る
				float ArmLen = m_AtToEyeVec.Length();
				//Dパッド下
				if (CntlVec[0].wButtons & XINPUT_GAMEPAD_DPAD_DOWN) {
					//カメラ位置を引く
					ArmLen += m_ZoomSpeed;
					if (m_ArmMax <= ArmLen) {
						ArmLen = m_ArmMax;
					}
				}
				//Dパッド上
				if (CntlVec[0].wButtons & XINPUT_GAMEPAD_DPAD_UP) {
					//カメラ位置を寄る
					ArmLen -= m_ZoomSpeed;
					if (m_ArmMin >= ArmLen) {
						ArmLen = m_ArmMin;
					}
				}
				//ベクトルを正規化
				m_AtToEyeVec.Normalize();
				//ベクトルにアームの長さを設定
				m_AtToEyeVec *= ArmLen;
			}

			//
			auto PtrCamera = dynamic_pointer_cast<Camera>(GetView()->GetTargetCamera());
			auto PtrPlayer1 = GetSharedGameObject<Player>(L"Player");
			auto PtrPlayer2 = GetSharedGameObject<Player2>(L"Player2");
			if (PtrCamera && PtrPlayer1 && PtrPlayer2) {
				float CenterAtZ =
					PtrPlayer1->GetComponent<Transform>()->GetPosition().z
					+ PtrPlayer2->GetComponent<Transform>()->GetPosition().z;
				//1と2の中間地点をとる
				CenterAtZ *= 0.5f;
				//Cameraに注目するオブジェクト（プレイヤー）の設定
				PtrCamera->SetAt(-0.5f, 0, CenterAtZ);
				PtrCamera->SetEye(PtrCamera->GetAt() + m_AtToEyeVec);
			}


			//コントローラの取得
			if (CntlVec[0].bConnected) {
				//Yボタンが押された瞬間ならステージ推移
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_Y) {
					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-1");
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_X) {
					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToGameStage1-5");
				}
				else if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START) {
					auto ScenePtr = App::GetApp()->GetScene<Scene>();
					PostEvent(0.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToMenuStage");
				}

			}

		}

	}

	void GameStage1−6::OnLastUpdate() {

		float px1 = GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetPosition().x;
		float pz1 = GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetPosition().z;

		float Left1 = -11.7f;
		float Right1 = -1.3f;
		float MaxD1 = 39.2f;
		float MinD1 = -38.0f;

		if (px1 > Left1 && px1 < Right1 && pz1 > MinD1 && pz1 < MaxD1) {
			;
		}
		else
		{
			auto Beforpos = GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetBeforePosition();
			GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->SetPosition(Beforpos);
		}

		float px2 = GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->GetPosition().x;
		float pz2 = GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->GetPosition().z;

		float Left2 = 0.3f;
		float Right2 = 10.7f;
		float MaxD2 = 39.2f;
		float MinD2 = -38.0f;

		if (px2 > Left2 && px2 < Right2 && pz2 > MinD2 && pz2 < MaxD2) {
			;
		}
		else
		{
			auto Beforpos = GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->GetBeforePosition();
			GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->SetPosition(Beforpos);
		}

		auto P1Pos = GetSharedGameObject<Player>(L"Player")->GetComponent<Transform>()->GetPosition();
		auto P2Pos = GetSharedGameObject<Player2>(L"Player2")->GetComponent<Transform>()->GetPosition();
		auto Group = GetSharedObjectGroup(L"MoveTORUS");
		auto P1GoalSpeed = GetSharedGameObject<Player>(L"Player");
		auto P2GoalSpeed = GetSharedGameObject<Player2>(L"Player2");
		bool p1goal = false;
		bool p2goal = false;
		for (auto& v : Group->GetGroupVector()) {
			auto ptrTorus = dynamic_pointer_cast<MoveTORUS>(v.lock());
			auto Sh = v.lock();
			if (ptrTorus) {
				auto ShPos = Sh->GetComponent<Transform>()->GetPosition();
				if (Vector3EX::Length(P1Pos - ShPos) <= 2.0f) {

					//スパークの放出
					auto PtrSpark = GetSharedGameObject<MultiSpark>(L"MultiSpark", false);
					if (PtrSpark) {
						PtrSpark->InsertSpark(ShPos);
					}
					p1goal = true;
				}
				else if (Vector3EX::Length(P2Pos - ShPos) <= 2.0f) {
					//スパークの放出
					auto PtrSpark = GetSharedGameObject<MultiSpark>(L"MultiSpark", false);
					if (PtrSpark) {
						PtrSpark->InsertSpark(ShPos);
					}
					p2goal = true;
				}
			}
		}
		if (p1goal && p2goal) {
			P1GoalSpeed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);
			P2GoalSpeed->AddComponent<Rigidbody>()->SetMaxSpeed(0.0f);
			GetSharedGameObject<Black>(L"Black", false)->StartBlack();
			//auto ScenePtr = App::GetApp()->GetScene<Scene>();
			//PostEvent(1.0f, GetThis<ObjectInterface>(), ScenePtr, L"ToResult");
			//暗転を毎回判定
			//暗転終わり
			if (GetSharedGameObject<Black>(L"Black", false)->GetBlackFinish())
			{
				//シーン切り替え
				SceneChange();
			}
		}

	}



}
//end basecross
