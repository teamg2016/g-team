/*!
@file Menu.h
@brief メニューステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	メニューステージクラス
	//--------------------------------------------------------------------------------------
	class StageSelect3 : public Stage {
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		//void CreatePlate();
		//壁模様のスプライト作成
		void CreateSprite();

		int a = 0;
		bool b = false;

		bool H2 = true;
		bool H3 = true;

		//bool H5 = true;

		shared_ptr<MultiAudioObject> m_AudioObjectPtr;


	public:
		//構築と破棄
		StageSelect3() :Stage() {}
		virtual ~StageSelect3() {}
		void SceneChange1();

		void SceneChange2();

		void SceneChange3();

		void SceneChange6();
		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
	};

}
//end basecross
