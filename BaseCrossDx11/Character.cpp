/*!
@file Character.cpp
@brief キャラクターなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{


	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FixedBox::FixedBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	FixedBox::~FixedBox() {}

	//初期化
	void FixedBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"TUTI_TX");
	}
	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FixedBox2::FixedBox2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	FixedBox2::~FixedBox2() {}

	//初期化
	void FixedBox2::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"NIZIHAI_TX");
	}

	//--------------------------------------------------------------------------------------
	//	class FixedTORUS : public GameObject;
	//	用途: 固定の輪
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FixedTORUS::FixedTORUS(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	FixedTORUS::~FixedTORUS() {}

	//初期化
	void FixedTORUS::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_TORUS");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_TORUS");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SIRO_TX");
	}

	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	CSVFixedSphere::CSVFixedSphere(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(1.0f, 1.0f, 1.0f),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	CSVFixedSphere::~CSVFixedSphere() {}

	//初期化
	void CSVFixedSphere::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//Rigidbodyをつける
		auto PtrRigid = AddComponent<Rigidbody>();
		//横部分のみ反発
		//反発係数は0.5（半分）
		PtrRigid->SetReflection(0.5f);
		//Arrive操舵
		auto PtrArrive = AddComponent<ArriveSteering>();
		//Arriveは無効にしておく
		PtrArrive->SetUpdateActive(false);

		//SPの衝突判定をつける
		auto PtrColl = AddComponent<CollisionSphere>();
		//横部分のみ反発
		PtrColl->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(L"NIZI_TX");
		SetAlphaActive(true);
	}


	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	CSVFixedBox::CSVFixedBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(1.0f,2.0f,1.0f),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	CSVFixedBox::~CSVFixedBox() {}

	//初期化
	void CSVFixedBox::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"TUTI_TX");
		SetAlphaActive(true);
	}

	void CSVFixedBox::OnDraw() {
		auto Mng = GetStage()->GetSharedGameObject<CSVFixedBoxManager>(L"CSVFixedBoxManager", false);
		if (Mng) {
			auto PtrTrans = GetComponent<Transform>();
			auto Mat = PtrTrans->GetWorldMatrix();
			Mat.Transpose();
			Mng->InsertCSVFixedBox(Mat);
		}
		else {
			GameObject::OnDraw();
		}
	}

	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	CSVFixedBox2::CSVFixedBox2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(1.0f, 2.5f, 1.0f),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	CSVFixedBox2::~CSVFixedBox2() {}

	//初期化
	void CSVFixedBox2::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"TUTI_TX");
	}

	void CSVFixedBox2::OnDraw() {
		auto Mng = GetStage()->GetSharedGameObject<CSVFixedBoxManager>(L"CSVFixedBoxManager", false);
		if (Mng) {
			auto PtrTrans = GetComponent<Transform>();
			auto Mat = PtrTrans->GetWorldMatrix();
			Mat.Transpose();
			Mng->InsertCSVFixedBox(Mat);
		}
		else {
			GameObject::OnDraw();
		}
	}

	IMPLEMENT_DX11_VERTEX_SHADER(VSPNTInstance, App::GetApp()->m_wstrRelativeShadersPath + L"VSPNTInstance.cso")

		//--------------------------------------------------------------------------------------
		//	class CSVFixedBoxManager : public GameObject;
		//	用途: CSV用の固定のボックスの描画クラス
		//--------------------------------------------------------------------------------------

		void CSVFixedBoxManager::CreateBuffers() {
		//Cubeの作成
		float HelfSize = 0.5f;
		vector<Vector3> PosVec = {
			{ Vector3(-HelfSize, HelfSize, -HelfSize) },
			{ Vector3(HelfSize, HelfSize, -HelfSize) },
			{ Vector3(-HelfSize, -HelfSize, -HelfSize) },
			{ Vector3(HelfSize, -HelfSize, -HelfSize) },
			{ Vector3(HelfSize, HelfSize, HelfSize) },
			{ Vector3(-HelfSize, HelfSize, HelfSize) },
			{ Vector3(HelfSize, -HelfSize, HelfSize) },
			{ Vector3(-HelfSize, -HelfSize, HelfSize) },
		};
		vector<UINT> PosIndeces = {
			0, 1, 2, 3,
			1, 4, 3, 6,
			4, 5, 6, 7,
			5, 0, 7, 2,
			5, 4, 0, 1,
			2, 3, 7, 6,
		};


		vector<Vector3> FaceNormalVec = {
			{ Vector3(0, 0, -1.0f) },
			{ Vector3(1.0f, 0, 0) },
			{ Vector3(0, 0, 1.0f) },
			{ Vector3(-1.0f, 0, 0) },
			{ Vector3(0, 1.0f, 0) },
			{ Vector3(0, -1.0f, 0) }
		};

		vector<VertexPositionNormalTexture> vertices;
		vector<uint16_t> indices;
		UINT BasePosCount = 0;
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 4; j++) {
				VertexPositionNormalTexture Data;
				Data.position = PosVec[PosIndeces[BasePosCount + j]];
				Data.normal = FaceNormalVec[i];
				switch (j) {
				case 0:
					Data.textureCoordinate = Vector2(0, 0);
					break;
				case 1:
					Data.textureCoordinate = Vector2(1.0f, 0);
					break;
				case 2:
					Data.textureCoordinate = Vector2(0, 1.0f);
					break;
				case 3:
					Data.textureCoordinate = Vector2(1.0f, 1.0f);
					break;
				}
				vertices.push_back(Data);
			}

			indices.push_back((uint16_t)BasePosCount + 0);
			indices.push_back((uint16_t)BasePosCount + 1);
			indices.push_back((uint16_t)BasePosCount + 2);
			indices.push_back((uint16_t)BasePosCount + 1);
			indices.push_back((uint16_t)BasePosCount + 3);
			indices.push_back((uint16_t)BasePosCount + 2);

			BasePosCount += 4;
		}
		//メッシュの作成（変更できない）
		m_CubeMesh = MeshResource::CreateMeshResource(vertices, indices, false);
		//インスタンス行列バッファの作成
		//Max値で作成する
		vector<Matrix4X4> matrices(m_MaxInstance);
		for (auto& m : matrices) {
			m = Matrix4X4();
		}
		MeshResource::CreateDynamicVertexBuffer(m_MatrixBuffer, matrices);
	}


	CSVFixedBoxManager::CSVFixedBoxManager(const shared_ptr<Stage>& StagePtr,
		const wstring& TextureFileName)
		:GameObject(StagePtr),
		m_TextureFileName(TextureFileName),
		m_MaxInstance(2000)
	{

	}

	void CSVFixedBoxManager::OnCreate() {
		CreateBuffers();
		//テクスチャの作成
		m_TextureResource = ObjectFactory::Create<TextureResource>(m_TextureFileName, L"WIC");
	}

	void CSVFixedBoxManager::InsertCSVFixedBox(const Matrix4X4& m) {
		m_CubeMatrixVec.push_back(m);
	}
	void CSVFixedBoxManager::OnDraw() {
		if (m_CubeMatrixVec.empty()) {
			return;
		}

		//デバイスの取得
		auto Dev = App::GetApp()->GetDeviceResources();
		auto pDx11Device = Dev->GetD3DDevice();
		auto pD3D11DeviceContext = Dev->GetD3DDeviceContext();
		//インスタンスバッファにマップ
		D3D11_MAP mapType = D3D11_MAP_WRITE_DISCARD;
		D3D11_MAPPED_SUBRESOURCE mappedBuffer;
		//行列のマップ
		if (FAILED(pD3D11DeviceContext->Map(m_MatrixBuffer.Get(), 0, mapType, 0, &mappedBuffer))) {
			// Map失敗
			throw BaseException(
				L"行列のMapに失敗しました。",
				L"if(FAILED(pID3D11DeviceContext->Map()))",
				L"CSVFixedBoxManager::OnDraw()"
			);
		}
		//行列の変更
		auto* matrices = (Matrix4X4*)mappedBuffer.pData;
		Matrix4X4 World;
		for (size_t i = 0; i < m_CubeMatrixVec.size(); i++) {
			World = m_CubeMatrixVec[i];
			//転置する
			World.Transpose();
			matrices[i] = m_CubeMatrixVec[i];
		}
		//アンマップ
		pD3D11DeviceContext->Unmap(m_MatrixBuffer.Get(), 0);

		auto RenderState = Dev->GetRenderState();


		//ストライドとオフセット
		//形状の頂点バッファと行列バッファを設定
		UINT stride[2] = { sizeof(VertexPositionNormalTexture), sizeof(Matrix4X4) };
		UINT offset[2] = { 0, 0 };

		ID3D11Buffer* pBuf[2] = { m_CubeMesh->GetVertexBuffer().Get(), m_MatrixBuffer.Get() };
		pD3D11DeviceContext->IASetVertexBuffers(0, 2, pBuf, stride, offset);
		//インデックスバッファのセット
		pD3D11DeviceContext->IASetIndexBuffer(m_CubeMesh->GetIndexBuffer().Get(), DXGI_FORMAT_R16_UINT, 0);

		//描画方法（3角形）
		pD3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		//シェーダの設定
		pD3D11DeviceContext->VSSetShader(VSPNTInstance::GetPtr()->GetShader(), nullptr, 0);
		pD3D11DeviceContext->PSSetShader(PSPNTStatic::GetPtr()->GetShader(), nullptr, 0);
		//インプットレイアウトの設定
		pD3D11DeviceContext->IASetInputLayout(VSPNTInstance::GetPtr()->GetInputLayout());

		//ブレンドステート
		//透明処理しない
		pD3D11DeviceContext->OMSetBlendState(RenderState->GetOpaque(), nullptr, 0xffffffff);
		//デプスステンシルステート
		pD3D11DeviceContext->OMSetDepthStencilState(RenderState->GetDepthDefault(), 0);
		//テクスチャとサンプラーの設定
		ID3D11ShaderResourceView* pNull[1] = { 0 };
		pD3D11DeviceContext->PSSetShaderResources(0, 1, m_TextureResource->GetShaderResourceView().GetAddressOf());
		ID3D11SamplerState* pSampler = RenderState->GetLinearClamp();
		pD3D11DeviceContext->PSSetSamplers(0, 1, &pSampler);
		//ラスタライザステート（表面描画）
		pD3D11DeviceContext->RSSetState(RenderState->GetCullBack());

		//ビュー行列の決定
		Matrix4X4 View, Proj;
		//カメラを得る
		auto CameraPtr = OnGetDrawCamera();
		//ビューと射影行列を得る
		View = CameraPtr->GetViewMatrix();
		//転置する
		View.Transpose();
		//転置する
		Proj = CameraPtr->GetProjMatrix();
		Proj.Transpose();
		//コンスタントバッファの準備
		StaticLightingConstantBuffer sb;

		sb.World = Matrix4X4();	//ワールド行列はダミー
		sb.View = View;
		sb.Projection = Proj;
		//ライティング
		auto StageLight = OnGetDrawLight();
		sb.LightDir = StageLight.m_Directional;
		sb.LightDir.w = 1.0f;
		//ディフューズ
		sb.Diffuse = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		//エミッシブ加算は行わない。
		sb.Emissive = Color4(0, 0, 0, 0);
		//コンスタントバッファの更新
		pD3D11DeviceContext->UpdateSubresource(CBStaticLighting::GetPtr()->GetBuffer(), 0, nullptr, &sb, 0, 0);

		//コンスタントバッファの設定
		ID3D11Buffer* pConstantBuffer = CBStaticLighting::GetPtr()->GetBuffer();
		ID3D11Buffer* pNullConstantBuffer = nullptr;
		//頂点シェーダに渡す
		pD3D11DeviceContext->VSSetConstantBuffers(0, 1, &pConstantBuffer);
		//ピクセルシェーダに渡す
		pD3D11DeviceContext->PSSetConstantBuffers(0, 1, &pConstantBuffer);
		//描画
		//		pD3D11DeviceContext->DrawIndexedInstanced(m_CubeMesh->GetNumIndicis(), 10, 0, 0, 0);
		pD3D11DeviceContext->DrawIndexedInstanced(m_CubeMesh->GetNumIndicis(), m_CubeMatrixVec.size(), 0, 0, 0);
		//後始末
		Dev->InitializeStates();

		m_CubeMatrixVec.clear();
	}

	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	CSVFixedBox3::CSVFixedBox3(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(1.0f, 2.5f, 1.0f),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	CSVFixedBox3::~CSVFixedBox3() {}

	//初期化
	void CSVFixedBox3::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"TUTI_TX");
	}


	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	CSVFixedBox5::CSVFixedBox5(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(1.0f, 2.5f, 1.0f),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	CSVFixedBox5::~CSVFixedBox5() {}

	//初期化
	void CSVFixedBox5::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"UTYU_TX");
	}

	//--------------------------------------------------------------------------------------
	//	class FixedBox2 : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	CSVFixedBoxMin::CSVFixedBoxMin(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(1.0f, 1.0f, 1.0f),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	CSVFixedBoxMin::~CSVFixedBoxMin() {}

	//初期化
	void CSVFixedBoxMin::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"TUTI_TX");
	}


	//--------------------------------------------------------------------------------------
	//	class MoveBox : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxGoalL::MoveBoxGoalL(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxGoalL::~MoveBoxGoalL() {}

	//初期化
	void MoveBoxGoalL::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(1.0f, Vector3(3.0f, 0.0f, 0.0f));
		PtrAction->AddMoveBy(1.0f, Vector3(-3.0f, 0.0f, 0.0f));

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();



		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxGoalL");
		Group->IntoGroup(GetThis<GameObject>());


	}

	//--------------------------------------------------------------------------------------
	//	class MoveBoxGoalR : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxGoalR::MoveBoxGoalR(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxGoalR::~MoveBoxGoalR() {}

	//初期化
	void MoveBoxGoalR::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(1.0f, Vector3(-3.0f, 0.0f, 0.0f));
		PtrAction->AddMoveBy(1.0f, Vector3(3.0f, 0.0f, 0.0f));

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxGoalR");
		Group->IntoGroup(GetThis<GameObject>());


	}
	//--------------------------------------------------------------------------------------
	//	class MoveBoxL : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxL::MoveBoxL(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxL::~MoveBoxL() {}

	//初期化
	void MoveBoxL::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(1.0f, Vector3(3.5f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(1.0f);
		PtrAction->AddMoveBy(1.0f, Vector3(-3.5f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(1.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();



		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxL");
		Group->IntoGroup(GetThis<GameObject>());


	}

	//--------------------------------------------------------------------------------------
	//	class MoveBoxR : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxR::MoveBoxR(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxR::~MoveBoxR() {}

	//初期化
	void MoveBoxR::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(1.0f, Vector3(-3.5f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(1.0f);
		PtrAction->AddMoveBy(1.0f, Vector3(3.5f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(1.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxR");
		Group->IntoGroup(GetThis<GameObject>());


	}


	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown : public GameObject;
	//	用途: イージーステージ　下に下がる床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxDown::MoveBoxDown(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxDown::~MoveBoxDown() {}

	//初期化
	void MoveBoxDown::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveInterval(3.0f);
		PtrAction->AddMoveBy(0.3f, Vector3(0.0f, -3.5f, 0.0f));
		PtrAction->AddMoveInterval(3.0f);
		PtrAction->AddMoveBy(1.0f, Vector3(0.0f, 3.5f, 0.0f));

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxDown");
		Group->IntoGroup(GetThis<GameObject>());

	}
	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown2 : public GameObject;
	//	用途: イージーステージ　下に下がる床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxDown2::MoveBoxDown2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxDown2::~MoveBoxDown2() {}

	//初期化
	void MoveBoxDown2::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveInterval(3.5f);
		PtrAction->AddMoveBy(0.3f, Vector3(0.0f, -3.5f, 0.0f));
		PtrAction->AddMoveInterval(2.5f);
		PtrAction->AddMoveBy(1.0f, Vector3(0.0f, 3.5f, 0.0f));

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxDown2");
		Group->IntoGroup(GetThis<GameObject>());

	}
	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown3 : public GameObject;
	//	用途: イージーステージ　下に下がる床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxDown3::MoveBoxDown3(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxDown3::~MoveBoxDown3() {}

	//初期化
	void MoveBoxDown3::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveInterval(4.0f);
		PtrAction->AddMoveBy(0.3f, Vector3(0.0f, -3.5f, 0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(1.0f, Vector3(0.0f, 3.5f, 0.0f));

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxDown3");
		Group->IntoGroup(GetThis<GameObject>());

	}
	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown4 : public GameObject;
	//	用途: ノーマルステージ　下に下がる床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxDown4::MoveBoxDown4(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxDown4::~MoveBoxDown4() {}

	//初期化
	void MoveBoxDown4::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveInterval(4.0f);
		PtrAction->AddMoveBy(0.3f, Vector3(0.0f, -3.5f, 0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(1.0f, Vector3(0.0f, 3.5f, 0.0f));

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxDown4");
		Group->IntoGroup(GetThis<GameObject>());

	}
	//--------------------------------------------------------------------------------------
	//	class MoveBoxDown5 : public GameObject;
	//	用途: イージーステージ　下に下がる床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxDown5::MoveBoxDown5(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxDown5::~MoveBoxDown5() {}

	//初期化
	void MoveBoxDown5::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveInterval(6.0f);
		PtrAction->AddMoveBy(4.0f, Vector3(0.0f, -3.5f, 0.0f));
		PtrAction->AddMoveInterval(200.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxDown5");
		Group->IntoGroup(GetThis<GameObject>());

	}

	//--------------------------------------------------------------------------------------
	//	class MoveBox2 : public GameObject;
	//	用途: 上下移動する壁
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBox2::MoveBox2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{}

	MoveBox2::~MoveBox2() {}

	//初期化
	void MoveBox2::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(0.5f, Vector3(0.0f, 5.0f, 0));
		PtrAction->AddMoveBy(0.5f, Vector3(0.0f, -5.0f, 0));
		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBox2");
		Group->IntoGroup(GetThis<GameObject>());


	}

	//--------------------------------------------------------------------------------------
	//	class MoveBox3 : public GameObject;
	//	用途: リフト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBox3::MoveBox3(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBox3::~MoveBox3() {}

	//初期化
	void MoveBox3::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, 1.5f, 0));
		PtrAction->AddMoveInterval(1.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, -1.5f, 0));
		PtrAction->AddMoveInterval(1.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBox3");
		Group->IntoGroup(GetThis<GameObject>());


	}




	//--------------------------------------------------------------------------------------
	//	class MoveBox4 : public GameObject;
	//	用途: 左回転するボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBox4::MoveBox4(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBox4::~MoveBox4() {}

	//初期化
	void MoveBox4::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddRotateBy(1.0f, Vector3(0.0f, 0.0f, 10.0f));
		PtrAction->AddRotateInterval(2.0f);
		PtrAction->AddRotateBy(1.0f, Vector3(0.0f, 0.0f, 10.0f));
		PtrAction->AddRotateInterval(2.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBox4");
		Group->IntoGroup(GetThis<GameObject>());


	}

	//--------------------------------------------------------------------------------------
	//	class MoveBox5 : public GameObject;
	//	用途: 右回転するボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBox5::MoveBox5(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBox5::~MoveBox5() {}

	//初期化
	void MoveBox5::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddRotateBy(0.5f, Vector3(0.0f, 1.6f, 0));
		PtrAction->AddRotateInterval(1.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBox5");
		Group->IntoGroup(GetThis<GameObject>());


	}


	//--------------------------------------------------------------------------------------
	//	class MoveBox6 : public GameObject;
	//	用途: 
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxRO::MoveBoxRO(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxRO::~MoveBoxRO() {}

	//初期化
	void MoveBoxRO::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddRotateBy(5.0f, Vector3(0.0f, 4.0f, 0));

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxRO");
		Group->IntoGroup(GetThis<GameObject>());

	}
	//--------------------------------------------------------------------------------------
	//	class MoveBox6 : public GameObject;
	//	用途: 
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxRO2::MoveBoxRO2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxRO2::~MoveBoxRO2() {}

	//初期化
	void MoveBoxRO2::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddRotateBy(5.0f, Vector3(0.0f, -4.0f, 0));

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxRO2");
		Group->IntoGroup(GetThis<GameObject>());

	}

	//--------------------------------------------------------------------------------------
	//	class MoveBoxRec : public GameObject;
	//	用途: 
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveBoxRec::MoveBoxRec(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveBoxRec::~MoveBoxRec() {}

	//初期化
	void MoveBoxRec::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		
		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveInterval(5.0f);
		PtrAction->AddMoveBy(25.0f, Vector3(0.0f, 0.0f, 50.0f));
		PtrAction->AddMoveInterval(3.0f);
		PtrAction->AddMoveBy(15.0f, Vector3(0.0f, 0.0f, 60.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(5.0f, Vector3(0.0f, 0.0f, 30.0f));
		//アクション開始
		PtrAction->Run();
		

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBoxRec");
		Group->IntoGroup(GetThis<GameObject>());

	}


	//--------------------------------------------------------------------------------------
	//	class MoveTORUS : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveTORUS::MoveTORUS(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(4.0f, 1.0f, 4.0f),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveTORUS::~MoveTORUS() {}

	//初期化
	void MoveTORUS::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddRotateBy(0.5f, Vector3(0.0f, 5.0f, 0.0f));


		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_TORUS");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_TORUS");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"NIZI_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveTORUS");
		Group->IntoGroup(GetThis<MoveTORUS>());


	}

	//--------------------------------------------------------------------------------------
	//	class MovePlate : public GameObject;
	//	用途: 傾く床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MovePlate::MovePlate(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MovePlate::~MovePlate() {}

	//初期化
	void MovePlate::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
			PtrAction->AddRotateBy(10.0f, Vector3(0.0f, 0.0f, 0.1f));
			PtrAction->AddRotateInterval(10.0f);
			PtrAction->AddRotateBy(10.0f, Vector3(0.0f, 0.0f, -0.1f));
			PtrAction->AddRotateInterval(5.0f);
			PtrAction->AddRotateBy(10.0f, Vector3(0.0f, 0.0f, -0.1f));
			PtrAction->AddRotateInterval(10.0f);
			PtrAction->AddRotateBy(10.0f, Vector3(0.0f, 0.0f, 0.1f));
			PtrAction->AddRotateInterval(5.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"MIZUIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MovePlate");
		Group->IntoGroup(GetThis<GameObject>());


	}


	//--------------------------------------------------------------------------------------
	//	class MoveSphere : public GameObject;
	//	用途: 雪玉・岩
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveSphere::MoveSphere(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveSphere::~MoveSphere() {}

	//初期化
	void MoveSphere::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(3.0f, Vector3(8.0f,0.0f,0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, 0.0f, 6.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(-8.0f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, 0.0f, -6.0f));
		PtrAction->AddMoveInterval(2.0f);

		//PtrAction->AddRotateBy(3.0f, Vector3(5.0f, 0.0f, 5.0f));
		//PtrAction->AddRotateInterval(2.0f);
		//PtrAction->AddRotateBy(3.0f, Vector3(-5.0f, 0.0f, 5.0f));
		//PtrAction->AddRotateInterval(2.0f);


		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveSphere");
		Group->IntoGroup(GetThis<GameObject>());

	}
	//--------------------------------------------------------------------------------------
	//	class MoveSphere : public GameObject;
	//	用途: 雪玉・岩
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveSphereL::MoveSphereL(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveSphereL::~MoveSphereL() {}

	//初期化
	void MoveSphereL::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(3.0f, Vector3(-8.0f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, 0.0f, 6.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(8.0f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, 0.0f, -6.0f));
		PtrAction->AddMoveInterval(2.0f);

		//PtrAction->AddRotateBy(3.0f, Vector3(-5.0f, 0.0f, 5.0f));
		//PtrAction->AddRotateInterval(2.0f);
		//PtrAction->AddRotateBy(3.0f, Vector3(5.0f, 0.0f, 5.0f));
		//PtrAction->AddRotateInterval(2.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveSphereL");
		Group->IntoGroup(GetThis<GameObject>());

	}
	//--------------------------------------------------------------------------------------
	//	class MoveSphere : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveSphere2::MoveSphere2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveSphere2::~MoveSphere2() {}

	//初期化
	void MoveSphere2::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(3.0f, Vector3(-8.0f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, 0.0f, -6.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(8.0f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, 0.0f, 6.0f));
		PtrAction->AddMoveInterval(2.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveSphere2");
		Group->IntoGroup(GetThis<GameObject>());

	}
	//--------------------------------------------------------------------------------------
	//	class MoveSphere : public GameObject;
	//	用途: 左右移動するボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MoveSphereL2::MoveSphereL2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	MoveSphereL2::~MoveSphereL2() {}

	//初期化
	void MoveSphereL2::OnCreate() {
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(3.0f, Vector3(8.0f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, 0.0f, -6.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(-8.0f, 0.0f, 0.0f));
		PtrAction->AddMoveInterval(2.0f);
		PtrAction->AddMoveBy(3.0f, Vector3(0.0f, 0.0f, 6.0f));
		PtrAction->AddMoveInterval(2.0f);

		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SIRO_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"MoveSphereL2");
		Group->IntoGroup(GetThis<GameObject>());

	}


	//--------------------------------------------------------------------------------------
	//	class SphereObject : public GameObject;
	//	用途: コイン
	//--------------------------------------------------------------------------------------
	//構築と破棄
	SphereObject::SphereObject(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos)
	{}
	SphereObject::~SphereObject() {}
	//初期化
	void SphereObject::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(0.5f, 0.5f, 0.5f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//操舵系のコンポーネントをつける場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		//Sphereの衝突判定をつける
		auto CollPtr = AddComponent<CollisionSphere>();
		CollPtr->SetFixed(true);
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(L"KIIRO_TX");
	}

	void SphereObject::Col() {
		SetDrawActive(false);
		GetComponent<CollisionSphere>()->SetUpdateActive(false);
		auto Ptr = App::GetApp()->GetScene<Scene>();
		Ptr->ScoreUp();
		GetStage()->GetSharedGameObject<NumberSprite>(L"SCORE")->SetNum(Ptr->GetScore());
	}

	//--------------------------------------------------------------------------------------
	//class MultiSpark : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark::MultiSpark(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark::~MultiSpark() {}

	//初期化
	void MultiSpark::OnCreate() {
	}


	void MultiSpark::InsertSpark(const Vector3& Pos) {
		auto ParticlePtr = InsertParticle(4);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 2.0f - 1.0f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 2.0f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 2.0f - 1.0f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 1.0f,
				rParticleSprite.m_LocalPos.y * 1.0f,
				rParticleSprite.m_LocalPos.z * 1.0f
			);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	//--------------------------------------------------------------------------------------
	//class MultiSpark2 : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark2::MultiSpark2(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark2::~MultiSpark2() {}

	//初期化
	void MultiSpark2::OnCreate() {
	}


	void MultiSpark2::InsertSpark(const Vector3& Pos) {
		auto ParticlePtr = InsertParticle(4);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 2.0f - 1.0f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 2.0f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 2.0f - 1.0f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 1.0f,
				rParticleSprite.m_LocalPos.y * 1.0f,
				rParticleSprite.m_LocalPos.z * 1.0f
			);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 0.0f, 1.0f);
		}
	}

	//--------------------------------------------------------------------------------------
	//class MultiSpark3 : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark3::MultiSpark3(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark3::~MultiSpark3() {}

	//初期化
	void MultiSpark3::OnCreate() {
	}


	void MultiSpark3::InsertSpark(const Vector3& Pos) {
		auto ParticlePtr = InsertParticle(4);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 2.0f - 1.0f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 2.0f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 2.0f - 1.0f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 1.0f,
				rParticleSprite.m_LocalPos.y * 1.0f,
				rParticleSprite.m_LocalPos.z * 1.0f
			);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 0.0f, 0.0f, 1.0f);
		}
	}

	//--------------------------------------------------------------------------------------
	//class MultiSpark4 : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark4::MultiSpark4(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark4::~MultiSpark4() {}

	//初期化
	void MultiSpark4::OnCreate() {
	}


	void MultiSpark4::InsertSpark(const Vector3& Pos) {
		auto ParticlePtr = InsertParticle(4);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()) {
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 2.0f - 1.0f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 2.0f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 2.0f - 1.0f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 1.0f,
				rParticleSprite.m_LocalPos.y * 1.0f,
				rParticleSprite.m_LocalPos.z * 1.0f
			);
			//色の指定
			rParticleSprite.m_Color = Color4(0.0f, 1.0f, 1.0f, 1.0f);
		}
	}

		//--------------------------------------------------------------------------------------
	///	スプライト
	//--------------------------------------------------------------------------------------
	Sprite::Sprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos):
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos)
	{}

	Sprite::~Sprite() {}
	void Sprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列(縦横5個ずつ表示)
		//インデックス配列
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>();
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);
		//透明処理
		SetAlphaActive(true);



	}

	//--------------------------------------------------------------------------------------
	///	スコア表示のスプライト
	//--------------------------------------------------------------------------------------
	ScoreSprite::ScoreSprite(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
		const wstring& TextureKey, bool Trace,
		const Vector2& StartScale, const Vector2& StartPos) :
		GameObject(StagePtr),
		m_NumberOfDigits(NumberOfDigits),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_Score(0.0f)
	{}

	void ScoreSprite::OnCreate() {
		float XPiecesize = 1.0f / (float)m_NumberOfDigits;
		float HelfSize = 0.5f;

		//インデックス配列
		vector<uint16_t> indices;
		for (UINT i = 0; i < m_NumberOfDigits; i++) {
			float Vertex0 = -HelfSize + XPiecesize * (float)i;
			float Vertex1 = Vertex0 + XPiecesize;
			//0
			m_BackupVertices.push_back(
				VertexPositionTexture(Vector3(Vertex0, HelfSize, 0), Vector2(0.0f, 0.0f))
			);
			//1
			m_BackupVertices.push_back(
				VertexPositionTexture(Vector3(Vertex1, HelfSize, 0), Vector2(0.1f, 0.0f))
			);
			//2
			m_BackupVertices.push_back(
				VertexPositionTexture(Vector3(Vertex0, -HelfSize, 0), Vector2(0.0f, 1.0f))
			);
			//3
			m_BackupVertices.push_back(
				VertexPositionTexture(Vector3(Vertex1, -HelfSize, 0), Vector2(0.1f, 1.0f))
			);
			indices.push_back(i * 4 + 0);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 2);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 3);
			indices.push_back(i * 4 + 2);
		}

		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetWrapSampler(true);
		PtrDraw->SetTextureResource(m_TextureKey);

		GetStage()->SetSharedGameObject(L"ScoreSprite", GetThis<ScoreSprite>());
	}

	void ScoreSprite::OnUpdate() {
		vector<VertexPositionTexture> NewVertices;
		UINT Num;
		int VerNum = 0;
		for (UINT i = m_NumberOfDigits; i > 0; i--) {
			UINT Base = (UINT)pow(10, i);
			Num = ((UINT)m_Score) % Base;
			Num = Num / (Base / 10);
			Vector2 UV0 = m_BackupVertices[VerNum].textureCoordinate;
			UV0.x = (float)Num / 10.0f;
			auto v = VertexPositionTexture(
				m_BackupVertices[VerNum].position,
				UV0
			);
			NewVertices.push_back(v);

			Vector2 UV1 = m_BackupVertices[VerNum + 1].textureCoordinate;
			UV1.x = UV0.x + 0.1f;
			v = VertexPositionTexture(
				m_BackupVertices[VerNum + 1].position,
				UV1
			);
			NewVertices.push_back(v);

			Vector2 UV2 = m_BackupVertices[VerNum + 2].textureCoordinate;
			UV2.x = UV0.x;

			v = VertexPositionTexture(
				m_BackupVertices[VerNum + 2].position,
				UV2
			);
			NewVertices.push_back(v);

			Vector2 UV3 = m_BackupVertices[VerNum + 3].textureCoordinate;
			UV3.x = UV0.x + 0.1f;

			v = VertexPositionTexture(
				m_BackupVertices[VerNum + 3].position,
				UV3
			);
			NewVertices.push_back(v);

			VerNum += 4;
		}
		auto PtrDraw = GetComponent<PTSpriteDraw>();
		PtrDraw->UpdateVertices(NewVertices);
	}

	//--------------------------------------------------------------------------------------
	//	class Black : public GameObject;
	//	用途: 暗転用黒
	//--------------------------------------------------------------------------------------
	Black::Black(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	void Black::OnCreate()
	{
		//ウィンドウサイズ取得
		Vector2 WindowSize = Vector2((float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		auto Ptr = GetComponent<Transform>();
		Ptr->SetPosition(0, 0, 0);
		Ptr->SetScale(WindowSize.x, WindowSize.y, 1);
		Ptr->SetRotation(0, 0, 0);

		//テクスチャをつける
		auto PtrSprite = this->AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"KURO_TX");

		//透明化
		PtrSprite->SetDiffuse(Color4(1, 1, 1, 0));

		//表示レイヤー10設定
		SetDrawLayer(10);

		//透明度反映
		SetAlphaActive(true);

	}

	void Black::OnUpdate()
	{
		//起動状態
		if (m_StartFlg)
		{
			//実体化
			auto PtrSprite = this->AddComponent<PCTSpriteDraw>();
			m_alpha += 0.03f;
			PtrSprite->SetDiffuse(Color4(1, 1, 1, m_alpha));

			if (m_alpha >= 1.0f)
			{
				m_StartFlg = false;
				m_EndFlg = true;
			}
		}
	}

	void Black::StartBlack()
	{
		m_StartFlg = true;
	}

	bool Black::GetBlackFinish()
	{
		return m_EndFlg;
	}

	//--------------------------------------------------------------------------------------
	//	class White : public GameObject;
	//	用途: 暗転用黒
	//--------------------------------------------------------------------------------------
	White::White(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	void White::OnCreate()
	{
		//ウィンドウサイズ取得
		Vector2 WindowSize = Vector2((float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		auto Ptr = GetComponent<Transform>();
		Ptr->SetPosition(0, 0, 0);
		Ptr->SetScale(WindowSize.x, WindowSize.y, 1);
		Ptr->SetRotation(0, 0, 0);

		//テクスチャをつける
		auto PtrSprite = this->AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"KURO_TX");

		//透明化
		PtrSprite->SetDiffuse(Color4(1, 1, 1, 0));

		//表示レイヤー10設定
		SetDrawLayer(10);

		//透明度反映
		SetAlphaActive(true);

	}

	void White::OnUpdate()
	{
		//起動状態
		if (m_StartFlg)
		{
			//実体化
			auto PtrSprite = this->AddComponent<PCTSpriteDraw>();
			m_alpha += 0.03f;
			PtrSprite->SetDiffuse(Color4(1, 1, 1, m_alpha));

			if (m_alpha >= 1.0f)
			{
				m_StartFlg = false;
				m_EndFlg = true;
			}
		}
	}

	void White::StartWhite()
	{
		m_StartFlg = true;
	}

	bool White::GetWhiteFinish()
	{
		return m_EndFlg;
	}

	//--------------------------------------------------------------------------------------
	//	class Black : public GameObject;
	//	用途: 暗転用黒
	//--------------------------------------------------------------------------------------
	BlackIn::BlackIn(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	void BlackIn::OnCreate()
	{
		//ウィンドウサイズ取得
		Vector2 WindowSize = Vector2((float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		auto Ptr = GetComponent<Transform>();
		Ptr->SetPosition(0, 0, 0);
		Ptr->SetScale(WindowSize.x, WindowSize.y, 1);
		Ptr->SetRotation(0, 0, 0);

		//テクスチャをつける
		auto PtrSprite = this->AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"KURO_TX");

		//透明化
		PtrSprite->SetDiffuse(Color4(1, 1, 1, 1));

		//表示レイヤー10設定
		SetDrawLayer(10);

		//透明度反映
		SetAlphaActive(true);

	}

	void BlackIn::OnUpdate()
	{
		//起動状態
		if (m_StartFlg)
		{
			//実体化
			auto PtrSprite = this->AddComponent<PCTSpriteDraw>();
			m_alpha += 0.03f;
			PtrSprite->SetDiffuse(Color4(1, 1, 1, 1-m_alpha));

			if (m_alpha >= 1.0f)
			{
				m_StartFlg = false;
				m_EndFlg = true;
			}
		}
	}

	void BlackIn::StartBlackIn()
	{
		m_StartFlg = true;
	}

	bool BlackIn::GetBlackInFinish()
	{
		return m_EndFlg;
	}

	//--------------------------------------------------------------------------------------
	//	class White : public GameObject;
	//	用途: 暗転用黒
	//--------------------------------------------------------------------------------------
	WhiteIn::WhiteIn(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	void WhiteIn::OnCreate()
	{
		//ウィンドウサイズ取得
		Vector2 WindowSize = Vector2((float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());

		auto Ptr = GetComponent<Transform>();
		Ptr->SetPosition(0, 0, 0);
		Ptr->SetScale(WindowSize.x, WindowSize.y, 1);
		Ptr->SetRotation(0, 0, 0);

		//テクスチャをつける
		auto PtrSprite = this->AddComponent<PCTSpriteDraw>();
		PtrSprite->SetTextureResource(L"KURO_TX");

		//透明化
		PtrSprite->SetDiffuse(Color4(1, 1, 1, 0));

		//表示レイヤー10設定
		SetDrawLayer(10);

		//透明度反映
		SetAlphaActive(true);

	}

	void WhiteIn::OnUpdate()
	{
		//起動状態
		if (m_StartFlg)
		{
			//実体化
			auto PtrSprite = this->AddComponent<PCTSpriteDraw>();
			m_alpha += 0.03f;
			PtrSprite->SetDiffuse(Color4(1, 1, 1, m_alpha));

			if (m_alpha >= 1.0f)
			{
				m_StartFlg = false;
				m_EndFlg = true;
			}
		}
	}

	void WhiteIn::StartWhiteIn()
	{
		m_StartFlg = true;
	}

	bool WhiteIn::GetWhiteInFinish()
	{
		return m_EndFlg;
	}

	//--------------------------------------------------------------------------------------
	//	class Switch_Floor : public GameObject;
	//	用途: スイッチ床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Switch_Floor::Switch_Floor(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position,
		const int& switchno
	) :
		GameObject(StagePtr),
		m_Position(Position),
		m_SwitchNo(switchno)
	{
	}
	Switch_Floor::~Switch_Floor() {}

	//初期化
	void Switch_Floor::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(Vector3(1,0.3f,1));
		PtrTransform->SetRotation(Vector3(0,0,0));
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionSphere>();
		PtrObb->SetFixed(true);


		AddComponent<Rigidbody>();
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SA_TX");

		GetStage()->DoorSwitch = true;
	}

	void Switch_Floor::OnUpdate() {
	}

	void Switch_Floor::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {
	}
	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: スイッチに対応したどあ
	//--------------------------------------------------------------------------------------
	//構築と破棄
	DoorModel::DoorModel(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Position,
		const int& doorno
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_DoorNo(doorno)
	{
	}
	DoorModel::~DoorModel() {}

	//初期化
	void DoorModel::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(Vector3(0, 0, 0));
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"USUIAO_TX");

		GetStage()->DoorSwitch = true;

	}

	void DoorModel::Open() {

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(1.0f, Vector3(0.0f, -3.0f, 0.0f));

		//ループする
		PtrAction->SetLooped(false);
		//アクション開始
		PtrAction->Run();

	}

	//--------------------------------------------------------------------------------------
	//	class Switch_Floor : public GameObject;
	//	用途: スイッチ床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Switch_Floor2::Switch_Floor2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position,
		const int& switchno
	) :
		GameObject(StagePtr),
		m_Position(Position),
		m_SwitchNo(switchno)
	{
	}
	Switch_Floor2::~Switch_Floor2() {}

	//初期化
	void Switch_Floor2::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(Vector3(1, 0.3f, 1));
		PtrTransform->SetRotation(Vector3(0, 0, 0));
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionSphere>();
		PtrObb->SetFixed(true);


		AddComponent<Rigidbody>();
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SB_TX");

		GetStage()->DoorSwitch2 = true;
	}

	void Switch_Floor2::OnUpdate() {
	}

	void Switch_Floor2::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

	}
	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: スイッチに対応したどあ
	//--------------------------------------------------------------------------------------
	//構築と破棄
	DoorModel2::DoorModel2(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Position,
		const int& doorno
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_DoorNo(doorno)
	{
	}
	DoorModel2::~DoorModel2() {}

	//初期化
	void DoorModel2::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(Vector3(0, 0, 0));
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

		GetStage()->DoorSwitch2 = true;

	}

	void DoorModel2::Open() {

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(1.0f, Vector3(5.0f, 0.0f, 0.0f));

		//ループする
		PtrAction->SetLooped(false);
		//アクション開始
		PtrAction->Run();

	}
	//--------------------------------------------------------------------------------------
	//	class Switch_Floor : public GameObject;
	//	用途: スイッチ床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Switch_Floor3::Switch_Floor3(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position,
		const int& switchno
	) :
		GameObject(StagePtr),
		m_Position(Position),
		m_SwitchNo(switchno)
	{
	}
	Switch_Floor3::~Switch_Floor3() {}

	//初期化
	void Switch_Floor3::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(Vector3(1, 0.3f, 1));
		PtrTransform->SetRotation(Vector3(0, 0, 0));
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionSphere>();
		PtrObb->SetFixed(true);


		AddComponent<Rigidbody>();
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"SC_TX");

		GetStage()->DoorSwitch3 = true;
	}

	void Switch_Floor3::OnUpdate() {
	}

	void Switch_Floor3::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

	}
	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: スイッチに対応したどあ
	//--------------------------------------------------------------------------------------
	//構築と破棄
	DoorModel3::DoorModel3(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Position,
		const int& doorno
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_DoorNo(doorno)
	{
	}
	DoorModel3::~DoorModel3() {}

	//初期化
	void DoorModel3::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(Vector3(0, 0, 0));
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"USUKIN_TX");

		GetStage()->DoorSwitch3 = true;

	}

	void DoorModel3::Open() {

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(1.0f, Vector3(-4.0f, 0.0f, 0.0f));

		//ループする
		PtrAction->SetLooped(false);
		//アクション開始
		PtrAction->Run();

	}
	//--------------------------------------------------------------------------------------
	//	class Switch_Floor : public GameObject;
	//	用途: スイッチ床
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Switch_Floor4::Switch_Floor4(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position,
		const int& switchno
	) :
		GameObject(StagePtr),
		m_Position(Position),
		m_SwitchNo(switchno)
	{
	}
	Switch_Floor4::~Switch_Floor4() {}

	//初期化
	void Switch_Floor4::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(Vector3(1, 0.3f, 1));
		PtrTransform->SetRotation(Vector3(0, 0, 0));
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionSphere>();
		PtrObb->SetFixed(true);


		AddComponent<Rigidbody>();
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"USUKIN_TX");

		GetStage()->DoorSwitch3 = true;
	}

	void Switch_Floor4::OnUpdate() {
	}

	void Switch_Floor4::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

	}
	//--------------------------------------------------------------------------------------
	//	class DoorModel : public GameObject;
	//	用途: スイッチに対応したどあ
	//--------------------------------------------------------------------------------------
	//構築と破棄
	DoorModel4::DoorModel4(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Position,
		const int& doorno
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_DoorNo(doorno)
	{
	}
	DoorModel4::~DoorModel4() {}

	//初期化
	void DoorModel4::OnCreate() {
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(Vector3(0, 0, 0));
		PtrTransform->SetPosition(m_Position);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"USUKIN_TX");

		GetStage()->DoorSwitch3 = true;

	}

	void DoorModel4::Open() {

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		PtrAction->AddMoveBy(1.0f, Vector3(4.0f, 0.0f, 0.0f));

		//ループする
		PtrAction->SetLooped(false);
		//アクション開始
		PtrAction->Run();

	}

	//--------------------------------------------------------------------------------------
	//	class UnevenGroundData : public GameObject;
	//	用途: でこぼこ床のデータ
	//--------------------------------------------------------------------------------------
	//構築と破棄
	UnevenGroundData::UnevenGroundData(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}
	UnevenGroundData::~UnevenGroundData() {}
	//初期化
	void UnevenGroundData::OnCreate() {
		vector<VertexPositionNormalTexture> vertices =
		{
			{ VertexPositionNormalTexture(Vector3(-1.0f, 0.0f, 0.5f), Vector3(0.0f, 1.0f,0.0f), Vector2(0.0f, 0.0f)) },
			{ VertexPositionNormalTexture(Vector3(0.0f, 0.5f, 0.5f), Vector3(0.0f, 1.0f,0.0f), Vector2(0.5f, 0.0f)) },
			{ VertexPositionNormalTexture(Vector3(-1.0f, 0.0f,-0.5f), Vector3(0.0f, 1.0f,0.0f), Vector2(0.0f, 1.0f)) },
			{ VertexPositionNormalTexture(Vector3(0.0f, 0.0f,-0.5f), Vector3(0.0f, 1.0f,0.0f), Vector2(0.5f, 1.0f)) },
			{ VertexPositionNormalTexture(Vector3(1.0f, 0.0f, 0.5f), Vector3(0.0f, 1.0f,0.0f), Vector2(1.0f, 0.0f)) },
			{ VertexPositionNormalTexture(Vector3(1.0f, 0.0f,-0.5f), Vector3(0.0f, 1.0f,0.0f), Vector2(1.0f, 1.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = {
			0, 1, 2,
			1, 3, 2 ,
			1, 4, 5 ,
			3, 1, 5
		};
		wstring CheckStr = L"UnevenGroundMesh";
		//リソースが登録されているか確認し、登録されてなければ作成
		//こうしておくとほかのステージでもメッシュを使える
		if (!App::GetApp()->CheckResource<MeshResource>(CheckStr)) {
			//頂点を使って表示用メッシュリソースの作成
			auto mesh = MeshResource::CreateMeshResource<VertexPositionNormalTexture>(vertices, indices, false);
			//表示用メッシュをリソースに登録
			App::GetApp()->RegisterResource(CheckStr, mesh);
			//ワイアフレーム表示用のメッシュの作成
			vector<VertexPositionColor> new_vertices;
			for (size_t i = 0; i < vertices.size(); i++) {
				VertexPositionColor new_v;
				new_v.position = vertices[i].position;
				new_v.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
				new_vertices.push_back(new_v);
			}
			//ワイアフレームメッシュをリソースに登録
			App::GetApp()->RegisterResource(L"UnevenGroundWireMesh", MeshResource::CreateMeshResource(new_vertices, indices, false));
		}
		//三角形衝突判定用の三角形の配列を作成
		//こちらはステージ単位
		size_t i = 0;
		while (i < indices.size()) {
			TRIANGLE tr;
			tr.m_A = vertices[indices[i]].position;
			tr.m_B = vertices[indices[i + 1]].position;
			tr.m_C = vertices[indices[i + 2]].position;
			m_Triangles.push_back(tr);
			i += 3;
		}

		//自分自身をシェアオブジェクトに登録
		GetStage()->SetSharedGameObject(L"UnevenGroundData", GetThis<UnevenGroundData>());

	}



	//--------------------------------------------------------------------------------------
	//	class UnevenGround : public GameObject;
	//	用途: でこぼこ床
	//--------------------------------------------------------------------------------------
	UnevenGround::UnevenGround(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	UnevenGround::~UnevenGround() {}
	//初期化
	void UnevenGround::OnCreate() {

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto DataPtr = GetStage()->GetSharedGameObject<UnevenGroundData>(L"UnevenGroundData");
		//三角形コリジョン
		auto PtrTriangle = AddComponent<CollisionTriangles>();
		PtrTriangle->SetMakedTriangles(DataPtr->GetTriangles());
		PtrTriangle->SetWireFrameMesh(L"UnevenGroundWireMesh");
		PtrTriangle->SetDrawActive(true);

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"UnevenGroundMesh");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"HAIIRO_TX");

	}

	//--------------------------------------------------------------------------------------
	//	class NumberSprite : public GameObject;
	//	用途: 数字のスプライト
	//--------------------------------------------------------------------------------------
	NumberSprite::NumberSprite(const shared_ptr<Stage>& StagePtr, int numb, Vector2 pos, Vector2 Scale, int layer) :
		GameObject(StagePtr),
		m_num(numb),
		m_pos(pos),
		m_scale(Scale),
		m_layer(layer)
	{}

	void NumberSprite::OnCreate()
	{
		//数字の大きさによって回数変更
		int count = m_num;
		do
		{
			count /= 10;
			m_digit++;
		} while (count > 0);

		//数字のスプライト作成
		for (int i = 0; i < 10; i++)
		{
			//頂点配列
			vector<VertexPositionNormalTexture> vertices;
			//インデックスを作成するための配列
			vector<uint16_t> indices;
			//Squareの作成(ヘルパー関数を利用)
			MeshUtill::CreateSquare(1.0f, vertices, indices);
			//UV値の変更
			float from = i / 10.0f;
			float to = from + (1.0f / 10.0f);
			//左上頂点
			vertices[0].textureCoordinate = Vector2(from, 0);
			//右上頂点
			vertices[1].textureCoordinate = Vector2(to, 0);
			//左下頂点
			vertices[2].textureCoordinate = Vector2(from, 1.0f);
			//右下頂点
			vertices[3].textureCoordinate = Vector2(to, 1.0f);
			//頂点の型を変えた新しい頂点を作成
			vector<VertexPositionColorTexture> new_vertices;
			for (auto& v : vertices) {
				VertexPositionColorTexture nv;
				nv.position = v.position;
				nv.color = Color4(1.0f, 1.0f, 1.0f, 1.0f);
				nv.textureCoordinate = v.textureCoordinate;
				new_vertices.push_back(nv);
			}
			//メッシュ作成
			m_Mesh.push_back(MeshResource::CreateMeshResource<VertexPositionColorTexture>(new_vertices, indices, true));
		}

		int masternum = m_num;

		//桁分ループ
		for (int j = m_digit - 1; j >= 0; j--)
		{
			//生成桁数追加
			m_Constdigit++;

			int digi = j;
			int num = masternum / pow(10, (digi));
			masternum = masternum % (int)(pow(10, (digi)));

			auto NumP = GetStage()->AddGameObject<GameObject>();

			float distance = m_scale.x / 1.8f;

			auto TranP = NumP->AddComponent<Transform>();
			TranP->SetPosition(m_pos.x - (distance*j), m_pos.y, 0);
			TranP->SetScale(m_scale.x, m_scale.y, 1);
			TranP->SetRotation(0, 0, 0);

			auto DrawP = NumP->AddComponent<PCTSpriteDraw>();
			DrawP->SetTextureResource(L"TIMER_TX");
			DrawP->SetMeshResource(m_Mesh[num]);
			NumP->SetAlphaActive(true);

			NumP->SetDrawLayer(m_layer);
			m_Numbers.push_back(NumP);
		}
	}

	void NumberSprite::SetPositionVec2(Vector2 pos)
	{
		int count = 0;
		for (auto v : m_Numbers)
		{
			v->GetComponent<Transform>()->SetPosition(m_pos.x, m_pos.y, 0);
			count++;
		}
	}

	void NumberSprite::SetScaleVec2(Vector2 scale)
	{

	}

	void NumberSprite::SetNum(int num)
	{
		//マイナス弾く
		if (m_num >= 0)
		{
			m_num = num;
			//入力された桁持ってくる
			int digit = 0;
			int innum = num;
			do
			{
				innum /= 10;
				digit++;
			} while (innum > 0);

			//入力されたほうが大きかったら
			if (digit > m_digit)
			{
				for (int j = 0; j < (digit - m_Constdigit); j++)
				{
					//左側に桁追加
					int digitDif = digit - m_digit;

					m_Constdigit++;

					auto NumP = GetStage()->AddGameObject<GameObject>();

					float distance = m_scale.x / 1.8f;

					auto TranP = NumP->AddComponent<Transform>();
					TranP->SetPosition(m_pos.x - (distance*(m_digit)), m_pos.y, 0);
					TranP->SetScale(m_scale.x, m_scale.y, 1);
					TranP->SetRotation(0, 0, 0);

					auto DrawP = NumP->AddComponent<PCTSpriteDraw>();
					DrawP->SetTextureResource(L"TIMER_TX");
					DrawP->SetMeshResource(m_Mesh[0]);
					NumP->SetAlphaActive(true);

					NumP->SetDrawLayer(m_layer);
					m_Numbers.push_back(NumP);
				}

				for (int i = 0; i < m_Constdigit; i++)
				{
					m_Numbers[i]->SetDrawActive(true);
				}
			}

			//入力されたほうが小さい
			if (digit < m_digit)
			{
				for (int i = m_digit - 1; i > digit - 1; i--)
				{
					m_Numbers[i]->SetDrawActive(false);
				}
			}

			//桁更新
			m_digit = digit;

			//数字入れ替え
			int masternum = m_num;
			for (int i = m_digit - 1; i >= 0; i--)
			{
				int digi = i;
				int setnum = masternum / pow(10, (digi));
				masternum = masternum % (int)(pow(10, (digi)));

				m_Numbers[i]->GetComponent<PCTSpriteDraw>()->SetMeshResource(m_Mesh[setnum]);
			}
		}
	}

}
//end basecross
