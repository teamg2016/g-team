/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage2−3 : public Stage {
		//カメラの注視点からカメラ位置のベクトル
		Vector3 m_AtToEyeVec;
		//カメラズームの最大距離
		float m_ArmMax;
		//カメラズームの最小距離
		float m_ArmMin;
		//ズームスピード(ターン単位)
		float m_ZoomSpeed;

		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//固定のボックスの作成
		void CreateFixedBox();

		//左右移動しているボックスの作成
		void CreateMoveBoxL();
		//上下移動しているボックスの作成
		void CreateMoveBox2();
		//上下移動しているボックスの作成
		void CreateMoveBoxR();

		void CreateMoveBoxRO2();

		//すいっちとどあの作成
		void CreateDoorAndSwitch();
		//すいっちとどあの作成
		void CreateDoorAndSwitch2();
		//すいっちとどあの作成
		void CreateDoorAndSwitch3();
		//でこぼこ床の作成
		void CreateUnevenGround();

		//セルマップの作成
		void CreateStageCellMap();
		//ボックスのコストをセルマップに反映
		void SetCellMapCost();

		void CreateCSVFixedBoxManager();

		void CreateSpark();

		void CreateSpark2();

		void CreateSpark3();

		void CreateSpark4();

		int Count = 0;

		bool m_ContlFlag;
		int m_Count;
		bool Selectflag;
		bool PF = false;
		bool PF1 = false;
		bool GLF = false;
		bool NF = false;
		bool NF2 = false;

		shared_ptr<MultiAudioObject> m_AudioObjectPtr;

		void CreateTimerSprite();
		void CreateTimerSprite2();
		void CreateTimerSprite3();

		void CreateScore();

		Vector2 m_PlayerSpawnCell;

		size_t m_sColSize;
		size_t m_sRowSize;

		vector<vector<size_t>>m_MapDataVec;
		vector<vector<size_t>>m_MapDataVec2;

		void CreateRoad();

		//Frameタイマーを秒数へ変換用変数
		int CountFrame = 01;

		float m_Timer = 0;
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		bool Win = true;

		int T_scalechange = 1;

		float count = 5.0;


	public:
		//構築と破棄
		GameStage2−3() :Stage(),
			m_AtToEyeVec(0.0f, 15.0f, -18.0f),
			m_ArmMax(25.0f),
			m_ArmMin(3.0f),
			m_ZoomSpeed(0.1f)
		{}
		virtual ~GameStage2−3() {}
		void SceneChange();
		//初期化
		virtual void OnCreate()override;
		void CreateSelect1();
		void PlateSelect();
		virtual void OnUpdate()override;

		virtual void OnLastUpdate()override;

	};


}
//end basecross
