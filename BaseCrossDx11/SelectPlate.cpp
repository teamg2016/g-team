#include "stdafx.h"
#include "Project.h"

namespace basecross {

	SelectPlate::SelectPlate(const shared_ptr<Stage>& stage, const wstring & plate, const wstring & scene, int nam,const Vector3& pos,const Vector3& scl):
		GameObject(stage),
		m_pos(pos),
		m_scl(scl),
		m_scene(scene),
		pouse(plate),
		m_Num(nam),
		m_autosideNum(1),
		m_Select(false)
	{

	}

	void SelectPlate:: OnCreate() {
		auto transform = AddComponent<Transform>();
		transform->SetPosition(m_pos);
		transform->SetRotation(0, 0, 0);
		transform->SetScale(m_scl);

		auto Draw = AddComponent<PCTSpriteDraw>();
		Draw->SetTextureResource(pouse);
		SetAlphaActive(true);

		auto GetGroup = GetStage()->GetSharedObjectGroup(L"PlateGroup");
		GetGroup->IntoGroup(GetThis<GameObject>());
	}
	void SelectPlate::OnUpdate() {
		if (m_Select) {
			auto transform = GetComponent<Transform>();

			if (m_Num == m_autosideNum)
			{
				transform->SetScale(m_scl*1.5f);

				auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
				if (CntlVec[0].wPressedButtons&XINPUT_GAMEPAD_A&&m_scene!=L"Return") {
					auto scene = App::GetApp()->GetScene<Scene>();
					PostEvent(0, GetThis<GameObject>(), scene, m_scene);

				}
			}
			else {
				transform->SetScale(m_scl);
			}
		}
		
	}
}