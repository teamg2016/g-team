/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Player2 : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player2::Player2(const shared_ptr<Stage>& StagePtr, const Vector3& Pos, float MaxSpeed) :
		GameObject(StagePtr),
		m_Pos(Pos),
		m_MaxSpeed(MaxSpeed),	//最高速度
		m_Decel(0.95f),	//減速値
		m_Mass(1.0f),	//質量
		m_Goal(false)
	{}

	//初期化
	void Player2::OnCreate() {
		//初期位置などの設定
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(0.6f, 0.6f, 0.6f);	//直径25センチの球体
		Ptr->SetRotation(0.0f, 0.0f, 0.0f);
		Ptr->SetPosition(5.0f, 0.125f, -3.0f);
		Ptr->SetPosition(m_Pos);

		//Rigidbodyをつける
		auto PtrRedid = AddComponent<Rigidbody>();
		//反発係数は0.5（半分）
		PtrRedid->SetReflection(0.5f);
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();

		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();
		//横部分のみ反発
		PtrCol->SetIsHitAction(IsHitAction::Slide);
		AddComponent<Action>();
		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");
		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		//描画するテクスチャを設定
		PtrDraw->SetTextureResource(L"KURO_TX");
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Jump");
		pMultiSoundEffect->AddAudioResource(L"Body");
		pMultiSoundEffect->AddAudioResource(L"Switch");
		pMultiSoundEffect->AddAudioResource(L"Gate");
		pMultiSoundEffect->AddAudioResource(L"Item");

		//透明処理
		SetAlphaActive(true);
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Player2> >(GetThis<Player2>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->ChangeState(DefaultState2::Instance());
	}

	//移動の向きを得る
	Vector3 Player2::GetAngle() {
		Vector3 Angle(0, 0, 0);
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			if (CntlVec[0].fThumbLX != 0 || CntlVec[0].fThumbLY != 0) {
				float MoveLength = 0;	//動いた時のスピード
				auto PtrTransform = GetComponent<Transform>();
				auto PtrCamera = GetStage()->GetView()->GetTargetCamera();
				//進行方向の向きを計算
				Vector3 Front = PtrCamera->GetAt() - PtrCamera->GetEye();
				Front.y = 0;
				Front.Normalize();
				//進行方向向きからの角度を算出
				float FrontAngle = atan2(Front.z, Front.x);
				//コントローラの向き計算
				float MoveX = CntlVec[0].fThumbLX;
				float MoveZ = CntlVec[0].fThumbLY;
				//コントローラの向きから角度を計算
				float CntlAngle = atan2(-MoveX, MoveZ);
				//トータルの角度を算出
				TotalAngle = FrontAngle + CntlAngle;
				//角度からベクトルを作成
				Angle = Vector3(cos(TotalAngle), 0, sin(TotalAngle));
				//正規化する
				Angle.Normalize();
				//Y軸は変化させない
				Angle.y = 0;
			}
		}
		return Angle;
	}


	//更新
	void Player2::OnUpdate() {
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();

		auto Ptr = App::GetApp()->GetScene<Scene>();
		auto GF = App::GetApp()->GetScene<Scene>()->getGF2();
		auto AF = App::GetApp()->GetScene<Scene>()->getAF2();
		if (GF) {
			if (!AF) {
				GetComponent<Action>()->Stop();
				auto my_Trans = GetComponent<Transform>();
				auto my_Pos = my_Trans->GetPosition();
				auto my_Scale = my_Trans->GetScale();
				my_Trans->SetPosition(my_Pos);
				GetComponent<CollisionSphere>()->SetUpdateActive(false);
				GetComponent<Action>()->AllActionClear();
				GetComponent<Action>()->AddScaleBy(5.0, Vector3(0, 0, 0));
				GetComponent<Action>()->Run();
				Ptr->setAF2(true);
				Ptr->setGF2(false);
			}
		}
	}

	bool Player2::GoalCollision(vector<shared_ptr<GameObject>>& OtherVec) {
		m_Goal = false;
		auto PlayerPos = GetComponent<Transform>()->GetPosition();
		for (auto& v : OtherVec) {
			auto ShMoveTORUS = dynamic_pointer_cast<MoveTORUS>(v);
			if (ShMoveTORUS) {
				auto OtherTORUSPos = ShMoveTORUS->GetComponent<Transform>()->GetPosition();
				if (PlayerPos.y > OtherTORUSPos.y) {
					m_Goal = true;
					return true;
				}

			}
		}

		return false;
	}


	void Player2::OnCollision(vector<shared_ptr<GameObject>>& OtherVec) {

		//スコアアイテムを取った時
		for (auto v : OtherVec)
		{
			auto ScorePtr = dynamic_pointer_cast<SphereObject>(v);
			if (v == ScorePtr) {
				auto GS = GetStage();
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Item", 0, 1.0f);
				auto Gs = v;
				auto GSPos = Gs->GetComponent<Transform>()->GetPosition();
				//スパークの放出
				auto PtrSpark2 = GS->GetSharedGameObject<MultiSpark2>(L"MultiSpark2", false);
				if (PtrSpark2) {
					PtrSpark2->InsertSpark(GSPos);
				}
				dynamic_pointer_cast<SphereObject>(v)->Col();
			}
		}

		GoalCollision(OtherVec);

		if (GetStateMachine()->GetCurrentState() == JumpState2::Instance()) {
		}
		auto Player2Pos = GetComponent<Transform>()->GetPosition();
		for (auto& v : OtherVec) {
			auto ShMoveTORUS2 = dynamic_pointer_cast<MoveTORUS>(v);
			if (ShMoveTORUS2) {
				auto OtherTORUSPos2 = ShMoveTORUS2->GetComponent<Transform>()->GetPosition();
				if (Player2Pos.y > OtherTORUSPos2.y) {
					Goal2 = true;
				}
			}
		}
		auto PlayerVelo = GetComponent<Rigidbody>()->GetVelocity();
		auto PlayerSp = GetComponent<CollisionSphere>();
		for (auto& v : OtherVec) {
			auto PtrObbCol = v->GetComponent<CollisionObb>(false);
			if (PtrObbCol) {
				auto sp = PlayerSp->GetSphere();
				auto obb = PtrObbCol->GetObb();
				Vector3 Ret;
				HitTest::SPHERE_OBB(sp, obb, Ret);
				auto Normal = sp.m_Center - Ret;

				Normal.Normalize();
				if (Vector3EX::AngleBetweenNormals(Normal, Vector3(0, 1, 0)) > 0.01f) {
					Normal *= 3.5f;
					PlayerVelo += Normal;
					GetComponent<Rigidbody>()->SetVelocity(PlayerVelo);
					return;
				}
			}
		}

		//敵との反発判定
		auto Player2Velo = GetComponent<Rigidbody>()->GetVelocity();
		auto Player2Sp = GetComponent<CollisionSphere>();
		for (auto& v : OtherVec) {
			auto PtrObbCol = v->GetComponent<CollisionObb>(false);
			if (PtrObbCol) {
				auto sp = Player2Sp->GetSphere();
				auto obb = PtrObbCol->GetObb();
				Vector3 Ret;
				HitTest::SPHERE_OBB(sp, obb, Ret);
				auto Normal = sp.m_Center - Ret;

				Normal.Normalize();
				if (Vector3EX::AngleBetweenNormals(Normal, Vector3(0, 1, 0)) > 0.01f) {
					Normal *= 3.5f;
					Player2Velo += Normal;
					GetComponent<Rigidbody>()->SetVelocity(Player2Velo);
					return;
				}
			}
			auto shenemy2 = dynamic_pointer_cast<EnemyInterface2>(v);
			auto sh = dynamic_pointer_cast<GameObject>(v);
			if (shenemy2) {
				auto PtrRedid = GetComponent<Rigidbody>();
				auto Pos = GetComponent<Transform>()->GetPosition();
				auto Enemy2Pos = sh->GetComponent<Transform>()->GetPosition();

				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Body", 0, 2.0f);

				auto GS = GetStage();
				auto Gs = v;
				auto GSPos = Gs->GetComponent<Transform>()->GetPosition();
				//スパークの放出
				auto PtrSpark3 = GS->GetSharedGameObject<MultiSpark3>(L"MultiSpark3", false);
				if (PtrSpark3) {
					PtrSpark3->InsertSpark(GSPos);
				}

				//敵がダイレクトアタックしてきた時のプレイヤーの反発
				auto Velo = Pos - Enemy2Pos;
				Velo *= 10.0f;
				PtrRedid->SetVelocity(Velo);
			}
		}

		//ドアグループを呼び出す
		if (GetStage()->DoorSwitch) {
			auto dGroup = GetStage()->GetSharedObjectGroup(L"DoorGroup");
			auto sGroup = GetStage()->GetSharedObjectGroup(L"SwitchGroup");
			for (auto& s : OtherVec) {
				for (auto& so : sGroup->GetGroupVector()) {
					auto s_ptr = dynamic_pointer_cast<Switch_Floor>(so.lock());
					if (s_ptr == s) {
						for (auto& d : dGroup->GetGroupVector()) {
							auto d_ptr = dynamic_pointer_cast<DoorModel>(d.lock());
							if (s_ptr->m_SwitchNo == d_ptr->m_DoorNo) {
								auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
								pMultiSoundEffect->Start(L"Switch", 0, 1.0f);
								pMultiSoundEffect->Start(L"Gate", 0, 0.5f);
								auto GS = GetStage();
								auto Gs = s;
								auto Pos = Gs->GetComponent<Transform>()->GetPosition();
								//スパークの放出
								auto PtrSpark = GS->GetSharedGameObject<MultiSpark4>(L"MultiSpark4", false);
								if (PtrSpark) {
									PtrSpark->InsertSpark(Pos);
								}
								d_ptr->Open();
								s_ptr->SetDrawActive(false);
								s_ptr->GetComponent<CollisionSphere>()->SetUpdateActive(false);
							}
						}
					}
				}
			}
		}
		if (GetStage()->DoorSwitch2) {
			auto dGroup = GetStage()->GetSharedObjectGroup(L"DoorGroup2");
			auto sGroup = GetStage()->GetSharedObjectGroup(L"SwitchGroup2");
			for (auto& s : OtherVec) {
				for (auto& so : sGroup->GetGroupVector()) {
					auto s_ptr = dynamic_pointer_cast<Switch_Floor2>(so.lock());
					if (s_ptr == s) {
						for (auto& d : dGroup->GetGroupVector()) {
							auto d_ptr = dynamic_pointer_cast<DoorModel2>(d.lock());
							if (s_ptr->m_SwitchNo == d_ptr->m_DoorNo) {
								auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
								pMultiSoundEffect->Start(L"Switch", 0, 1.0f);
								pMultiSoundEffect->Start(L"Gate", 0, 0.5f);
								auto GS = GetStage();
								auto Gs = s;
								auto Pos = Gs->GetComponent<Transform>()->GetPosition();
								//スパークの放出
								auto PtrSpark = GS->GetSharedGameObject<MultiSpark4>(L"MultiSpark4", false);
								if (PtrSpark) {
									PtrSpark->InsertSpark(Pos);
								}
								d_ptr->Open();
								s_ptr->SetDrawActive(false);
								s_ptr->GetComponent<CollisionSphere>()->SetUpdateActive(false);
							}
						}
					}
				}
			}
		}
		if (GetStage()->DoorSwitch3) {
			auto dGroup = GetStage()->GetSharedObjectGroup(L"DoorGroup3");
			auto sGroup = GetStage()->GetSharedObjectGroup(L"SwitchGroup3");
			for (auto& s : OtherVec) {
				for (auto& so : sGroup->GetGroupVector()) {
					auto s_ptr = dynamic_pointer_cast<Switch_Floor3>(so.lock());
					if (s_ptr == s) {
						for (auto& d : dGroup->GetGroupVector()) {
							auto d_ptr = dynamic_pointer_cast<DoorModel3>(d.lock());
							if (s_ptr->m_SwitchNo == d_ptr->m_DoorNo) {
								auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
								pMultiSoundEffect->Start(L"Switch", 0, 1.0f);
								pMultiSoundEffect->Start(L"Gate", 0, 0.5f);
								auto GS = GetStage();
								auto Gs = s;
								auto Pos = Gs->GetComponent<Transform>()->GetPosition();
								//スパークの放出
								auto PtrSpark = GS->GetSharedGameObject<MultiSpark4>(L"MultiSpark4", false);
								if (PtrSpark) {
									PtrSpark->InsertSpark(Pos);
								}
								d_ptr->Open();
								s_ptr->SetDrawActive(false);
								s_ptr->GetComponent<CollisionSphere>()->SetUpdateActive(false);
							}
						}
					}
				}
			}
		}

	}

	void Player2::OnCollisionExcute(vector<shared_ptr<GameObject>>& OtherVec) {
		if (OtherVec.size() >= 3) {
			m_CollCount++;
			if (m_CollCount >= 2) {
				m_CollCount = 0;
				auto TransPtr = GetComponent<Transform>();
				auto Pos = TransPtr->GetPosition();
				Pos.y += 2.0f;
				TransPtr->ResetPosition(Pos);
			}
		}
		else {
			m_CollCount = 0;
		}
	}



	//ターンの最終更新時
	void Player2::OnLastUpdate() {

		auto Pos = GetComponent<Transform>()->GetWorldMatrix().PosInMatrix();

		auto PtrCol = GetComponent<CollisionSphere>();
		
		auto Group = GetStage()->GetSharedObjectGroup(L"MoveBox2");
		auto GVec = Group->GetGroupVector();
		auto PtrTrans = GetComponent<Transform>();
		for (auto& v : GVec) {
			auto shptr = dynamic_pointer_cast<MoveBox2>(v.lock());
			if (shptr) {
				auto ParCol = shptr->GetComponent<CollisionObb>();
			}
		}
	}

	//モーションを実装する関数群
	//移動して向きを移動方向にする
	void Player2::MoveRotationMotion() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		Vector3 Angle = GetAngle();
		//Transform
		auto PtrTransform = GetComponent<Transform>();
		//Rigidbodyを取り出す
		auto PtrRedit = GetComponent<Rigidbody>();
		//現在の速度を取り出す
		auto Velo = PtrRedit->GetVelocity();
		//目的地を最高速度を掛けて求める
		auto Target = Angle * m_MaxSpeed;
		//目的地に向かうために力のかける方向を計算する
		//Forceはフォースである
		auto Force = Target - Velo;
		//yは0にする
		Force.y = 0;
		//加速度を求める
		auto Accel = Force / m_Mass;
		//ターン時間を掛けたものを速度に加算する
		Velo += (Accel * ElapsedTime);
		//減速する
		Velo *= m_Decel;
		//速度を設定する
		PtrRedit->SetVelocity(Velo);
		//回転の計算
		float YRot = PtrTransform->GetRotation().y;
		Quaternion Qt;
		Qt.Identity();
		if (Angle.Length() > 0.0f) {
			//ベクトルをY軸回転に変換
			float PlayerAngle = atan2(Angle.x, Angle.z);
			Qt.RotationRollPitchYaw(0, PlayerAngle, 0);
			Qt.Normalize();
		}
		else {
			Qt.RotationRollPitchYaw(0, YRot, 0);
			Qt.Normalize();
		}
		//Transform
		PtrTransform->SetQuaternion(Qt);
	}


	//Aボタンでジャンプするどうかを得る
	bool Player2::IsJumpMotion() {
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected) {
			//Aボタンが押された瞬間ならジャンプ
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Jump", 0, 0.5f);
				return true;
			}
		}
		return false;
	}

	//Aボタンでジャンプする瞬間の処理
	void Player2::JumpMotion() {
		auto PtrTrans = GetComponent<Transform>();
		//重力
		auto PtrGravity = GetComponent<Gravity>();
		//ジャンプスタート
		Vector3 JumpVec(0.0f, 4.0f, 0);
		PtrGravity->StartJump(JumpVec);
	}
	//Aボタンでジャンプしている間の処理
	//ジャンプ終了したらtrueを返す
	bool Player2::JumpMoveMotion() {
		auto PtrTransform = GetComponent<Transform>();
		//重力
		auto PtrGravity = GetComponent<Gravity>();
		if (PtrGravity->GetGravityVelocity().Length() <= 0) {
			return true;
		}
		return false;
	}


	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<DefaultState2> DefaultState2::Instance() {
		static shared_ptr<DefaultState2> instance;
		if (!instance) {
			instance = shared_ptr<DefaultState2>(new DefaultState2);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void DefaultState2::Enter(const shared_ptr<Player2>& Obj) {
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void DefaultState2::Execute(const shared_ptr<Player2>& Obj) {
		if (Obj->flgTime++ >= 0.1f)
		{
			Obj->Goal2 = false;
			Obj->flgTime = 0;
		}
		Obj->MoveRotationMotion();
		if (Obj->IsJumpMotion()) {
			//Jumpボタンでステート変更
			Obj->GetStateMachine()->ChangeState(JumpState2::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void DefaultState2::Exit(const shared_ptr<Player2>& Obj) {
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//	class JumpState : public ObjState<Player>;
	//	用途: ジャンプ状態
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<JumpState2> JumpState2::Instance() {
		static shared_ptr<JumpState2> instance;
		if (!instance) {
			instance = shared_ptr<JumpState2>(new JumpState2);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void JumpState2::Enter(const shared_ptr<Player2>& Obj) {
		//ジャンプ中も移動可能とする
		Obj->MoveRotationMotion();
		Obj->JumpMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void JumpState2::Execute(const shared_ptr<Player2>& Obj) {
		/*if (Obj->IsP1JumpMotion()) {
			Obj->JumpMotion();
		}*/

		//ジャンプ中も移動可能とする
		Obj->MoveRotationMotion();
		if (Obj->JumpMoveMotion()) {
			//通常状態に戻る
			Obj->GetStateMachine()->ChangeState(DefaultState2::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void JumpState2::Exit(const shared_ptr<Player2>& Obj) {
		//何もしない
	}


}
//end basecross

